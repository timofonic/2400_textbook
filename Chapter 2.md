# Chapter 2
### In which we dig in a little deeper

## Context
We begin this chapter by stating unequivocally that computers have historically done a lousy job with *context*. They have been unable to understand, process and make sense of the nuanced detail of our lives and our interactions with each other and with our environment. Why? Because they have struggled with *knowledge*. They have struggled to *learn* in the way that humans learn. They have not been able to generalise either from the specific to the general or from the general to the specific. They have been unable to observe a piece of art and decide if they *like* it or not or even, until very recently, to quantify its characteristics. They struggle with deciding if a painting were a Picasso, a Monet or a Da Vinci. 

Those kind of tasks -- appreciating beauty, creating music, writing poetry -- have been left to humans because computers lacked the ability to create and appreciate *context*. They can’t see the *bigger picture*. All this is changing. Read on.

**Figure DC. The definition of context from Visual Thesaurus**

![visualthesaurus.com Definition of context](https://raw.githubusercontent.com/robertriordan/2400/master/Images/vt_context.PNG)

The Visual Thesaurus definition for context reads, in part:

1. Language that helps to determine its interpretation 
2. The set of facts or circumstances that surround a situation or event

Of the two definitions, the second is most important for us. I have written elsewhere that data are *facts*. Just pieces of unencumbered specifics, providing measurement of some phenomenon. *Circumstance*, on the other hand, is a collection of facts which together frame the *situation* or *condition*. It’s the *fact bath* in which we continuously find ourselves. It’s all the stuff that happens *around* an event at a place and time that give us the ability to weave a rich fabric of meaning around the event and allows us reduce the error around what’s happening in a particular setting. Here's what the online Oxford Reference has to say about context:

"Most broadly, any frame of reference or framework within which something is perceived, produced, consumed, communicated, interpreted, or otherwise experienced, or which is seen as relevant to the description or analysis of any phenomenon."

[Interested?](http://bit.ly/1ITF5oD)

The salient parts of that definition refer to the *frame of reference* within which *something is perceived, consumed... or otherwise experienced...* Why so important? Because context is everything. As humans, we live in a contextual soup of signs, signals, impressions, facts and suspicions. Computers can’t quite figure us out. Yet.

So what is context? Another way to look at it is by examining what we mean by the term *situation*. Here's what the Visual Thesaurus has to show on that term:

**Figure LJST. The definition of *situation* from Visual Thesaurus**

![visualthesaurus.com Definition of context](https://raw.githubusercontent.com/robertriordan/2400/master/Images/vt_situation.png)

The specific Visual Thesaurus definition presented in LJST pertains to "the general state of things; the combination of circumstances at a given time." It is here where we find *context* crossing with *situation*, *surroundings* and *circumstance*.  

Think for the moment of the process of getting ready for an adventure outside the home. Going to school maybe, or work, or just for a walk. Think about the decisions that need to be made, focusing on what to wear. If you and I are even somewhat alike, then you have a range of things to put on your back -- but what to wear today? First level of decision, likely made with no conscious thought whatsoever, is to consider the season. Thus the season provides the largest or highest level (most abstract) *context* in which is made the decision of what to wear outside. Season sets the broad parameters. Fact: It’s cold in winter and hot in summer and spring and fall are a crap shoot. 

Next in line is likely the general state of the weather, within the broader context of season. If it's winter, is it sunny and cold or is a blizzard raging? Finally, just hearing that it's cold outside is insufficient context. As we know, being good Canadians, there's Winnipeg or Ottawa cold and then there's Vancouver or Niagara cold. Not the same thing at all. *Ergo* "cold" is a qualitative and relative measure, relative to the context of where one currently finds oneself. It doesn't much help us to decide on apparel. Summer cold is a different beast entirely from winter cold. Consider further the tug-of-war between the Alberta Clipper and the Chinooks. Even being in Canada in the winter can cause wide swings in temperature. 

Let's just drive this home by considering temperature in a little more detail. In determining what to wear, we might be wise to consult a weather service. Navigate to a website or click a *Follow me* app such as www.theweathernetwork.com (and isn't the function of 'follow me weather' to provide the **context** of where one is currently located?) on your device or switch on the TV and select the weather channel (where a local context will already be provided because the weather channel *knows* to where the signal is being sent and thus provides the proper geo-context for you). Normally the current temperature is the most salient factor, thus we might find a large **20** prominently displayed on the screen. Not to put too fine a point on it, and you've likely already caught on, but *20 degrees* indicates entirely different weather in Buffalo as opposed to just across the lake in Toronto - even on a day where the objective weather is nearly the same in both cities. That's owing to, obviously, the scale of temperature being measured in *Fahrenheit* in Buffalo and *Celsius* in Toronto. Twenty F is below freezing in Buffalo whereas 20 C is t-shirt weather in Toronto. So the context of location, regardless of the actual weather, is important. 

Context drives interpretation. Context makes information out of data and allows us to make decisions.

And now, just to really make this a challenge, consider the final bowl of contextual stew in which we find ourselves. *Fashion. Whim. Trends. Expectations. Style.* How do I feel today? Is it before or after Labour Day? Should my pants and jacket be from the same design house? Do I dare wear a department store label? What did I wear last time I was with the people I'm likely to see today? What will they expect me to wear, given that I just got a promotion at work? Will *he* be there? What kid of person do I need to be today?

Often, when someone reminds me of a particularly silly or ill-conceived decision I have made in the past, I glibly justify by stating "Oh, **that** was during my pastel phase." It's meaningless banter meant to deflect criticism in a lighthearted way but... what if it were true? What if I did have a *pastel phase*, whatever that might mean. Indeed, how could we ever expect a machine to decode and enforce the rules around such a thing whether in the present (I'm only wearing chalk tones these days") or when examining the past ("Oh, well, I must be excused for that behaviour -- I was under the influence at the time")?

Fashion is a tall order for people, let alone machines. And people often get fashion wrong. Take a stroll through a Tim Horton's. Worse still, fashion is *opinion* driven by the huge influence machine we call *marketing*. We can't win.   

### Situatedness
An interesting corollary of context is the notion of *situatedness*. Oxford Reference defines it partly as:

“The dependence of meaning (and/or identity) on the specifics of particular sociohistorical, geographical, and cultural contexts, social and power relations, and philosophical and ideological frameworks, within which the multiple perspectives of social actors are *dynamically constructed, negotiated, and contested.*” [emphasis added] 

[Interested?](http://bit.ly/1svpjLY)

Ever answered to someone “You had to be there.” in reply to the question “What’s so funny?” If so, then you appreciate *context*. The interpretation of a message or communication is dependent on the situation in which it occurs and to which it refers. Imagine how many ways a simple word like *Yes* or *No* could be interpreted when you consider situational variables such as voice inflection, facial expression, volume and the length of utterance of the speaker, emphasis on a particular word or even syllable, to name but a few. There’s an hilarious comedy skit by a Jewish comic in which he successively puts the emphasis on each word in the interrogative phrase “He said I should bring a gift?” and in so doing changes the interpretation of the phrase each time. Try it yourself: Emphasise the **bolded** word in each sentence below by making the word a question in and of itself and note how the meaning attached to the phrase changes each time:

**He?** said I should bring a gift?

He **said?** I should bring a gift?

He said **I?** should bring a gift?

He said I **should?** bring a gift?

And so forth. 

There are literally dozens of possible interpretations or meanings for a simple word, depending on the context in which the word was uttered. Misreading the context of a situation can be a very serious matter. People are in jail for it. Lives are ruined by it. If someone has ever quoted you *out of context* then you know what we’re talking about here. So subtle and so powerful is context that we must dynamically manage all our behaviours in order to be appropriate *in context*.

Oxford suggests that context is dynamically constructed, negotiated and contested. I would add that people navigate and interpret context all the time. We are social actors who are continuously jostling for a position of advantage among our peers and contemporaries. Such is the human condition.

Consider, if you will, the enormous scope of what humans do in constructing context. Look at all the terms in Figure LJST (above) and how each nuanced difference in term conjures up a slightly different meaning for us humans. Milieu, surroundings, circumstance, setting, background, scope, ecology. arena, domain, state of affairs and indeed, *context.*   

#### Exformation
Appropriate in this context is the discussion of the concept of *Exformation*. Here I extensively quote [the exformation entry in wikipedia.com](https://en.wikipedia.org/wiki/Exformation "Exformation") (all emphasis added by me]:

"Exformation (originally spelled *eksformation* in Danish) is a term coined by Danish science writer Tor Nørretranders in his book *The User Illusion* published in English 1998. It is meant to mean *explicitly discarded information*. However, the term also has other meanings related to information, for instance *useful and relevant information* ...

"Effective communication depends on a shared body of knowledge between the persons communicating. In using words, sounds, and gestures, the speaker has deliberately thrown away a huge body of information, though it remains implied. This shared **context** is called *exformation*.

"Exformation is everything we do not actually say but have in our heads when, or before, we say anything at all - whereas information is the measurable, demonstrable utterance we actually come out with. [see Chapter 1 of this textbook.]

If someone is talking about computers, what is said will have more meaning if the person listening has some prior idea what a computer is, what it is good for, and in what *contexts* one might encounter one. From the information content of a message alone, there is no way of measuring how much exformation it contains.

"In 1862 the author Victor Hugo wrote to his publisher asking how his most recent book, *Les Misérables*, was getting on. Hugo just wrote "?" in his message, to which his publisher replied "!", to indicate it was selling well. This exchange of messages would have no meaning to a third party because the shared context is unique to those taking part in it. The amount of information (a single character) was extremely small, and yet because of exformation a meaning is clearly conveyed."

**Figure JJLL. Exformation as context**

![Exformation as context](https://raw.githubusercontent.com/robertriordan/2400/master/Images/exformation.png)

Examine Figure JJLL and imagine time-transporting Canada's first PM, Sir John A. Macdonald to the present day and presenting him with an Android device (I don't know about you, but I indulge in such fantasies regularly...). Note the massive *exformation* in this interaction - the things that we just implicitly understand about such modern devices that Sir John A. could not possibly fathom. This is context at its most obvious. 

We might think of exformation as *common sense* in a way. Common sense is *context* that is commonly held by individuals in a particular social setting. When someone says "That's just common sense!" it is understood that a person should have internalised the context in which some action was taken and have been guided by it. "It's just common sense that you need a computing device to send an email", for example It's a shared understanding and a product of having been immersed in the computing culture since forever. But what of Sir John A?   

Our question in this section is “Will machines ever be able to make sense of context and exformation?" We’ll get there in our discussion. Hang on. 

Meanwhile, take a look at context in practice. Below in Figure GH we see the distribution of scores on the 2013 high school exit examination in Poland. Is human decision making at play here ("We just can't let poor Wojciech fail... he's so close!") or have humans written a computer algorithm that contextualises grades such that "Given that scores have a confidence interval around them of say +/-3%, any score that is in the range of 27-30 should be awarded a grade of 30." We don't know, but this is interesting *contextual rule-making*, no? Also note the small amount of what's referred to in statistical circles as *heaping* at the highest level (100%). I guess if you get close enough, what's the difference? If you log into bit.ly (see below) and examine the data, you will see that there were no scores of 92, 95 or 98, but that the frequency of test scores at 100% was twice that at 99% despite a pattern of continually falling percentages as the distribution approaches 100%. You might also note that no one scored a 29. There was also a bit of a heap at 31 -- so likely a 31 means you scored a legit 30 and were given a 31 so as to differentiate from those who were *gifted* a 30 - that's a message in itself. A 30 means you didn't earn it... And there's a bit of heaping at 32 up to about 36 or so as the *bumping up* continues to ripple through the distribution. An otherwise almost textbook *normal distribution* becomes the victim of context. 

**Figure GH. Context at its finest.**

<div>
    <a href="https://plot.ly/~Dreamshot/461/" target="_blank" title="Distribution of the Results of the Matura in 2013 (Poland&#39;s High School Exit Exam)&lt;br&gt;&lt;br&gt;The minimum score to pass is 30%" style="display: block; text-align: left;"><img src="https://plot.ly/~Dreamshot/461.png" alt="Distribution of the Results of the Matura in 2013 (Poland&#39;s High School Exit Exam)&lt;br&gt;&lt;br&gt;The minimum score to pass is 30%" style="max-width: 100%;width: 700px;"  width="700" onerror="this.onerror=null;this.src='https://plot.ly/404.png';" /></a>
    <script data-plotly="Dreamshot:461"  src="https://plot.ly/embed.js" async></script>
</div>
*Source: https://plot.ly. Create an account and play around. Fun stuff.*

### Meaning
How do we derive meaning from data or communication? Through interpreting data and facts in context. Context provides meaning for meaningless data. But what does *meaning* mean? Oxford to the rescue.

"Whatever it is that makes what would otherwise be mere sounds [...] into instruments of communication and understanding. The philosophical problem is to demystify this power, and to relate it to what we know of ourselves and the world."

[Interested?](http://bit.ly/1GoQwmb)

What we *know* of ourselves and the world is clearly *knowledge*.  But from where comes knowledge?
[Oxford](http://www.oxforddictionaries.com/us/definition/american_english/knowledge "Oxford on knowledge") offers the following (edited for applicability in this context -- see? Context everywhere). Knowledge is:

1. **Facts, information**, and skills acquired by a person through **experience** or education; the theoretical or practical understanding of a subject
2. Awareness or familiarity gained by experience of a fact or **situation** 

The important concepts were **bolded** in the definitions above by me. Let’s look at each in turn:

1. Facts – facts are data; simple measurements lacking context
2. Information – we deal with information below, and how it is derived from data and then becomes potentially actionable through context
3. Experience – comes from observation of the results of actions, which flow from decisions fuelled by actions as a result of information produced from data in a context
4. Situation – clearly a synonym for context

This suggests a simple and general model of knowledge acquisition that applies to all creatures, great and small, that are faced with making decisions. But where does this all fit in terms of business? 

### Context in business decisions
**Figure LJLJ. A simple context - from measurement to knowledge**

![Simple context](https://raw.githubusercontent.com/robertriordan/2400/master/Images/simple_context.png)

Examine Figure LJLJ from left to right. Imagine this as the first ever measurement of a phenomenon in a state of complete *nescience* (absence of knowledge); the very first ever occurrence of this data; perhaps the very first sale on the first day of a new business where everything is all brand new. View it as a system in the way we understand systems from the introductory chapter where *d* (data) is the input and *K* (knowledge) is the output. The only existing elements are measurements, which make the data tangible (length, for example, is a concept or a variable; 6 cm. is tangible data and a fact representing the length of something being measured). 

There might well be a question or a challenge creating a strategy vacuum and drawing data into that question context. It’s also possible that exploratory research is driving the data initiative, as in “Let’s look at our data and see what it has to tell us.” with no particular agenda in mind. 

Regardless of what nudged the process into action (remember that systems need a trigger in order to start), the ensuing process is the same. It begins as newly-measured data approach from outside the system and become understood in a *context* where the data become *information*. This creation of new information allows a decision to be made, and decisions lead either to *action* or *inaction*. Action/inaction leads to results, the measurement of which yield new data (measured observations) in the context of that situation, which leads ultimately to knowledge (experienced gained by observing the effects of the action/inaction in this particular context). 

This is a general theory of how data are transformed into knowledge. 

There are thus two contexts in each chain from data to knowledge. There’s the initial context with the decision and action/inaction attendant upon it, then there’s the context resulting from the action/inaction. This is how we gain knowledge. We act or stand still. We experiment. We make decisions and we see what happens. But there is a much richer model to come, wherein we benefit from our accumulated knowledge and our observations of context. That’s the only real reason for any of this. To get better at what we do based on experience. Hang on. 

It’s important to note that **information only occurs within a context**, and each context is, by definition, unique. There can be similar contexts, contexts that share traits with others, but **each context is itself unique**. The universe ticks ahead and an infinite array of things change with each tick. Thus it’s important to bring an optimal amount of data to bear on a particular context, as it will never occur again. Everything changes in the ticking of the clock. We will return to this.  

Let’s re-examine our clothing decision in light of this theory.  

Imagine a new friend has just landed from Mars. Or Alberta. Our visitor needs to venture outdoors to find food and has no preconceived notions (previous contexts) upon which to base a choice of clothing. Our Martian friend has an objective measurement of temperature (temperature being a universal concept and all living organisms are sensitive to temperature and have an *operating range* outside of which they cannot function), is aware of their location (earth) and of the star date, and finally that they have a certain tolerance for temperature (this is accumulated knowledge). *What to wear?* 

In our knowledge-acquisition model, there is *always* context. The context in this case is that there is precious little data upon which to base the information required to choose what to wear. So in this context, our Martian friend simply chooses to wear nothing (no action) while going for a stroll to the mall. Turns out today is February 10th and we’re in Ottawa. A tad chilly. The result of this decision to take no dressing action is that our friend experiences intense cold against an unprotected body and the ambient temperature is outside the sustainable operating range of a healthy Martian. Their body measures the temperature and the effects of that temperature and this data, in the new context, becomes information. This information in this context, in association with the information provided by the initial context is stored, becomes knowledge. Now our friend has a bit of a bead on what to wear in the next, similar, context. They have gained some *knowledge*. 

The nugget of knowledge for our little green friend is that at this temperature on this date in this location taking no action to protect oneself from the elements is potentially damaging. *Bingo.* Knowledge and a new context upon which to base further action. Walk through this scenario again with this knowledge but with the same Martian in the same situation on July 10. What would the process look like? 

To answer this question, we must move to the more elaborated model of knowledge acquisition (or decision making if you prefer). See Figure LAJ.

**Figure LAJ: The flow of data into contextual information to decision, action and knowledge**

![Context](https://raw.githubusercontent.com/robertriordan/2400/master/Images/context.png)

In Figure LAJ we see illustrated how the process of measurement (m), whether conscious or not, provides a value for a new piece of data (d), which in combination with existing, measured data (whether newly-measured or previously measured) flows into a new context (circle) where new information (green) is produced when combined with knowledge available through feedback from previous contexts (green arrow). This new 'mashup' provides the necessary ingredients for a new decision. The decision begets some results, whether through action or inaction, which, in turn, creates new information in this new context. This new information in turn feeds knowledge from observing the outcome of the action. This new knowledge (K), in combination with prior knowledge, in its accumulated glory, washes back over any new context. The creation of knowledge is thus a dynamic, fluid and constant activity for living organisms and inanimate entities such as organisations alike. 

### Schemata and knowledge acquisition
This model of knowledge acquisition is not inconsistent with, and may be seen as an extension of, the work done in the fields of Psychology and Cognitive Science in the area of *schemata*. Pioneering work by Bartlett, extended by Jean Piaget, (see the Interested? links below) postulates that people learn about and make sense of their work by building, modifying, extending and discarding mental models of how things work. A quick (and brutally naïve) explanation and example follows.

The first premise necessary to accept if one subscribes to the schema notion of knowledge acquisition is that of *tabula rasa*, loosely translated and understood to mean *blank slate*, as in a blank chalk board in a classroom. As far back as Aristotle, the notion that humans are born with a blank slate mind has been popular. [Interested?]( http://en.wikipedia.org/wiki/Tabula_rasa) This *blank slate* is written on, erased, altered and generally enhanced as humans grow and learn and acquire knowledge. So at the first encounter of a child with a dog, for example, the rudimentary schema of *dog* is created that inscribes the observed characteristics of the dog (a particular size creature with four legs, fur, a tail and a long snout of a nose, etc.) to the dog schema. A parent labels the creature *dog* and our kid is off to the races, able to recognise all manner of four-legged creatures forever more. But trouble looms...

Next, our fearless young dog expert encounters a cow. Slightly bigger, but hey, it’s got four legs, fur, a snout and a tail. Must be a dog. So our schema builder spots the cow and happily announces the results of their interpretation to a parent: “Dog!” Nope, the parent corrects, that’s a cow (and probably makes a mooing sound to reinforce the difference). New schema in order here. At the same time, it’s possible that a *super schema* is also developed, this one encompassing the characteristics we know as *animal* and containing the attributes “four legs, fur, tail, long snout” of which *dog* and *cow* are specific examples. *Dog* might now have an encoded attribute for *size* that would differentiate the dog from the cow based on bulk. But *calf* and *pony* and *puppy* and *kitten* present problems, necessitating further refinements of the existing schemata. Isn't a small calf about the same size a a big dog? We won’t even get into the difference between a house cat (and the colour variation) and the myriad of *jungle casts*. Learning can be frustrating. 

A [McKinsey report on Machine Learning](http://www.mckinsey.com/insights/high_tech_telecoms_internet/an_executives_guide_to_machine_learning "Machine Learning") had the following to say about how machines are *taught* to recognise simple things like cows and cats:

"In 2007 Fei-Fei Li, the head of Stanford’s [University] Artificial Intelligence Lab, gave up trying to program computers to recognize objects and began labeling the millions of raw images that a child might encounter by age three and feeding them to computers. By being shown thousands and thousands of labeled data sets with instances of, say, a cat, the machine could shape its own rules for deciding whether a particular set of digital pixels was, in fact, a cat. Last November, Li’s team unveiled a program that identifies the visual elements of any picture with a high degree of accuracy. IBM’s Watson machine relied on a similar self-generated scoring system among hundreds of potential answers to crush the world’s best Jeopardy! players in 2011." 

We return to *Jeopardy!* and *Watson* later in this chapter.

It is through continual exposure to new data and continual feedback based on made decisions emanating from such exposure that specific and actionable knowledge is created and stored. You can see that the notion of context fits nicely here (well maybe you can’t see yet, but I can and I hope you will come to an understanding of these principles as we progress). It is also very interesting to note the similarities between the schemas of cognitive psychology and the software development technique known as *Object-Oriented Programming* (or OOP). I’ll try to remember to revisit that connection in the chapter on Systems Development... :)   

Knowledge acquisition (also referred to as the process of *Knowledge Engineering*) is also the biggest obstacle to creating *Artificial Intelligence* (AI). If we have time to fit that in somewhere, it will likely be in the chapter on *Future Trends*. While we’re here, I also strongly recommend a course (or even picking up a book on the topic) in *Cognitive Psychology* for anyone interested in ICT, Marketing, Finance or Management. It’s so cool you might just fall in love.  

[Interested in schemas?](http://en.wikipedia.org/wiki/Schema_(psychology))
[Interested in knowledge acquisition?]( http://www.wisegeek.com/what-is-knowledge-acquisition.htm)

Knowledge acquisition can be seen as a system, with the four main entities identified by coloured background ovals in Figure LAJ. On the left is *Input*, flowing into *Process*, which produces *Output* which is fed back into the system as *Input* to the continuous process of knowledge production and acquisition. And don't overlook the important tenet that much of the data that enters a decision process as input, and equally previous knowledge that is fed back to the decision-making process, functions as *context*. It is the soup made from the situation and the previous experience that allows us to decide how to move forward. All the ingredients of the soup together allow us to decide if we like the taste. 

## Important takeaways
* 
A *context* is a new, unique and non-reoccurring circumstance created out of the intersection of existing and new data and existing knowledge gained from previous contexts. It can only occur once and it's gone. Time marches on. No two contexts are alike.
    
* 
Since contexts (and thus decisions emanating from the information created in them) are unique, it is critical to provide the optimal (note I didn’t write *maximal* as too much data can be as confounding as too little data) amount of data and knowledge as input to the context. There is no substitute for accurately-measured data and accumulated knowledge. 

* 
Decision quality is a function of reducing error around the decision outcome. Accumulated knowledge from previous contexts and of the outcomes of decisions attendant upon them will reduce error. Better decisions are the result. If you have *seen stuff like this before* and have *observed the outcome* you are clearly in a better position to make a more accurate prediction of the future, and thus better decisions about how to get there. 

* 
Knowledge is accumulated by observing results of action/inaction in context. 

* 
Wisdom is knowing how and when to use it...

(tk - relate this to input/output ratio)

### Data architecture

How do we go about ensuring that optimal contexts are built, within which we generate the most valuable information? The answer is *data architecture* and perhaps in the broader field of *information architecture*. Our online Business Dictionary defines *data architecture* as:

"Models, policies, rules, or standards that govern which data is collected, and how it is stored, arranged, and put to use in a database system, and/or in an organization."

[If you are interested, make sure to follow each link for the related definitions!](http://www.businessdictionary.com/definition/data-architecture.html)

What does all this mean for us? It means that we need to carefully think about *which* data to collect, *how* we will collect it, how we will *store* it and how we will *retrieve* it in order that it will contribute to our decision-making context. We will return to this general topic when we discuss databases and related technology in a subsequent chapter. 

For now, imagine a survey with only one question: "So what do you think?" Administering this survey to a sample of your cohort would lead to all kinds of rich data, wild imaginings and likely some quite pointed suggestions about what we can do with our *survey*. The data we get in response might well be interesting, but not at all targeted to anything. People would be looking for a context. "What do I think about WHAT?" In fact, I went to the trouble of creating such a survey on a public survey service site but thought better of it... I can imagine the results. It is this principle which guides us in architecting our data collection, storage and retrieval plans for an organisation. We must specify what questions we will need to answer:

- about our organisation when it comes time to account for our activities
- to determine how we are performing
- to understand our place in the market
- to determine how to move forward
- in reply to our employees, customers, stakeholders, regulators, partners and even competitors
  
In this regard, organisations must carefully consider their data strategy. But we must also be aware that simply saving everything in every possible format *just in case* is not a wise strategy either. The burden would be onerous, the data would be monstrous, the information generated from it redundant and perhaps even contradictory. The cost would soon far outweigh the benefits. The inputs would balloon and the outputs shrink. Our output to input ratio would soon be underwater. Rather, the best way forward is an intelligent strategy to specify what data will be required in what volume at what frequency and in which format(s). 

[McKinsey](http://www.mckinsey.com/insights/high_tech_telecoms_internet/an_executives_guide_to_machine_learning "Machine Learning") had this to say on the subject of whom should be responsible for *marshalling data* in the organisation:

"Access to troves of useful and reliable data is required for effective machine learning, such as [IBM *Jeopardy!*-winning supercomputing cluster] Watson’s ability, in tests, to predict oncological [cancer] outcomes better than physicians or Facebook’s recent success teaching computers to identify specific human faces nearly as accurately as humans do. A true data strategy starts with identifying gaps in the data, determining the time and money required to fill those gaps, and breaking down silos [see our discussion of the ERP organisation [(take me there)](#erp_org). Too often, departments hoard information and politicize access to it—one reason some companies have created the new role of chief data officer [CDO as opposed to Chief Information Officer (CIO)] to pull together what’s required. Other elements include putting responsibility for generating data in the hands of frontline managers."

### Information Theory 
This all leads to thinking about how to think about information, and for this, we need to dig a bit deeper. Here we go. 

Our business dictionary defines Information Theory as: "Basic data communication theory that applies to the technical processes of encoding a signal for transmission, and provides a statistical description of the message produced by the code. It defines information as choice or entropy and treats the 'meaning' of a message (in the human sense) as irrelevant. Proposed together by the US mathematicians Claude Shannon (1916-2001) and Warren Weaver (1894-1978) in 1949, it focuses on how to transmit data most efficiently and economically, and to detect errors in its transmission and reception."

[[Interested?](http://www.businessdictionary.com/definition/information-theory.html)]

To really understand Shannon and Weaver (and a guy named Weiner), we need to look a little more deeply into the theory of information.

<a name="entropy"></a>
### Entropy and organisation and *potential* information
From another relatively old (1998) but still [excellent piece](http://www.sveiby.com/articles/Information.html), we find an introduction to the concept of *entropy*. 

“In the physical sciences the entropy associated with a situation is a measure of the degree of randomness. The second law of thermodynamics states that entropy always increases in the universe. High entropy equals high level of chaos.”  

Thus for decision making, entropy is the enemy. Entropy is *junk on the signal or static on the line.* It's your cell signal *breaking up*. It thwarts our efforts to make sense of a data transmission  and to translate data into information. While entropy and chaos and superfluous data provide richness in terms of the **volume** of signal being sent, they are useless in the context of seeking pointed, surgical, targeted information to answer a specific question and to guide action.

But this (rather dense – too much so to make it an [Interested?] link) article also raises some crucial points. Specifically that “The word information is derived from Latin *informare* which means "give form to". […] Most people tend to think of information as disjointed little bundles of 'facts'. In the Oxford definition of the word it is connected both to knowledge and communication. […] The way the word information is used can refer to both 'facts' in themselves and the transmission of the facts.”

The author continues. “The double notions of information as both facts and communication are also inherent in one of the foundations of information theory: cybernetics introduced by Norbert Wiener (1948). The cybernetic theory was derived from the new findings in the 1930s and 1940s regarding the role of bioelectric signals in biological systems, including the human being. The full title was: 'Cybernetics or Control and Communication in the Animal and the Machine'. Cybernetics was thus attached to biology from the beginning.

"Wiener introduces the concepts, amount of information, entropy, feedback and background noise as essential characteristics of how the human brain functions. [...] "The notion of the amount of information attaches itself very naturally to a classical notion in statistical mechanics: that of entropy. Just as the amount of information in a system is a measure of its degree of organisation, so the entropy of a system is a measure of its degree of disorganisation. [...]

Thus *entropy* = *disorganisation* = *information*. How odd. How can *dis*organisation yield more *information*? Disorganisation leaves a lot of room for interpretation. It allows for various different conclusions about the actual message or meaning of the data. The process of organising information reduces *dis*organisation in that superfluous elements (not related to a particular context, for example) are removed, yielding more targeted and focused data. 

We can also easily imagine how this notion jives with our contention that systems only add value if they either reduce input or augment output. The more parsimonious we are in architecting our data for specific purposes, the less input will be required in any specific context. Thus thinking about data is important, and planning its collection, storage and retrieval is pivotal to efficiency. Think of this as a geologist would inasmuch as more pure ore makes it easier to produce steel - there is less to discard. Or the more pure and clean a cell transmission is, the better understanding of the conversation in context.  

"What is information and how is it measured? Wiener defines it as a probability: One of the simplest, most unitary forms of information is the recording of choice between two equally probable simple alternatives, one or the other is bound to happen - a choice, for example, between heads and tails in the tossing of a coin. We shall call a single choice of this sort a decision. If we then ask for the amount of information in the perfectly precise measurement of a quantity known to lie between A and B [...] then the number of choices made and the consequent amount of information is infinite. [...] The quantity that we here define as amount of information is the **negative** of the quantity usually defined as entropy in similar situations." (article author’s bold)

The author continues: "Information is from its conception attached to issues of decisions, communication and control, by Wiener. System theorists build further on this concept and see information as something that is used by a mechanism or organism, a system which is seen as a 'black box', for steering the system towards a predefined goal. The goal is compared with the actual performance and signals are sent back to the sender if the performance deviates from the norm. This concept of negative feedback has proven to be a powerful tool in most control mechanisms, relays etc."

I will now muddy the already turbid water by introducing an opposing viewpoint, that of Claude Shannon, an eminent information scientist working at AT&T (the US telephone people) in the 1950s. The author writes:

"The other scientist connected with information theory is Claude Shannon. He was a contemporary of Wiener and as an AT&T mathematician he was primarily interested in the limitations of a channel in transferring signals and the cost of information transfer via a telephone line. He developed a mathematical theory for such communication in *The Mathematical Theory of Communication*, (Shannon & Weaver 1959). Shannon defines information as a purely quantitative measure of communicative exchanges."

So Shannon wasn't interested in *what* was communicated as much as he was in the *volume* or *occurrence* of communication. It's not *what* for him, but *that*. The author continues:

"[...] based on Shannon it does not matter whether we are communicating a fact, a judgment or just nonsense. Everything we transmit over a telephone line is 'information'. The message 'I feel fine' is information, but 'ff eeI efni' is an equal amount of information."

Note that the message (we cannot say the 'intended message' as we do not know the intention of the sender when sending it) 'I feel fine' is *almost* an anagram of the gibberish 'ff eeI efni' (one too many 'f' and no 'l'), but there may be other possible combinations of letters and spaces that would yield other equally viable messages *in this context*. For Shannon there is information richness in this disorganisation. The potential for many messages means there is more raw information in the message. As we will see, for business, this isn't a good thing. 

In Shannon's defence, the author goes on to write that "Shannon is said to have been unhappy with the word 'information' in his theory. He was advised to use the word 'entropy' instead, but entropy was a concept too difficult to communicate so he remained with the word. Since his theory concerns only transmission of signals, Langefors (1968) suggested that a better term for Shannon’s information theory would therefore perhaps be 'signal transmission theory'."

But we have a problem here. How can one theorist describe information as *organisation* and another describe it as *disorganisation*? The article continues with:

“Weaver, explaining Shannon’s theory in the same book: Information is a measure of one’s freedom of choice in selecting a message. The greater this freedom of choice, the greater the information, the greater is the uncertainty that the message actually selected is some particular one. Greater freedom of choice, greater uncertainty, greater information go hand in hand.”

Here comes the contradiction...

“There is thus one large - and confusing - difference between Shannon and Wiener. Whereas Wiener sees information as negative entropy, i.e. a 'structured piece of the world', Shannon's information is the same as (positive) entropy. This makes Shannon's ‘information’ the opposite of Wiener's ‘information’."

For Shannon, the content of a message (which he calls *information* but which I call *potential* information or simply *data*) is a function of volume. The bigger the message, the greater the information content. Shannon was a telephone company engineer, interested only in *that* a message was sent and not *what* message was sent. Shannon did not care *what* people were talking about on the phone but only *that* they were talking. The volume of data transmitted was more important than the actual content. 

And this makes sense if you think about it, from the point of view of a telephone conversation. Imagine you are in a phone call. In the background, you have music playing loudly enough for the other party to hear. While the music is not part of the conversation, *per se*, it becomes an element of the message being sent from you to the other party. It’s background and contributes to the richness of the signal.

But that music does *not* contribute to the clarity of the conversation. Music is competition for understanding the spoken word. 

Thus for Shannon, the more *entropy* (disorder – as in background music), the more disorganisation (lack of focus) and therefore the more *potential* interpretations could be made as a result of the message. If the background music was too loud, for example, the *intended message* might become garbled or unintelligible. This is not helpful for us in business, where we rely on *targeted* almost *surgical* messaging in order to make decisions that result in positive outcomes. This is especially true in information systems, where *interpretation* is not a particularly strong suit of software. Systems are rules based for the most part. They don't interpret well. Yet. 

Weiner, on the other hand, saw information as *negative entropy*, or positive organisation with structure, interpretability, less equivocation and noise and more certainty. This is the kind of message that business communication requires. Straightforward and to the point. No guessing about the information content of a data stream. *Less input for a given output.*

Entropy is the friend of *information volume* but the enemy of good *decision making*. In business, we need to keep the junk off the signal. Entropy is to be avoided. Structure is valued. Clean communication is the goal. Understanding is critical.

Thus Weiner is our man. We care both *that* messaging is occurring and *what* is being messaged. But Shannon was an inspiring academic. You’ll see reference to him in the section on ASCII. 

[Interested?]( http://www.kerryr.net/pioneers/shannon.htm)

### The five faces of information
Below is an infographic of sorts, showing the five faces of information. The faces and their characteristics are from a (highly recommended) 2012 book by Joel Katz entitled *Designing Information: Human factor and common sense in design* (2012, John Wiley & Sons).  

**Figure FFI. the five faces of information**

![Nature of information](https://raw.githubusercontent.com/robertriordan/2400/master/Images/nature_of_info.png)

I have arranged Katz’s five types into three groups, then ranked them in terms of their importance to us as students of business, but moreover, added a column delineating the approach taken by ICT to the challenge of how to deal with each group. Let’s begin at the bottom, and explain each in turn. 

As we walk our way up the scale, consider the context to be that our organisation is examining the results of an online poll asking visitors to a third-party website to rate the various products in our industry (let’s say it’s the toothpaste segment of the dental hygiene industry just to put a face on it). The results have been summarised by the polling firm and released at a press conference sponsored by one of our biggest competitors. This could be damaging to us. The opinions of visitor’s to the website have been measured with the online survey and the raw data have been contextualised by the polling firm and, worst of all, our competitor has provided their own spin. 

Though the data have been transformed into information in the context of our competitor’s survey and the analysis by the polling firm, for us, it’s still raw data. In order to make a decision whether to act on the release of the data, we need to put it in our own unique context. The data are inbound per Figure LJLJ. 

We need to contextualise the input data in order to create information sufficient to decide on an action. Here we go, starting from the bottom of the taxonomy. 

#### Non-information
Non-information is described by Katz as being possibly true (thus perhaps untrue), probably unimportant (perhaps we can ignore it) and/or possibly confusing (not well enough explained or contextualised to allow understanding). The ICT approach is likely to monitor and filter. If the information is deemed to be of no impact, the ICT response could be to filter it out and remove it from consideration by our company. If deemed potentially relevant, we might conclude that the survey information passes the non-information test, thus we retain it and move up the ladder. Our organisation might have a simple system in place to monitor electronic news services and social media flows looking for keywords (our organisation’s name for example, and perhaps negative and positive words in the context). Such monitoring might allow the system to decide whether to move the status of the news item up the scale to alert. News items that fail to meet a minimum criteria would likely be filtered out and not subject to further scrutiny.

#### Un-information
Next up is un-information as we move into the orange zone of importance. This is a curious category indeed, but information here is more important (potentially more impactful) than is non-information. It’s slightly up the *fudge scale* in terms of perceived truth, going from only ‘possibly true’ to ‘probably not untrue’ -- talk about a fine distinction! It remains probably unimportant, but has moved up the scale from confusing to possibly interesting. We have then, in this situation, some input data that is at least potentially interesting to us. The ICT response might be to alert the firm to the existence and location of the information, and wait for a knowledge worker to decide whether the information warrants escalation or disposal.

Assuming the knowledge worker decided on escalation, a further determination is necessary in order to decide on a response. We need to go up another rung on the ladder into the critical red zone. This is where context and information become critical. 

#### Disinformation
The first category in the red zone is ‘disinformation’, which Katz characterised as *deliberately not true* and very likely used tactically to intentionally mislead those consuming it. This information might well have been fabricated by our competitor to cast us in a bad light. This activity might even be illegal (libelous), and could be quite damaging to our reputation. In this context, we will want to take some action to protect ourselves. An ICT response could be to provide data to counter the disinformation and disseminate it to our stakeholders in order to protect ourselves from damage. 

Indeed, news organisations are continuously on the lookout for potentially damaging posts in the *Comments* section following many news stories (in fact, many online news publications have abandoned comments altogether owing to the near impossibility of monitoring and moderating them). The *Toronto Star*, for example, wrote the following in an editorial piece appearing on their online site on July 23, 2015, regarding such comment sections:

"Our moderation team spends the bulk of its time working on limiting the worst of the worst: racism, threats and personal attacks. This is done with help from readers who flag comments they deem out of line with our community guidelines and *software that scans for keywords we have identified as sensitive, and moves those comments to a ‘disabled’ queue until an editor can read them.*" [*emphasis* added]

#### Misinformation
If analysis determines that the information released by our competitor is not deliberately untrue, it might well be ‘misinformation’, described as definitely not true and important to avoid, in the sense that if the information were considered to be true (where it is, in fact, *untrue*) any actions taken under the erroneous belief that it *is* true would be inappropriate in the least and damaging at worst. This information is toxic and needs to be corrected before inappropriate action is taken by some party believing it to be true. Some party such as our customers! The ICT response could be to provide true information to counter the false assertions, which could be provided to our customers through various actions (email, Twitter, Facebook, etc.) made possible by stored electronic customer service records with contact information. 

#### Information
If the information is deemed to be not untrue (*ergo*, it’s true) then we move to the last level in the scale: information. If we make it up to this level, we know that the information released at the press conference is deemed to contain a true representation of our customers’ opinions of our products and, if released by our competitor, the results must be saying either something bad about us or something good about them. Whichever, the information is true and action must be taken to deal with the fallout. Several ICT strategies are possible, including targeted information campaigns designed to bolster our reputation and/or discredit the competition. 

## Contextualisation and the role of ICT
We can now provide a little more depth in terms of contextualisation and the role of ICT in the process. Examine Figure LLJJ below. It’s the same figure as LJLJ above, but with the ICT contributions overlaid on each element. 

**Figure LLJJ. Context with ICT contribution**

![ICT context](https://raw.githubusercontent.com/robertriordan/2400/master/Images/ict_context.png)

### System input
As our discussion moves from left to right in Figure LLJJ, each area is marked with a numbered black box for easier reference. 

We begin on the input end of the context system, and we begin pre-data, with measurement. **This is Area 1.** ICT provides several critical functions in terms of data measurement, beginning at the very basic level of *sensing*. ICT systems are deployed in a myriad of situations as sensors, simply sitting quietly and waiting for something to happen. Sensing a phenomenon is often akin to measuring it – a photon of light passing through a pane of glass, for example, causes a count of photons to tick up by one. An RFID (Radio Frequency Identification) system on a gas pump continuously broadcasts its presence, waiting for a customer’s matching key fob transmitter to spring to life in response to the signal. Then all kinds of commerce can happen. Sensors are everywhere from parking lots to soft drink dispensers and from washrooms to smart phones. And more and more sensors are being deployed all the time. We will return to this topic a number of times as we move forward, including later in this chapter when we discuss sensors and *beacons* in a bit more depth [(take me there}](#sens_beac). 

[Interested in measurement?]( http://en.wikipedia.org/wiki/Measurement)

ICT can of course measure things independent of sensing. A scale on the highway measures truck weight in the same way a bathroom scale measures our own weight, minus the incessant upward bias that shows our weight as impossibly and erroneously heavy… oops. Did I say that out loud? A radar gun measures speed. An elevator measures weight and will refuse to close its doors if overloaded. An accelerometer measures the speed of your finger swipe on a tablet computer in order to calibrate the throwing of a lance at an invading barbarian to protect your virtual village from attack. All are measurement devices. In performing this function, ICT systems must be engineered to respect the most critical tenet of measurement: *accuracy* and its constituents, *trueness* and *precision*, and including *reproducibility*. Once measured, the same data must yield the same results using the same technique. This is sometimes referred to as *reliability*. We discussed these things in Chapter 1 [(take me there)](#measurement). 

**On to Area 2**. Note also that the infographic implies that a lot of data just *fly right by* the system. This is true in two ways. First, data that has no known bearing on the current context has no value in this system. Data about vehicle fuel efficiency, no matter how accurate and reliable, simply cannot add to the context of a decision about the marketing of disposable diapers, for example (though both involve tailpipe emissions). It’s simply not in the domain. So this data flies right by. 

Next, however, is a more important aspect. There is simply so much data being generated every micro-second (5 Exabytes every two days according to Google chairman Eric Schmidt though this is contentious... [Interested?](http://techcrunch.com/2010/08/04/schmidt-data/)) that it’s impossible to capture it all in either it’s incredible volume or to process it in a timely manner before it becomes stale, so vast is the big data cloud. But what’s important is to capture an optimal amount in order to create the richest context and thus the most informed information, leading to the most accurate decisions about a course of action.

**Figure BGJ. Data generation**

![How much data is out there?](https://raw.githubusercontent.com/robertriordan/2400/master/Images/schmidt.png)

Another compelling visualisation of the amount of data generated is provided in Figure WTP.

**Figure WTP. Data generation**

![Data never sleeps](https://raw.githubusercontent.com/robertriordan/2400/master/Images/DataNeverSleeps.jpg)

This leads to the next contribution of ICT to the area of decision making and knowledge acquisition, that of dealing with the data itself. ICT systems can act as data filters, enforcing rules about relevancy, domain, accuracy, freshness and reliability. Data can be fed through systems and sorted, filtered, combined and then stored for later use. When required, systems can assemble and package data into usable formats (a spreadsheet format for example) and can then send them on their way to other systems for analysis (via network file sharing or even email). This is referred to as *marshalling*. At a low level, operating systems support marshalling through file management. Allowing us to store data under a single name in a place in either short- or long-term memory. In a very real way, data marshalling is like railway marshalling, where huge networks of track are used to *assemble* trains. Much like at the rail yard, ICT can store, slice and dice and then assemble and transport data. A one-stop shop for management. 

**Photo TTA. A railway marshalling yard in Germany**

![How much data is out there?](https://raw.githubusercontent.com/robertriordan/2400/master/Images/trainyard.jpg)

*Photo credit: http://d1.stern.de/bilder/stern_5/fotografie/2014/KW44/bahn_von_oben/bahn-von-oben-06_maxsize_2048_1536.jpg*

This can be understood in terms of the input/output dichotomy in a variety of ways. Can ICT reduce the amount of input required to produce a given output? Yes, in many situations. Even simple filtering and sorting, arranging and, in true Weiner fashion, reducing the uncertainty around input data, can contribute to more streamlined and efficient processes and thus satisfy the *less input* tenet of our *contribution to progress* rule.   

Moving to the area at the boundary between input and processing, we enter one of the most crucial and hot areas in ICT: *context and information creation*. We’ve spent some considerable time talking about context and information in context, so all we need here is to elaborate on how ICT contributes to and supports the activities in this area. **This is Area 3.**

<a name="sens_beac"></a>
#### Sensors and beacons 

Let's first step back and look at two pivotal pieces of technology which we have discussed briefly previously, but need to really *contextualise* in order o fully appreciate their impact. We're talking about *sensors* and their *less-capable* cousins, the *beacons*. Take a look at Figure SB.

**Figure SB. Sensors and beacons**

![How much data is out there?](https://raw.githubusercontent.com/robertriordan/2400/master/Images/sensors_beacons.png)

In making sense of Figure SB, let's begin with the walking icon at the left. The motion attendant upon walking triggers a *motion detector* (we've all seen these) which might, in this scenario, trigger a door to open (such as at the *LCBO* - where the height of design silliness often causes doors that do not need to be opened to swing violently agape) or a light to illuminate. This kind of device can be used to make decisions and cause action n the absence of synchronous decision making by humans. True enough, a person designed the sensor, created the rules about what the sensor should trigger (open a door, etc.) and wrote all the communications protocols and created the servo-mechanisms to effect the door opening etc. The system was created by humans specifically to make decisions and do work *without our intervention in real time.* Make no mistake, the door is opening as someone approached because humans decided that's what they wanted to happen. But they wanted machines to take care of all the mundane details. This kind of low-level, rules-based automation is everywhere.

But sensor data can also fulfill other ends. Data from sensors can be transmitted not only between other sensors in a network, but also to more central data-collection points and even to the cloud for storage, analysis or transmission to anywhere the *custodians* of the data deem appropriate. So sensor data can contribute enormously to the *context creation* of decision making. All this rich sensor data, growing exponentially, will radically change the machine world's ability to supplant human decision-making in the near term. Get ready for the age of machines precipitated by the so-called *internet of things*. Some call it the *internet of everything*. We will return to the issues around this revolution just below.

The less sophisticated correlate of the sensor is the *beacon*. While a sensor has the capacity to both read (sense) the environment and share (transmit) data (and fairly rich data), the beacon does no sensing at all. Beacons simply transmit a signal saying "Here I am!" either via their own power (and the issue of how to replenish that power, if required, is interesting) or via power transmitted to it by either a fixed or movable receiver that sends out a message like "Are you there?" Think of the *tap* capability of modern bank ATM and credit cards. Make no mistake. There is no magic in the *tap*. It's simply a way to get the card chip close enough to the reader/transmitter to allow a signal to be received by the card, with just enough power to allow the card to send its information to the reader; your information. There is no power source in the card. It's simply sending everything it knows in response to a query from the reader.  Other beacons, however, transmit data either continually or continuously. There are beacons at airports, sending signals to airplanes approaching for landing and giving exact coordinates. This is so pilots can safely land their crafts in situations of near-zero visibility. Beacons have been around since antiquity. Strategically-located fires were used by many an army to signal certain situations. Lighthouses are beacons, as are the flashing lights atop emergency vehicles. All send the signal "I am here!" or "Here I am!" 

Specialty applications and hardware/software has been developed by several firms, most notable Apple, with their *iBeacons*. According to Wikipedia (see *Interested?* link below), an industry has sprung up around iBeacons and several vendors have created iBeacon-compatible hardware transmitters, which are a class of *bluetooth* (a widely-used communications protocol) low-energy devices that broadcast their unique identity to nearby portable electronic devices such as smartphones, tablets and other devices, enabling them to perform actions (such as social media *check-ins* or to receive location-based messages when in close proximity to an iBeacon. A range of activities is possible including launching an app on a mobile smart device then in the vicinity of the beacon. Unlike a generic beacon (which is a one-way transmitter continuously repeating "Here I am!", the iBeacon requires the installation of a specif app on the receiving device (from the Apple Store or Google Play, for example) thus granting implicit permission for the app to track the owner's whereabouts.      

[Interested in Bluetooth low energy?](https://en.wikipedia.org/wiki/Bluetooth_low_energy)
[Interested in iBeacons?](https://en.wikipedia.org/wiki/IBeacon)

And a rich set of data can be accumulated over time by monitoring beacon movement. If a beacons transmits its location at a particular time and a new location at some time interval alter, much data can be generated by calculating the gradients across the time series. We can calculate absolute movement (was in Peterborough at 10:07 but in Ottawa at 14:30), trajectory (path of movement - in this case generally eastward), speed (a rough calculation unless we know the beacon's exact arrival time - it might well have arrived at 13:32 but we didn't get a measurement of its location until 14:30). So plenty of *secondary indicators*- can be generated from *primary observations* of a beacon's location. Moreover, when a number of beacons get together at the same time and place, well, all hell can break loose. Imagine the possibilities.

A wealth of data can be collected through embedding beacons in everything from our own bodies to our vehicles (both commercial and personal) and in our aircraft (the black box is a beacon). And again, all this data can be collected for further analysis, surveillance and prediction. This is true *context.*

[Interested?](https://en.wikipedia.org/wiki/Beacon)

This should help in explaining how ICT can assist in the contextualisation of the decision making through providing measured data to contribute to the richness of the solution space. ICT can accomplish this in a number of ways, and not just the traditional method of storing and marshaling legacy data (such as spreadsheet data of last year’s sales, for example), but by providing real-time, synchronous data *representing the current situation* (think sensors and beacons). Things such as identity authentication (allowing you to be in a certain place at a certain time based on either what you have – such as a password or a fingerprint – or what know, such as a password). Your location in time and space can be known and broadcast. 

And if it can be done for you, it can be done for others. Thus random gatherings of persons in a particular place and time can be sensed and utilised in assembling a context. Even simple things such as GPS and cell tower triangulation can locate you and others. This can facilitate all sorts of crowd-related things such as pop-up retail, policing and research into facility location. It’s also how things such as geo-fencing are accomplished. Geo-fencing has been in the spotlight lately with the use (and abuse) of a little app named *Yik-Yak*. 

[Interested?]( http://whatis.techtarget.com/definition/geofencing)

In this way, ICT can produce better or more output from the same input as the rich contextualisation of the decision space around *what's happening here?* leads to a more informed (better quality) decision from the same amount of input data.   

ICT can not only use and broadcast your location, it can also sense activity – *what* you’re doing. If your location puts you on the a highway, ICT can make a reasonable guess about what you’re doing, and the error around deciding what it might be can be reduced by examining (measuring) your velocity, for example. Insurance firms are increasingly using voluntary sensors to monitor your driving habits and providing insurance at a lower premium for "good" drivers. 

[Interested?](http://www.ecommercetimes.com/story/75600.html)

Systems can sense and determine lots of activity in which you might be engaged. It can therefore authorise you, based on location, time and activity, to perform certain tasks, such as allow you access to a secure facility or to take possession of rental materials or even rental vehicles. Unattended attendants. How cool. And way fewer resources are required to get those rental materials to you. More output for the same input. *Bingo*.   

Finally (but by no means exhaustively), ICT can contextualise your activities based on your or others’ previous pattern of activity at a time and place. A system might also make some reasonable guesses (though systems don’t guess things, rather they use rules at best and probability at least) about what you are likely to do *next* and anticipate and allow you to discover available services in your vicinity that are appropriate and appealing to you. Walking through the park? Ever rented a canoe to go paddling in the pond? How about a little text message on your phone from Joe’s Canoe Rental? This stuff, called *m-commerce* facilitated by *location services*, is popping up all over. And we’ve just scratched the surface. Mobile payment options now allow commerce between two parties with just cellphones, *disintermediating* financial institutions altogether. 

[Interested?](https://en.wikipedia.org/wiki/Mobile_commerce)

Note that a considerable part of this data that is brought to bear on a context has no direct relationship to the problem or challenge itself, *per se.* The decision space around a firm’s advertising spend this year would contain plenty of direct information, such as the firm’s liquidity position, sales figures, cost to advertise in various markets, customer demographics, competitor’s spend, etc. But there are plenty of other, more *contextual* variables that come into play. Things such as the firm’s mission and vision, ethics, brand value, consumer tastes and trends, strategy, resource capabilities and others. All of these things play into the decision but some are less, shall we say, tangible? These things provide context and they are the things at which ICT hasn’t been as good, historically. 

But we are on the verge of a change. Context is *the single biggest thing happening in ICT today*. The proliferation of sensors, so tiny and innocuous, is facilitating measurement at such a fine scale and with such sensitivity that soon we’ll have what one author referred to as *liquid information* (see below). Context is so compelling, let’s take a few more minutes with it. 

<a name="digital"></a>
###Digital vs. Analog
The distinction between the concepts of *analog* and *digital* is important here. The difference between them is akin to the difference between an integer (whole) number and a real number (with decimal precision of varying degrees). Analog is the *real numbers* of nature. Analog is the subtle curve and continuous and apparently seamless change we witness all around us; so subtle that sometimes it’s impossible to tell where one thing ends and another begins. 

Consider colour. In Figure ECC below, we see colour represented in two different ways, as discrete swatches representing the ROYGBIV colours of the rainbow and then, below that, as a continuum of those visible colours. 

**Figure ECC. Illustrating the difference between analog and digital**

![Analog and digital](https://raw.githubusercontent.com/robertriordan/2400/master/Images/analog_digital.png)

We can (most of us – some people suffer with some form of colour blindness [Interested?]( http://www.colourblindawareness.org/colour-blindness/types-of-colour-blindness/)) easily discern the difference between red and orange, or orange and yellow from among the boxes in the top row of the figure. And we could equally easily point to a green region or a blue region in the continuous strip of colour beneath. The challenge becomes specifying the point at which yellow becomes green, or exactly where indigo becomes violet. Try and pinpoint the exact location where yellow disappears and becomes green as we move left to right. The continuous nature of the colour strip makes it difficult to nail anything down, in fact. 

The colours in the upper box are represented using a specific method of reproducing colour called the RGB method, standing for Red, Green, Blue, two of the three primary colours. All colours, using this method, are produced as a function of mixing more or less of each of these three on a scale from 0 to 255. So the RGB for the colour red is 255, 0, 0. The maximum red (255) and no green or blue. Green is 0, 255, 0 and blue, 0, 0, 255. Of course there are plenty of shades and hues between these values, and indigo and violet off the right end of the spectrum are entities unto themselves. 

It doesn’t matter to us how this or other colour representation methods (such as CMYK, Pantone, etc.) actually work. What matters is that the continuous colour scale represented by the visible spectrum of the rainbow can be *sampled* and *digitised* such that we can work with it in a discrete way. The RGB scale itself produces tints and shades between which the naked eye could not discern. I challenge you to distinguish an RGB of 255, 0, 0 from 254, 0, 0. It would take an expensive display device to even produce and display an image capable of allowing us to discern that difference – but our eyes must be able to work at that level of precision. The message being, at some point or at some resolution, a digital representation becomes just as good as an analog one. It’s just as good because *we can’t tell the difference*. I’m not saying *better* or more *natural* but rather *just as good* for certain purposes. Take a look at Figure JP below. 

**Figure JP. Subtle differences in RGB**

![Can you tell the difference?](https://raw.githubusercontent.com/robertriordan/2400/master/Images/rgb.png)

The four reds are, for certain, red. The normally-sighted would have no trouble identifying each and all as being of the colour red from among the other colours of the visible spectrum. But the differences within the range of red are more difficult to detect. The leftmost (labeled 255) is the same red as in Figure ECC. I have altered the amount of red first from the max to 254, then to 245 and then to 225. The difference between 255 and 254 is so slight that it is nearly impossible to detect at the resolution of that figure on any of my devices or monitors with my eyes (such as they are). *Perhaps a slightly darker tint?* We begin to see a subtle but discernible difference at 245 (a clearly darker tint) while the difference at 225 is quite noticeable. For some purposes, a specific red of a specific hue might be required. For the vast majority of others, any of these reds will do. We would all stop at a traffic light if it showed any of these reds. And that’s the point.

Imagine now, sensing the difference between an RGB value of 255, 0, 0 and 254.5460274, 0, 0. Only in the most exacting and demanding of scientific or engineering contexts would such a difference be important (you see the use of *context* here as a *situation* in which certain things, such as precision, are critical). In 99.999999999999999% of cases where the two were compared, it would make no difference. We can *model* the analog nature of nature and get a *good enough* representation on a digital scale. Hang on. 

This concept is akin to *granularity*, and we will be introduced to it shortly. 

I’m hoping that by this point you are starting to see the light. The point being that computers represent everything as a series and a combination of binary digits (bits) and the more bits that can be dedicated to modelling something, the more information can be carried and the finer and finer can be the distinction between discrete elements. So fine, with such massive computing power as we now have, that eventually the binary representation of things becomes so rich, so fluid, that we can no longer tell the difference between the *real thing* and the binary/digital representation of it such that, well, it doesn’t matter at all. Witness Apple’s *retina display* which Apple claims to be so close to analog that our retinae are incapable of discerning anything finer. We can’t tell the difference.

And what self-respecting chapter on this topic would be complete without a reference to the Keanu Reeves / Lawrence Fishburne epic movie series *The Matrix*, first released in 1999, written and directed by The Wachowski Brothers? 

[Wikipedia Interested?]( http://en.wikipedia.org/wiki/The*Matrix) and/or [IMDB Interested?](http://www.imdb.com/title/tt0133093/?ref*=nv*sr*1)

And just to throw a wrench into the works, the notion of human *spirit* and a *soul* play large in this debate. If everything analog can ultimately be represented by a series of bits to the point where we can’t tell the difference, then are people simply huge binary machines, as are computers, made up of such minute and many binary objects that we simply can’t *yet* detect and measure them? Is the analog nature of nature just digital at such a fine level of precision that we simply haven’t seen it yet?  Take a biochemistry course. Take philosophy courses. Everyone should. You aren’t complete without them. The truth is out there ;) 

**Time for an [XKCD](http://xkcd.com/1519/ "XKCD Venus").**

![XKCD Screaming bird of truth](http://imgs.xkcd.com/comics/venus.png)

<a name="liquid"></a>
### Liquidity...

Let’s bring this home. A very old but again ever-so-interesting site (www.liquidinformation.org) has [this](http://www.liquidinformation.org/ana_digi_liqui.html) much to offer on the apparently spurious distinction between digital and analog as it pertains to ICT. 

First some definitions:
 
*Analog*: A mechanism in which data is represented by continuously variable physical quantities.

*Digital*: Of or relating to the fingers or toes. Using calculation by numerical methods or by discrete units.

*Liquid*: Flowing freely like water. Having the properties of a liquid: being neither solid nor gaseous. Smooth and unconstrained in movement. 
  
The article offers that “We have been brought up to believe that there is a total distinction, a wall of separation between digital and analog: The world is smooth and continuous; analog whereas computers are operating on discrete, black & white separate units; they are digital. And the twain shall never meet. 

The author writes: “Well you know, it just ain't so. Imagine a couple of small grains of sand. Digital, separate, discrete. Now add a couple more. And a couple more. Millions more. Billions. And you have a beach. An analog, a smooth continuous environment. [sic] 
  
“Everyday home and office computers, with capacities to manipulate literally billions of bits literally billions of times a second […] have gone the way of the grains of sand and are definitively not just digital anymore. And they have the potential to become more than analog. They have the potential to become, and make us, liquid.”
 
So the question for us is: At what point does digital become analog? At what point (threshold) does it matter to us? At what point can we detect? What resolution is important for us? This is akin to decimal place precision. How many decimals is it necessary to report in a table of financial ratios, for example, before the additional digit becomes meaningless? Think about this. Especially the Accountants and Finance people among you. How may decimal places, how much precision, is necessary? When is it *good enough*? 


#### Just-noticeable differences and your brain on music
This introduces the notion of *Just-noticeable Difference* or JND. And of course a just-noticeable difference is context dependent (isn’t everything?). The message, again, is that at some point in the digitisation of analog phenomena, the distinction disappears and we can’t tell the difference. JND has applications in many areas, not the least of which is marketing, where intensive research has been done into how much a product can shrink, for example, before potential buyers notice a change in package size.  

[Interested in Marketing?](http://www.scoop.it/t/psychology-of-consumer-behaviour/?tag=Just+Noticeable+Difference)

[Interested in Weber's Law of JND?](http://apps.usd.edu/coglab/WebersLaw.html)

[Interested in the Weber-Fechner Law of JND?](http://en.wikipedia.org/wiki/Just-noticeable_difference)

I’m reading a few books as I am writing this text. One that is particularly interesting to me is entitled *This is Your Brain on Music: The Science of a Human Obsession* by McGill neuroscientist and musician Daniel Levitin (2006, Penguin). He writes: “Less well known are the extraordinary advances we have been able to make in modeling how our neurons work, thanks to the continuing revolution in computer technology. We are coming to understand computational systems in our head like never before.” He continues that “Even consciousness itself is no longer shrouded in a mystical fog, but rather is something that emerges from observable physical systems.”

But what’s more interesting for us is the parallel he draws between sound and sight. Consider a motion picture. In case you didn’t know, a movie is made up of a series of still photos interspersed with black screens. When shown at the right speed, however, our visual system can’t tell that it’s a series of stills. Here’s what Levitin writes: “The lowest note on a standard piano vibrates with a frequency of 27.5 Hz. Interestingly, this is about the same rate of motion that constitutes an important threshold in visual perception. [...] ‘Motion pictures’ are a sequence of still images alternating with pieces of black film presented at a rate (one forty-eighth of a second) that exceeds the temporal receiving property of the human visual system. We perceive smooth, continuous motion when in fact there is no such thing actually being shown to us.”  So motion pictures are *just as good* as the real thing for us. Next time you're out on the road, take a look at the wheels of vehicles as they pass on the street. Note that for many, it appears as if you can see the disk brakes behind the wheels as if there were almost no spokes in the rims at all. Our eyes can't keep up with the spinning speed and our brain can't paint the picture fast enough for us, rather just showing us that there's nothing there. But we know different. 

Finally, just for fun...

**Figure WWE. The *wagon wheel effect*** 

![ICT context](https://raw.githubusercontent.com/robertriordan/2400/master/Images/WagonWheelEffect.gif)

The *Wagon wheel effect* is often seen in old Western movies where the wheels of a horse-drawn carriage sometimes appear to either be frozen or to be rotating counter to the direction in which the wagon is moving. This animated GIF illustrates the phenomenon. The speed of the "camera", moving towards the right, constantly increases at the same rate with the objects sliding to the left. Halfway through the 24-second loop, the objects appear to suddenly shift and head backwards.

*Credit: "WagonWheelEffect" by Ulillillia at the English language Wikipedia. Licensed under CC BY-SA 3.0 via Wikimedia Commons - https://commons.wikimedia.org/wiki/File:WagonWheelEffect.gif#/media/File:WagonWheelEffect.gif*

[Interested?](https://en.wikipedia.org/wiki/Wagon-wheel_effect)

We might also want to consider the concept of *resolution*. The resolution of a sensor is the smallest change it can detect in the quantity that it is measuring. Sensor resolution is being continuously improving.

[Interested?](http://en.wikipedia.org/wiki/Sensor)

Why matters all of this? It matters because as systems and sensors proliferate and their ability to communication improves, their ability to measure with more and more precision in more and more places at lower and lower resolutions at increasing rates of speed provides a richer and richer stream of input data to flow into and to generate our contexts. And that’s how decisions get made. As ICT is increasingly able to provide more and more context, machines are more and more able to work at a resolution level that is not only *good enough* for most situations, but maybe better than we humans can do... And with such great context, better and better information is generated and more and more decisions can be made for us by machines far below our level of consciousness. We'll be the spinning wheels. So fast, we can't see it happen. 

Returning to our ICT Context discussion, we need to talk about Information and what ICT can do for and with the creation of information out of data in context.  So back we go to Figure LLJJ.

We continue our explanation of this figure at the point where we left off. We now consider the role played by ICT in the creation of information in the context (the confluence of contemporaneous activity = all the other $hit going on at the same time) in which we find the facts = measured data. **This is Area 4**. Given all that is going on at the same time, we need to focus on the available data, decide whether it is applicable, usable, reliable, up-to-date, etc., then create information out of the data (remember that information is actionable whereas data is not) and decide whether to act or not and if so, how to act. All this is about decision making. Recall also that we need the optimal amount of data in the context in order to create the optimal information and inform our subsequent decisions. This is a lot to swallow all at once, but people do it all the time, and organisations are expected to do it in a timely, rational, inclusive, profitable, equitable, respectful, mindful and repeatable way. It’s no mean feat. 

ICT can help with this process by effecting data analysis, by synthesising (blending and amalgamating existing data and previous information pertinent to the task at hand) and by creating information (linking context to data). Information, once created, can be managed, stored, transported and reported upon by various ICT systems. It is functionally impossible for organisations to do even one of these tasks efficiently, given the volume of information while respecting the time constraints within which decisions must be made, without the substantial assistance of machines and systems. It’s simply impossible to keep up. 

### Where the magic happens
Beyond analysis and synthesis of existing data and information in context, ICT also *creates* information in context. **This is perhaps the most critical role of ICT in the entire process of knowledge acquisition.** *This is where the magic happens*. This is the confluence of fact and circumstance and measurement and existing knowledge coming together to produce an understanding, at some level, of the decision space. An understanding of the problem or challenge and of the circumstance surrounding it such that a decision can be formulated out of the new information created in the new unique context that is this moment in space and time. This is the way forward. 

I know this sounds eerily sci-fi or even spiritual. Not so. It’s something that we humans do continuously without even being aware of it. Machines do it also, but not as well as we. Yet. We are at the frontier of machines being able to recognise the boundaries of context. Able to ascertain what’s in and what’s out of scope. Creating information that bears on the problem and the solution space. Making sense of data in that context. This is to where ICT is going. We need to go along for the ride. *We need to keep up with technology or it will overtake our ability to realise value from its work.*

All the tools are available and in the public domain. The infrastructure already exists for a rapid and radical alteration of the computing landscape. In a recent issue of *The Economist* I read the following paragraph in an article entitled *Who's afraid of America?* on the subject of weapons technology: "The large lead that America enjoyed then [1980s] has dwindled. Although the Pentagon has greatly refined and improved the technologies [... they] have also proliferated and become far cheaper. Colossal computational power, rapid data processing, sophisticated sensors and bandwidth [...] are now widely available."  (The Economist, Volume 415, Number 8942, June 13th-19th 2015.) It's all out there and being leveraged for value creation. 

When the information is created within the context, ICT can then assist in the more mundane activities of filtering for relevancy (as we have seen in our discussion of the functions of ICT in the management of information, misinformation, etc.), marshalling, managing, transporting, and reporting on it to various functions within the organisation (or just to us, as users, if it’s a local bit of information such as how many Tweets you issued in the past 24 hours – this is information and 67 TPD (tweets per day) is a report of that information to you). 

Let's bring back Figure LLJJ for a refresher.

**Figure LLJJ. Context with ICT contribution**

![ICT context](https://raw.githubusercontent.com/robertriordan/2400/master/Images/ict_context.png)

Moving on, we next tackle the role of technology in making decisions (not acting on decisions just yet) but in *making* decisions. **This is Area 5**. How about a few simple examples of machines making decisions for us?  

Consider the heating and cooling systems we enjoy in our homes, vehicles, places of work and public spaces. They constantly monitor the ambient temperature and turn on and off to match some parameters we set (such as desired temperature). Next consider red-light traffic cameras - an entirely automated process from flash to cash. Then think of active all-wheel drive in a vehicle, constantly monitoring the wheel rotations on all four corners of the vehicle and adjusting the amount of torque sent to the wheel to compensate for slippage. Then think of cruise control and self-parking cars and adaptive volume levels in sound systems and the list goes on. All decisions made for us with no input other than, at times, setting some default values. Such decisions are made constantly and far beneath our consciousness or our ability to do the same. Imagine we drivers having to take over for an adaptive all-wheel drive system and manually making a thousand adjustments per second. Impossible. 

Thinking just about self-parking cars will help to cement the notion of *context* when you realise that this activity is all about context and almost nothing else. The context includes determining if the space is large enough to accommodate the parking vehicle, and then about exactly where in space the obstacles are (cars in front and/or behind) as well as location of the curb (if present), road conditions, ambient light, speed of approach, arc of attack and a myriad of other variables that describe the solution space for parking. Data is turned into information and then action. Context, in parking, is everything.  

ICT can both make decisions and *act on them*. **This is Area 6.** ICT can also record and store the conditions leading to the decision, what decision was made, and the context of the decision – such as the current thermometer reading and time/date when a decision to heat your home was made. In addition, ICT can report all this information to anyone or any system that requires it for whatever purpose. Moreover, is can *combine* such information with sensor data indicating who and how many people are in the room, the season, the external temperature, the lighting conditions, the relative humidity, the time of day and a myriad of other usage data that just flies by in our homes every second we are there. This is the genesis for the *Net* system, bought by Google in early 2014 for $3.2B USD.

[Interested?](https://nest.com/ca/)

ICT can also execute decisions (take action) as a consequence of human intervention (we can decide to send an email, for example, and the machine will do our bidding). When executing, it can record and store the context of the execution action as well as monitoring and policing its own actions and provide an audit of its activities - such as adherence to policies, for example. Everything about your email is recorded in multiple places.   

Moving to the right in Figure LLJJ, we encounter the work of ICT in sensing and storing the results of the actions executed by human and beast. **This is Area 7.** In this realm, ICT can both sense the results of actions and store those results for further analysis. At any juncture, as with any other activity undertaken by ICT, systems can report on their activities. To whom they report is a critical aspect of privacy. We will deal with this elsewhere in this manuscript. 

Moving again to the right in Figure LLJJ, we again encounter the impact of ICT on the Data and Information entities. **This is Area 8 and 9.** Here the actions and impacts are the same as in our previous discussion of data and information so we won’t belabour them again here. 

We come finally to the Knowledge component of Figure LLJJ. **This is Area 10.** Here we find the culmination of the process of acquiring knowledge from measurement of data and represents the ultimate human activity – the creation of order and reason out of action. ICT assists in many of the components of this activity, beginning with analysis of the stream of information emanating from the process. ICT can analyse existing knowledge as well as synthesise new and existing knowledge into unique nuggets of new information. Then it can manage, store and report on such knowledge.  

Maybe give some examples here? 

We next tackle the feedback piece and talk about the synthesis of new with existing knowledge and how it washes back over new and unique contexts to become input to the next set of decisions.


### And finally, the rapidly declining cost of computing

We end this chapter with an astonishing table from our friends at Wikipedia on the cost of computing. A *gigaflop* is described as *the power to execute one billion floating point calculations per second*. A floating point operation (or a FLOP) is any algebraic manipulation (+ - * /) involving two numbers which have decimal places. So 2 (two) is an *integer* but 2.0 (two point zero) is a *floating point number*. *Ergo* a FLOP might be the mathematical operation to multiply 1.0 by 1.0. Floating point arithmetic takes longer than integer arithmetic. A computer processor's power is often measured in *gigaflops*, providing a universal standard for speed of execution (the *s* at the end stands for *per second*), which is a proxy for computing power. The more flops a system can do in a second, the more powerful the system.

Below is a table equating gigaflops with dollars. How much did the computing power to effect one gigaflop cost, down through the annals of computer history? Here's what Wikipedia has to say (it will astonish you):

**Table GFP. Hardware cost to get a gig going**

| :- | :- | :- | :- | :- |
| **Date** | **Approximate cost per GFLOPS** | **Approximate cost per GFLOPS** [inflation adjusted to 2013 US dollars] | **Platform providing the lowest cost per GFLOPS** | **Comments** |
| 1961 | US $1,100,000,000,000 ($1.1 trillion) | US $8.3 trillion | About 17 million IBM 1620 units costing $64,000 each | The 1620's multiplication operation takes 17.7 ms. |
| 1984 |$18,750,000 | $42,780,000 |Cray X-MP/48 | $15,000,000 / 0.8 GFLOPS |
| 1997 | $30,000 | $42,000 | Two 16-processor Beowulf clusters withPentium Pro microprocessors ||
| April 2000 | $1,000 | $1,300 | Bunyip Beowulf cluster | Bunyip was the first sub-US-$1/MFLOPS computing technology. It won the Gordon Bell Prize in 2000.|
| May 2000 | $640 | $836 | KLAT2 |KLAT2 was the first computing technology which scaled to large applications while staying under US-$1/MFLOPS. |
| August 2003 | $82 | $100 | KASY0 |KASY0 was the first sub-US-$100/GFLOPS computing technology. |
| August 2007 | $48 | $52 | Microwulf | As of August 2007, this 26.25 GFLOPS "personal" Beowulf cluster can be built for $1256. |
| March 2011 | $1.80 | $1.80 | HPU4Science | This $30,000 cluster was built using only commercially available "gamer" grade hardware. |
| June 2013 | $0.22 | $0.22 | Sony Playstation 4 | The Sony PlayStation 4 is listed as having a peak performance of 1.84 TFLOPS, at a price of $400 |
| November 2013 | $0.16 | $0.16 | AMD Sempron 145 GeForce GTX 760 System | Built using commercially available parts, a system using one AMD Sempron 145 and three GeForce GTX 760 reaches a total of 6.771 TFLOPS for a total cost of $1090.66. |
| December 2013 | $0.12 | $0.12 | Pentium G550 R9 290 System | Built using commercially available parts. Pentium G550 & AMD R9 290 tops out at 4.848 TFLOPS grand total of $681.84 USD. |
| January 2015 | $0.08 | $0.08 | Celeron G1830 R9 295x2 System | Built using commercially available parts. Intel Celeron G1830 & AMD Radeon R9 295x2 tops out at over 11.5 TFLOPS at a grand total of $902.57 USD. |

[See the source for important notes and additional information.]

*Source: http://en.wikipedia.org/wiki/FLOPS*

In 55 years (shorter than my already short life) the equivalent cost to execute a billion FLOPs has dropped from USD $8,300,000,000,000 to USD $0.08. If that's not stunning and breathtaking then neither is (insert your most stunning and breathtaking thing here). Computing wins. 

While a bit dated, in a liquidinformation.org piece representing probably the absolute minimum achievable computing power, it is asserted that, for USD $1,000, we will be able to buy a computer with the computing power of an insect in 2018. That same $1,K will get us a mouse brain in 2030, a human brain is 2042 and *all human brains combined* by 2060. If you are a normal early 20s undergraduate student, 2060 is well within your lifetime. Think of it. Just *think.*  

[Interested?](http://www.liquidinformation.org/information_history.html) {read just the *Computation* sub-section including the *When will it end?* piece]

**Go Figure #1. Random SC advert**


<iframe width="800" height="491" src="http://www.powtoon.com/embed/fdjbos4uIyd/" frameborder="0"></iframe>
