# Chapter 5 - The personal side of ICT

## What's in ICT for me?

Let's start with a gentle introduction to *the digital divide*. 

**Figure KS. The digital divide**

![Figure KS. The digital divide](https://raw.githubusercontent.com/robertriordan/2400/master/Images/internet_usage.png)

Dilbert: Move this to discussion of robots

![Dilbert](https://raw.githubusercontent.com/robertriordan/2400/master/Images/dilbert_bankers.PNG)

Make sure to do the Nuclear reactor backup thing. 