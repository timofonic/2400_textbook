# Chapter 1 
### In which we introduce some critical context


![XKCD is life itself](http://imgs.xkcd.com/comics/cosmologist_on_a_tire_swing.png)

Source: http://xkcd.com/1352/

In the very beginning, at the murky start of time, there was nothing. Or perhaps even less than that. Maybe *negative* nothing? But of course negative nothing is something, so maybe not. Or maybe there was already everything, but not something? In any event, at the instant of the *Big Bang* (with apologies to Sheldon *et al*), there was instantly something. It was the very first binary data. Nothing, then something. Zero then one. Off became on. Dark begat light. Up became different from down. In became relative to out. You get the picture. 

And so it began. Data began to accumulate at that very instant.  

Brief
Basics
Backstory 

***Welcome to the study of ICT!***

Let's get the dry, most academic stuff possible out of the way early, with a quote from the *Association for Computing Machinery, the Association for Information Systems* and the *Association of Information Systems Professionals* who, from time to time, get together and bang out a position paper on what should be taught to students in information systems in various different university and college faculties and programs. The most recent such update is from 2002 (yes, really). The full document is available [here](http://www.acm.org/education/education/curric_vols/is2002.pdf "ACM Curriculum"). But this is is the gist of what you need to know about what they think ICT is (even though they don't use the term *Information and Communication Technology* or the abbreviation (ICT), I do). That aside, here's what they say, *for the record*:

*"Information Systems as a field of academic study encompasses the concepts, principles, and processes for two broad areas of activity within organizations: (1) acquisition, deployment, and management of information technology resources and services (the information systems function) and (2) development, operation, and evolution of infrastructure and systems for use in organizational processes (system development, system operation, and system maintenance). The systems that deliver information and communications services in an organization combine both technical components and human operators and users. They capture, store, process, and communicate data, information, and knowledge.*

*"The information systems function in an organization has a broad responsibility to plan, develop or acquire, implement, and manage an infrastructure of information technology (computers and communications), data (both internal and external), and enterprise-wide information processing systems. It has the responsibility to track new information technology and assist in incorporating it into the organization's strategy, planning, and practices. The function also supports departmental and individual information technology systems. The technology employed may range from large centralized to mobile distributed systems. The development and management of the information technology infrastructure and processing systems may involve organizational employees, consultants, and outsourcing services.* 

*"The activity of developing or acquiring information technology applications for organizational and inter-organizational processes involves projects that define creative and productive use of information technology for transaction processing, data acquisition, communication, coordination, analysis, and decision support. Design, development or acquisition, and implementation techniques, technology, and methodologies are employed. Processes for creating and implementing information systems in organizations incorporate concepts of business process design, innovation, quality, human-machine systems, human-machine interfaces, e-business design, sociotechnical systems, and change management.*

*"Information systems professionals work with information technology and must have sound technical knowledge of computers, communications, and software. Since they operate within organizations and with organizational systems, they must also understand organizations and the functions within organizations (accounting, finance, marketing, operations, human resources, and so forth). They must understand concepts and processes for achieving organizational goals with information technology. The academic content of an information systems degree program therefore includes:*

*[1] information technology,* 

*[2] information systems management,* 

*[3] information systems development and implementation,* 

*[4] organizational functions, and* 

*[5] concepts and processes of organizational management."*

We will endeavour to hit ACM proclamations [1 to 3] *very* hard, and [4 and 5] slightly less so, in this book. If you are in an ICT concentration or degree program, other courses of study will back fill the depth of knowledge required on the remaining requirements.

With the formalities out of the way, there are four important pillars upon which we will focus in this opening chapter. They form the basis of all subsequent discussion throughout this text. They are:

1. Are we talking data, information or knowledge?
2. Is what we are examining a tangible or an intangible? REMOVE AS PILLAR. 
3. Does this (technology/service/data/etc.) either shrink input or grow output or both?
4. Does this product/service/technology or strategy lead to a sustainable competitive advantage?

So let's get started.

## Pillar 1: Are we talking data, information or knowledge?

### What are data, information and knowledge?

Much of this preliminary section relies on work from a dated but still quite interesting article from *liquidinformation.org*. While quite compelling and thought-provoking, I don't agree with everything written there, but I was certainly stimulated by the piece.

[[Interested?](http://www.liquidinformation.org/information_history.html)]

First of all, many of the times the author refers to *information*, I contend they are instead referring to *data*. This distinction will become more clear as we progress. Here is a quote from the piece: 

“The first information was incidental. A cratered moon records the history of the impacts [of interstellar objects] but it does so by accident, as a by-product of the events, not *for* the events.” [emphasis added]

From that point on, we have, as a species, become all about communication. There is almost nothing that we do that does not involve information and communication. The technology piece came later, but provides the impetus for our study. While I am quoting slightly out of order, the same article also offers:

“Information’s natural state used to be one of motion, of activity. Information is generated by interactions, information *is* interaction, as without comparison, without a *context* [much more on *context* in the second chapter of this text], without interaction, there is nothing. There is no temperature with only hot. There is no darkness without light [...] Information cannot exist without *context*. And [...] it is important to point out that information has to be useful to exist. [...] Useful in a specific *context*. If it is not, it either does not exist, or it becomes negatively useful. As in noise.”

So only something that is *useful* can be called information. If something is not useful, it cannot, by definition, be information. Now that's a bold statement. Our task then becomes to discover what *useful* means. I will argue that useful has to do with either causing change or persisting the *status quo*. Decision and action cause either change or persistence (the absence of change when change *could* occur). Please stay for the show. 

Right up front let's get clear on something. **I do not encourage the use of Wikipedia to support academic work.** My use of this source in this book is only to encourage thoughtful discussion and not to provide any kind of proof for my proposed theories; you will always get my interpretation of the issue at hand. 

That said, Wikipedia has this to say on the topic: 

In the context of information as: ”... an influence which leads to a transformation.” It can be seen as “... any type of pattern that influences the formation or transformation of other patterns. In this sense, there is no need for a conscious mind to perceive, much less appreciate, the pattern. Consider, for example, DNA. The sequence of nucleotides is a pattern that influences the formation and development of an organism without any need for a conscious mind.”

I have a couple of comments on these web musings. To begin, the assertion regarding *influence* is closest to what I will show is most important in the context of business. Information leads to decisions which have consequences. It is the consequences of decisions that produce knowledge and inform the future. I would, however, maintain that the DNA example is flawed in that DNA is *deterministic*. There is no *interpretation* necessary in order for DNA to have an impact. It requires no *context* in which information is created. It expresses what it is destined to express with no need for any understanding or translation. It cannot do other than it was destined to do. DNA's actions are determined *a priori* (before the fact) and in this sense DNA is *data*, as I will show later, and *not information.*

The Wikipedia article goes on that “If, however, the premise of 'influence' implies that information has been perceived by a conscious mind and also interpreted by it, the specific context associated with this interpretation may cause the transformation of the information into knowledge.” This is where you and I come in. We are the interpreters. The article continues that “Complex definitions of both 'information' and 'knowledge' make such semantic and logical analysis difficult, but the condition of 'transformation' is an important point in the study of information as it relates to knowledge.”

We will spend some time on just such a transformation and why it’s important for business. 

The article maintains that: “In this practice, tools and processes are used to assist a knowledge worker in performing research and making decisions, including steps such as [my comments in parens]:

- Reviewing information in order to effectively derive value and meaning [assessing the outcomes of decisions]
- Referencing metadata if any is available [metadata is literally *data about data* - the list of numbers you've called on your cell is metadata]
- Establishing a relevant context, often selecting from many possible contexts [this will be important for us]
- Deriving new knowledge from the information [critical in the business context]
- Making decisions or recommendations from the resulting knowledge [the feedback from previous knowledge acquisition – this is how knowledge grows]

“Stewart (2001) argues that the transformation of information into knowledge is a critical one, lying at the core of value creation and competitive advantage for the modern enterprise.”

Well you can't get any more impactful that that. Knowledge creation from information is at the very core of what makes business successful.

[Interested?](http://en.wikipedia.org/wiki/Information)

### The data, information and knowledge hierarchy
To begin, when studying Information Systems (or the broader term that I prefer - *Information and Communication Technology* or *ICT*), it is crucial to define what we mean by the pieces. We need to carefully specify the differences between the **Big Three** of *data, information* and *knowledge*. We need also to examine the intimate interrelationships between these levels of the hierarchy in order to understand what ICT really is and what impact it has on our private and public lives.

Let's first deal with a potential controversy. You might have heard (more likely you will hear or see in other places during your academic career) this hierarchy referred to as the Data, Information, Knowledge and _Wisdom_ hierarchy. There are problems with this particular characterisation so instead, I describe the Data, Information and Knowledge hierarchy here. 

[Interested?](https://hbr.org/2010/02/data-is-to-info-as-info-is-not)

### Measurement
<a name="measurement"></a>
We first need to carefully consider what is meant by the term *measurement* as measurement is necessary in order for data to be useful. Measurement makes data tangible and gives it value. Under the topic of *Accuracy and precision*, Wikipedia (accessed June 17, 2015) offers the following discussion seedlings:

#### Accuracy
“In the fields of science, engineering, industry, and statistics, the accuracy of a measurement system is the degree of closeness of measurements of a quantity to that quantity's actual (true) value."

#### Precision
"The precision of a measurement system, related to reproducibility and repeatability, is the degree to which repeated measurements under unchanged conditions show the same results. Although the two words precision and accuracy can be synonymous in colloquial use, they are deliberately contrasted in the context of the scientific method."

They continue that, “A measurement system can be accurate but not precise, precise but not accurate, neither, or both. For example, if an experiment contains a systematic error, then increasing the sample size generally increases precision but does not improve accuracy. The result would be a consistent yet inaccurate string of results from the flawed experiment. Eliminating the systematic error improves accuracy but does not change precision."

Furthermore, “A measurement system is considered valid if it is both accurate and precise. Related terms include bias (non-random or directed effects caused by a factor or factors unrelated to the independent variable) and error (random variability).

#### Another view
According to ISO 5725-1, Accuracy consists of Trueness (proximity of measurement results to the true value) and Precision (repeatability or reproducibility of the measurement)

According to ISO 5725-1, the terms trueness and precision are both used to describe the accuracy of a measurement. Trueness refers to the closeness of the mean of the measurement results to the actual (true) value and precision refers to the closeness of agreement within individual results. Therefore, according to the ISO standard, the term "accuracy" refers to both trueness and precision.

**Figure SMC. Low accuracy: poor precision, good trueness**  

![](http://upload.wikimedia.org/wikipedia/commons/thumb/1/10/High_accuracy_Low_precision.svg/200px-High_accuracy_Low_precision.svg.png)

Looking at the *target practice* image above, we immediately see that no one shot is exactly centre. This is to be expected and might even be considered a random (though intended) outcome. It's pretty hard to hit dead centre on any attempt. So we can't give any of them good marks for *precision* - the degree to which they hit the centre of the target. But all is not lost. We next observe that the four attempts are, in general, at about the same distance from dead centre. So none is a great shot, but considered together, they seem to have some consistency. So while not completely accurate, they are at least *true to each other*. This gives them marks for trueness. They err by the same amount in different directions. So, *on average*, a good set of results, though no one was accurate. For why this is important, see the *old joke* below.  

**Figure BFHC. Low accuracy: good precision, poor trueness**

![](http://upload.wikimedia.org/wikipedia/commons/thumb/3/3a/High_precision_Low_accuracy.svg/200px-High_precision_Low_accuracy.svg.png)

Here we have a different situation. While again not even one attempt was dead centre, all four were clustered closely to the left and below the *pay dirt* area. Still a situation of *low accuracy* but not for the same reason as above. In this case, the *precision* is excellent. The attempts are very closely clustered together and are thus very precise. But they are *consistently* wrong. As if some technical difficulty is introducing bias into the process (a crooked site perhaps?) causing an otherwise great set of attempts to be consistently wrong. But unlike the first low accuracy situation we looked at above, the attempts are not considered to be *true* on the whole. This is because their *average* distance from dead centre is considered to be large. 

Note in the first case above if you were to calculate an average difference from the centre, you would note that they are all at least touching the first outer ring of the target (though it is enticing to note, it doesn't matter that they are nearly at the 12, 3, 6 and 9 o'clock positions). Calculating an average distance for all four would yield a mean very close to 1 (one) if we were calibrating the rings as 1, 2, 3, etc.  

Now consider the second case. All are clustered in one spot AND two of the attempts are clearly farther away than any in the first example. The average difference would, therefore, be greater. It appears to be splitting hairs, but it's the best I've got. I'll try to photo some of my own for the next edition of this text. 

*Source: [Wikipedia accuracy and precision](http://en.wikipedia.org/wiki/Accuracy_and_precision). Image sources: Credit DarkEvil (public domain)*

This might clear up some of the confusion: "According to ISO 5725-1, the general term *accuracy* is used to describe the closeness of a measurement to the true value. When the term is applied to sets of measurements of the same _measurand_ [see this reference to understand the term *measurand*](http://en.wiktionary.org/wiki/measurand), it involves a component of random error and a component of systematic error. In this case *trueness* is the closeness of the mean of a set of measurement results to the actual (true) value and *precision* is the closeness of agreement among a set of results." 

**Figure SR. Precision and trueness of measurement**

![Accuracy](https://raw.githubusercontent.com/robertriordan/2400/master/Images/accuracy_new.png)

Figure SR might help in that it clearly shows precision as being *how close a measure is to the true value* whereas trueness is how closely each measure in a set of measures agrees with the others. And maybe it doesn't help. I'm doing my best. I'm sure you will LMK if you don't get this.   

The Wikipedia article continues that, “The terminology is also applied to indirect measurements—that is, values obtained by a computational procedure from observed data. In addition to accuracy and precision, measurements may also have a measurement resolution, which is the smallest change in the underlying physical quantity that produces a response in the measurement. In numerical analysis, accuracy is also the nearness of a calculation to the true value; while precision is the resolution of the representation, typically defined by the number of decimal or binary digits.”

Each of these will become important at different junctures in our study. Measurement resolution will appear again when we discuss *sensors* and resolution of representation when we talk about the shrinking divide between *digital and binary*. 
 
[Interested?](http://en.wikipedia.org/wiki/Accuracy_and_precision)  

*All this measurement talk reminds me of the great old joke about the doctor, the lawyer and the statistician on a hunting trip. While hiding in the bushes, they spot a big buck deer at a distance of 25 metres directly in front of them. The doctor sizes up the distance and wind speed, then aims and fires an arrow at the deer. His shot zooms one metre to the rear of the unsuspecting creature. The lawyer does some quick calculations, then aims and fires. His shot zips one metre in front of the deer. Our statistician friend jumps up, raises his hands in victory and shouts "GOT ‘IM!!"  (No animals were hurt in the process of creating this book.)* 

### Back to the Big Three
With all the measurement issues behind us, we now need to carefully define what each of *data*, *information* and *knowledge means in its own right. We can then examine the hierarchy that evolves when discussing these big three in the context of decision making. Oh yes, and *context* will be critical too. We will deal with that in some detail in Chapter 2.

### Data

**Figure STT. Definition of data from Visual Thesaurus**

![visualthesaurus.com Definition of data](https://raw.githubusercontent.com/robertriordan/2400/master/Images/vt_data.PNG)

The excellent website (or service, really) [visualthesaurus.com](http://visualthesaurus.com) shows the visualisation, above, of the look-up term *data* once expanded by clicking on the red node.  

The interpretation of the visual goes as follows:

* A solid line between two terms denotes a relationship between a meaning and a word. Thus the term at the distal (far) end of a solid line can be used to express the meaning of the defined word.
* A dashed line indicates a _type of_ relationship. Thus the term at the distal end of a dashed line indicates that this construct is a type of the term at the proximal (near) end of the dashed line. So *accounting data* is a *type of* data. Self-evident, really. 
* A dashed red line (not shown in this visual but appearing in others below) indicates an opposite. So a red line appearing between the terms *near* and *far* would indicate that they are a negation of each other. 

The Visual Thesaurus defines data as: "A collection of facts from which conclusions may be drawn." Hold onto this definition as we further analyse the complex relationship between data and information.

The visual on *data* above implies (states, really) that data and information are synonymous; that they can be used to describe each other, and thus that they are equal. My definition, which clears up some of the ambiguity in the terms *data* and *information*, is: 

*Data is pre-information*. It is potential information; a collection of one or more facts, figures or measurements of some entity whether tangible or not. *Information*, on the other hand, is *actionable data*; data embedded in a *context* that allows us to make a decision on a course of action. 

We have already considered *measurement* in some depth [(take me there)](#measurement) and we will spend some time unpacking the concepts of *entities*  and *tangible/intangible* in due course. 

The online [Business Dictionary](http://businessdictionary.com) defines data as:

1. "Information in raw or unorganized form (such as alphabets, numbers, or symbols) that refer to, or represent, conditions, ideas, or objects. Data is limitless and present everywhere in the universe. See also information and knowledge.", and
2. "Computers: Symbols or signals that are input, stored, and processed by a computer, for output as usable information.”

I respectfully disagree, ever so slightly, with these two definitions. Specifically, in definition #1, the word *information* should not appear. It is tautological to define data as information and information as data. Rather, both definitions should begin with the words in the second definition, specifically that data are *symbols and signals* or indeed drawings or sounds or signs or even facial expressions that carry within them the *potential* for information but not information *per se*. Data is raw and lacks context. A column of digits in a spreadsheet with no row header to indicate what the data *mean*, for example. I maintain that information occurs in the *interpretation* of data when it’s *located in context*. Read on.

Note the definition of information below, from the same source, and note that one of the necessary elements of information is that it be presented within a context that gives it meaning. 

[Interested?](http://www.businessdictionary.com/definition/data.html)

### Levels (types) of data

#### NOIR taxonomy:

1. Nominal
2. Ordinal
3. Interval
4. Ratio

Roughly corresponds to LATCH (discussion of this taxonomy follows directly below NOIR) but affords understanding of intrinsic value of data type

##### Nominal 

Name only – no intrinsic value - such as Chevrolet, Ford, Ferrari, Male, Female

Lowest information *affordance*

Can only be reported or used as a category for bins (as in throwing things in bins for counting and then that counts become Ratio – for example you can say that “35,000 times more people own a Chevrolet than a Ferrari.” – but that’s a count and not an attribute of *Chevyness* or *Ferraritude*).

No algebraic operations possible - no math other than comparing frequencies between categories. 

Corresponds to Location (in certain instances but not strictly to GPS which would be Interval-level data) or Attribute in LATCH. 

##### Ordinal 

An intrinsic (implied) ordering but cannot specify the *exact increment/decrement* implied by changes from one category to the next

A *Likert* scale in a survey for example - "On a scale of 1 to 5, how happy are you?" Is someone who answers *2* only *half* as happy as someone who answers *4*? The scale is not sensitive enough, or well enough *calibrated*, to make these determinations. We can report *frequencies* along latent dimensions only - for example "Twice as many people in our survey reported being somewhat or very *happy* as opposed to those reporting being somewhat or very *unhappy*." But honestly, we haven't even *operationalised* the concept of *happy* here.   

Another example: "How satisfied are you with this book?" There is no real objective way to measure the underlying dimension of *satisfaction*. We can ask if you are *Not at all, Somewhat, Neither satisfied nor dissatisfied,* etc. This gives us an indication and we can compare person to person at one point in time or with the same person over time. We just can't say that we've measured *satisfaction* in a truly rigorous way. 

Researchers call these underlying states *latent dimensions*. We often look for things that *act like the latent variable* and measure it, instead, as a *proxy* for the unmeasured variable. These *indicator variables* are *correlated * with our variable of interest - so they vary in a known way with our latent variable - but are not exact measures of the dimension that we are interested in measuring. (Don't leave me yet, it's just getting good.) So if we're measuring *happiness* we might measure the number of times a sample of people smiles in a day along with the number of laughs and warm embraces, etc. These things, are not *exactly* the same as happiness, but together, they can make a compelling argument for being able to tap into the underlying river of emotions that are expressed as happiness, sadness, anger, etc.     

Finally, with ordinal variables, we cannot do algebraic operations on categories except to report frequencies/counts, which are then Ratio. Corresponds to Hierarchical in LATCH.

##### Interval 
Similar to Ordinal but the exact difference in increments between categories can be specified but has an arbitrary zero value

For example temperature:

23oC is exactly one degree warmer than 24oC but 0oC is arbitrarily simply the temperature at which fresh water freezes at sea level

Can do some algebra (addition/subtraction)

Corresponds to Time and Hierarchical in LATCH

##### Ratio 

Is the highest level of measuerment – affords the most opportunity to understand the underlying dimensions captured therein

Age, dollar values, counts of discrete items (number of events, purchases, people, goods... expressed as a ratio to each other or to a total)

Interval data are exact and have a real zero thus any and all algebraic operations are possible (addition, subtraction, multiplication, division and exponentiation)

Corresponds to Hierarchical in LATCH

### LATCH Taxonomy

All data can be categorised according to one of the LATCH descriptors. Note the relationship to the more technical NOIR categories.

**Table XYZ. Comparing measurement taxonomies** 

| :- | :- |
| **LATCH** | **NOIR equivalent** |
| **L**ocation | **N**ominal |
| **A**lphabetical | **N**ominal |
| **T**ime | **O**rdinal, **I**nterval |
| **C**ategorical | **N**ominal, **O**rdinal |
| **H**ierchical | **O**rdinal, **I**nterval, **R**atio |

##### L -  Location data

Maps, broadly defined (from a location on the body for a surgeon to a room in a building for a delivery all the way up to quadrant in the universe)

GPS devices make use of data tagged with *geo-locale* information (latitude by longitude)

##### A - Alphabetic data

Very frequently used, often as a secondary attribute

Good for categorising enormous datasets such as a phone directory or an employee directory or a social media *friend* list

Since virtually everyone understands alphabetical order, this method is often used when consumers of data are not familiar with the subtle nuances of other methods of categorisation - *"When all else fails, alphabetise!"*

##### T - Time data

Anything that is tracked chronologically and has either a measurable start point and/or end point can be expressed in terms of time

The Vietnam Memorial in Washington DC, for example, is ordered by time, affording viewers the added information of seeing others who died at roughly the same time, and allowing context with other events in the news

##### C - Categorical data

That which falls into mutually-exclusive categories such as:

- Gender
- Paint colour
- Options group on a website
- Degree programme

Works well when trying to make sense of information of similar importance without an implied order or rank

##### H - Hierarchical Data

That which is measured in comparison with others and which can be compared in terms of quantity and/or quality, such as:

- Temperature
- Weight
- Dollar value

### Information

**Figure LP. Definition of information from Visual Thesaurus**

![visualthesaurus.com Definition of information](https://raw.githubusercontent.com/robertriordan/2400/master/Images/vt_info.PNG)

The Visual Thesaurus defines information, in conjunction with the visual above and ignoring the definitions pertaining strictly to the study or administration of justice and the law (with apologies to LJ), as:

1.	A message received and understood [YES]
2.	Knowledge acquired through study or experience or instruction [YES]
3.	A collection of facts from which conclusions may be drawn [MAYBE]
4.	The psychological result of perception and learning and reasoning [MAYBE]
5.	In communication theory, a numerical measure of the uncertainty of an outcome [MAYBE] 

We need some unpacking here. In the [square brackets] at the end of each definition of information above, I have added my little editorial comment about whether or not I agree with the characterisation. Information can *clearly* be characterised as *received and understood* as understanding assumes that the context of the message, it's meaning and its intent has been synthesised. If something is *understood* then the *context* is rich enough to allow a decision to be made based on the *content*. Data alone cannot do this.

The second and fourth definitions are acceptable, on the whole, , especially, as we will see, as they relate to *experience and learning from experience*. More to come on this.

The third definition is shading into murky territory. A collection of facts, in and of itself, cannot afford a conclusion. That is the domain of information. The facts might well form a context from which information can arise, but alone, no.

The fifth definition as it applies to *entropy* is dealt with in detail in Chapter 2.  

Our business dictionary source offers that *information* (not data but information) is:

"Data that is:  

1. accurate and timely, 
2. specific and organized for a purpose, 
3. presented within a context that gives it meaning and relevance, and 
4. can lead to an increase in understanding and decrease in uncertainty."

They go on to offer that “Information is valuable because it can affect behaviour, a decision, or an outcome. For example, if a manager is told his/her company's net profit decreased in the past month, he/she may use this information as a reason to cut financial spending for the next month. A piece of information is considered valueless if, after receiving it, things remain unchanged. For a technical definition of information see information theory."

I must respectfully disagree with several of the assertions in this definition. Specifically, they define *data* as *information*. The two are clearly not synonymous. To begin the definition of information with the assertion that it is *data* is clearly not helpful for us. Furthermore, data doesn't have the potential to change things (or to lead to change). Only information can lead to change through informing our decisions. 

[Interested in Information Theory?](http://www.businessdictionary.com/definition/information-theory.html)

Let’s be clear: **Data that can be measured and put into context leads to information, which facilitates decision making.** Data and information are not the same thing. We will look at the distinction in some detail in Chapter 2. 

Finally, it is categorically *not* necessary for information to lead to change. Information leads to decisions, which are binary by nature. One of the two possibilities when informed about something is the decision *to do nothing!* And that’s perfectly fine. Maybe that’s the right decision.

Now we're getting somewhere.

[Interested?](http://www.businessdictionary.com/definition/information.html) 

### Knowledge

**Figure KS. Definition of knowledge (know) from Visual Thesaurus**

![visualthesaurus.com Definition of know](https://raw.githubusercontent.com/robertriordan/2400/master/Images/vt_know.PNG)

How about knowledge? The [Visual Thesaurus](http://www.visualthesaurus.com/ "Visual Thesaurus") cogently defines *know* (a richer context is provided than if we asked for a definition of knowledge) as: "The psychological result of perception and learning and reasoning." So this definition explicitly excludes machines from having the capacity to *know something* since it refers to a "psychological result." Pity...  

But let's look at this visual for a second. Note how rich it is. Note how many nodes are of interest to us here. 

Next let's look at what our Business Dictionary source offers for the definition of *knowledge*. Their first definition is what we are looking for and here it is:

"General: Human faculty resulting from interpreted information; understanding that germinates from [the] combination of data, information, experience, and individual interpretation. Variously defined as, "Things that are held to be true in a given context and that drive us to action if there were no impediments" (Andre Boudreau). "Capacity to act" (Karl Sweiby). "Justified true belief that increases an entity's capacity for effective action" (Nonaka and Takeuchi). "The perception of the agreement or disagreement of two ideas" (John Locke). In an organizational context, knowledge is the sum of what is known and resides in the intelligence and the competence of people. In recent years, knowledge has come to be recognized as a factor of production (see knowledge capital) in its own right, and distinct from labor."

[Interested?](http://www.businessdictionary.com/definition/knowledge.html)

Some very rich definition and context here. From this we can glean that *data is useless unless in a context*, which allows it to become information. Information drives decisions and decisions drive action or inaction, in the rich context of previous information. Collected information from this action, previous actions and from all other sources of knowledge becomes new or evolving knowledge and confers the ability to pass judgment on the previous three. 

Again, we consider these important concepts in detail in Chapter 2. 

#### Important takeaways

- Measurement is complex, critical and has several quality metrics associated with it
- Data and information are not synonymous 
- Information is created in the presence of data in context
- Decisions arise from information and a decision to *not* act is as valid as one *to act*
- Knowledge arises from accumulated experience with the outcomes of decisions 

### Communication

Communication is the essence of our being. Communication is almost all we do. Our friends at Wikipedia have this to say about that: "Communication (from Latin commūnicāre, meaning 'to share') is the activity of conveying meaning through a shared system of signs and semiotic rules." 

[Interested?](http://en.wikipedia.org/wiki/Communication)

Semiotics is the study of signs and symbols in communication. 

[Interested?](http://www.amazon.ca/This-Means-That-Users-Semiotics/dp/1856697355/ref=sr_1_4?ie=UTF8&qid=1427474025&sr=8-4&keywords=semiotics)

We can further examine a subset of what [dictionary.com](http://dictionary.reference.com/browse/communication) offers:

1. The act or process of communicating; fact of being communicated. 
2. The imparting or interchange of thoughts, opinions, or information by speech, writing, or signs. 
3. Something imparted, interchanged, or transmitted. 
4. A document or message imparting news, views, information, etc. 
6. Communications - means of sending messages, orders, etc., including telephone, telegraph, radio, and television. 
7. Biology: a) activity by one organism that changes or has the potential to change the behavior of other organisms; b) transfer of information from one cell or molecule to another, as by chemical or electrical signals.

Now isn't that interesting, the Biology parts? That's exactly what we are saying about *Information*. Specifically that we (or at least I) have here defined information (that it emerges from data in a context) as something that is communicated and can cause *change*. So in nature, a large part of communication involves change, and such communication is achieved by the exchange of *information*. Indeed a website article we will examine in depth in Chapter 2 on *Liquid Information* offers on the [history of information]((http://www.liquidinformation.org/information_history.html) "History of info from liquid information") that as life appeared on earth it required “[…] storing information about how to replicate something, how to replicate itself. Information now served a purpose...”. Life requires information because life requires action to perpetuate itself. The purpose of life is perpetuation. 

Thus communication allows humans to fulfil their only real purpose on this planet: to remain on this planet. Too contentious for a business course in ICT? Expand your horizons. Read lots. Think lots. Then we can talk.  

How pervasive is information communication? What's the *coverage* in Canada? Take a look at this simple interactive map:

**Figure FMAP. Cellular coverage in Canada**

<iframe src="https://www.google.com/maps/d/embed?mid=ziiXzGEk59_I.kvJPNhGl3KKk" width="640" height="480"></iframe>

*Source: http://www.itbusiness.ca/news/interactive-map-inside-canadas-wireless-landscape/56833*

An excellent introduction to the history of communication is available in Wikipedia. 

[Interested?](https://en.wikipedia.org/wiki/History_of_communication)

**Flesh this out:**
Here is an interesting depiction of the [Internet Exchange Points](http://www.internetexchangemap.com/) in the world . Find the one closest to you. What is an internet exchange you ask? [Interested?](https://en.wikipedia.org/wiki/Internet_exchange_point)

And here is an interactive map of the [submarine cables](http://www.submarinecablemap.com/) in the world. Why is this important? [Interested?](http://www.cbc.ca/news/technology/submarine-cables-explainer-1.3289954)

### What is technology?

Wikipedia offers that Technology is "...the collection of tools, including machinery, modifications, arrangements and procedures used by humans." Note the breadth of this definition. How do we understand *tools* in this context, for example? It is not inconceivable that a tool could be a hammer, a cell phone, a traffic signal, a spoken language or even applied mathematics. So technology is broad indeed.

[Interested?](http://bit.ly/139Y25z)

The online [Business Dictionary](http://businessdictionary.com) also takes a broad view, and delineates categories and taxonomies. They write that technology is "The purposeful application of *information* in the design, production, and utilization of goods and services, and in the organization of human activities." (emphasis added)

<a name="tangible"></a>
#### Tangible versus intangible
Further, they provide the following detail, while splitting the taxonomy into two additive (presumably) sub-categories. Thus technology can be either tangible (can be touched, seen, etc.) or intangible (not physical -- as in the sense of services) (see also the more extensive treatment of this topic in the major Theme 2 section below). Further, they rank three levels of interaction between technology and the matter and forces with which it interacts. So we can have tangible technology that is either high, medium or low, with the same three levels for intangible. They write that: "Technology is generally divided into five categories [my explanations/examples in brackets]." The list begins with the distinction between tangible and intangible:

1. Tangible (blueprints, models, operating manuals, prototypes) [can be touched, take up space, etc.]
2. Intangible (consultancy, problem-solving, and training methods) [do not exist in the physical realm; do not exist until consumed...]

And then continues in term of *sophistication*:

1. High (entirely or almost entirely automated and intelligent technology that manipulates ever finer matter and ... powerful forces) [a space shuttle]
2. Intermediate (semi-automated partially intelligent technology that manipulates refined matter and medium level forces) [an eCommerce algorithm to get you to buy stuff] 
3. Low (labour-intensive technology that manipulates only coarse or gross matter and weaker forces) [a shovel]"

[Interested?](http://bit.ly/1GsH5SO)

But the online Oxford Dictionary takes a much narrower view, *viz*.: "The application of scientific knowledge [the logical progeny of information] for practical purposes, especially in industry. [For example,] advances in computer technology [or] recycling technologies." 

We will take this narrower view of technology in this book, partly because we just don't have the time to consider all the nuances that the broader scope suggests. So when we consider technology, and specifically *information and communication technology*, we will be constraining ourselves to the narrow realm where engineering, science (both natural and social) and business realities intersect and interact with the systems that are in use in our modern society, with an eye to whether or not they are creating value by either reducing input or enhancing output.

[Interested?](http://bit.ly/1z0JHX3)

### Information technology

What then of the overlap between information and technology? What exactly is it? Turns out is has a long history, stretching all the way back to prehistory. An excellent introduction is provided by a book in the openbookproject.

[Interested?](http://openbookproject.net/courses/intro2ict/history/history.html)

## Pillar 2: Is what we are examining a tangible or an intangible?

This distinction is larger and more important than the traditional one made in most introductory information system classes. We were wont to say that tangible things are those with a physical presence that we can touch, such as a computer or a smartphone. Intangible was something that, while it exists, cannot be weighed, measured or directly detected, such as software or a computer service such as email. You can weigh a smartphone, but how much does email weigh? What are its dimensions? If measurable at all, its measurement is in terms of other services, such as email sent and received. 

While the traditional distinctions are still valid, it’s now bigger than that. Read on.

It started slowly, then picked up steam in the 1960s. What was *it*? It was the switch from the production of tangible goods (manufacturing) to intangible services (banking, health, education) in the post-industrial era, sometimes referred to as the era of *deindustrialisation*. Indeed the proportion of Canadians employed in the *secondary* sector (*primary* roughly being agriculture, mining and extraction, forestry, etc. and *tertiary* being the services sector) has declined steadily over time.  

**Figure AF. Proportion of Canadians employed in the secondary and tertiary industries, 1931 to 1975**

![Employment by sector](https://raw.githubusercontent.com/robertriordan/2400/master/Images/employment_by_sector_31_75.PNG)

Examination of Figure AF shows some interesting trends. With the exception of the period during which Canada was at war (1939 – 1945), the proportion of Canadians of both sexes who were employed in the manufacturing sector has been in steady decline. On the other hand, there has been a steady rise, clearly since the inflection point at right around 1960, in jobs in what we call the *service or tertiary sector*. The trend lines in the graph clearly show the story. 

These trends have extended into the present day, where in 2014, using the percent of GDP generated by these sectors as an indicator, Policy Horizons Canada, using Statistics Canada data from 2011 and 2012, reported that activity in Manufacturing accounted for 13% while Service accounted for 72% of Canada’s Gross Domestic Product (GDP). Natural resource extraction and Other, combined, account for the remaining 15%.  Indeed [Statistics Canada](http://www.statcan.gc.ca/tables-tableaux/sum-som/l01/cst01/labor10b-eng.htm) reported that in 2014, the number of employed Canadians in manufacturing had sunk to 9.6% (in 1975 it was over 20%) while the number in services has skyrocketed to 78.1%.

[Interested?](http://www.horizons.gc.ca/eng/content/significant-shifts-key-economic-sectors)

Here is what Policy Horizons had to say about it: *“The Canadian economy is dominated by the services industry, which makes up 72% of GDP and employs 78% of Canada's workforce. The services sector includes accommodation, business, cultural, education, financial, food, health care, information, professional, retail, transportation, wholesale and a range of other services. The sub-sectors that contribute the most to GDP include finance, insurance and real estate services (29% of total services), health care and social assistance (9.3%), retail trade (8.6%), public administration (8.2%) and wholesale trade (8%).”*

There are multiple reasons for these seismic shifts, but the nuances are best left to the labour economists and policy wonks. For our purposes, it matters not at all *why*, but rather only *that*. And it matters for the remainder of the so-called *advanced economies* as well (according to [World Bank data](http://data.worldbank.org/data-catalog/GDP-ranking-table) for 2013, Canada ranked 11th in terms of the size of our economy by GDP). All advanced economies have experienced the same relative shift over the same period (see the 1998 [IMF study](https://books.google.ca/books?id=-05vo38wz-IC&dq=advanced+economies+shift+from+manufacturing&source=gbs_navlinks_s)), and the economies of the currently modernising countries (China, India, Brazil, among others) will experience the same shift towards intangible, knowledge-based contribution to GDP. See the World Bank [study](http://bit.ly/1DQ2xNG).

As my eldest granddaughter Kiana loved to ask repeatedly when younger “But why grandpa?”, we ask why it matters that the world’s largest economies are increasingly dominated by service providers. 

The Policy Horizons (*ibid.*) piece has this to offer:  *“Economic growth increasingly linked to intangibles: The shift from a goods economy to a knowledge economy has increased the importance of intangibles. Intangible assets include brand, customer base, design, human capital, intellectual property and [not physical but rather interpersonal and inter-organisational] networks, among others, which are all essential aspects driving firm value. Investments in intangible assets are an increasingly important source of growth that could result in a new range of business services. Also reshaping services is the rise of intangible artifacts through the digitization of information and arts. Freed from the resource constraints of physical production, the economics of these markets are being transformed by a shift from relative scarcity to relative abundance, suggesting a need for different pricing mechanisms. New monetization models are emerging, no longer centred on ownership but access, such as online subscriptions or the use of targeted advertising powered by data analytics. The greater ease of replicating intangible artifacts has also increased piracy and intellectual property concerns.”*

Careful reading of this passage reveals that we are firmly and irrevocably in what [Goldfinger](http://www.idate.fr/fic/revue_telech/68/goldfing.pdf)  (accessed April 17, 2015) has termed *The Intangible Economy.* He writes: “Knowledge Economy, Digital Economy, Information Society, Experience Economy, names for the new economy proliferate to the point of becoming ubiquitous buzzwords.” Furthermore, in trying to describe the new economy, he offers “I would like to suggest an alternative framework, based on a single defining trend: the shift from tangible to intangible. The economic landscape of the present and future is no longer shaped by physical flows of material goods and products but by ethereal streams of data, images and symbols.” And finally “At the core of the agricultural economy, there was a relationship between man, nature and natural products. The core relationship of the industrial economy was between man, machine and machine-created artificial objects. The intangible economy is structured around relationships between man and ideas and symbols. The source of economic value and wealth is no longer the production of material goods but the creation and manipulation of intangible content. *We live in the intangible economy* [emphasis added].”

Though the paper is a little dated (published in 2000), it remains quite viable today. A clever metric is reproduced below wherein the physical weight of certain commodities is expressed in terms of their cost in USD. It’s surprising.

**Figure LAJ. Comparison of price per pound for manufactured goods in different classes**

![Price per pound](https://raw.githubusercontent.com/robertriordan/2400/master/Images/price_per_pound.PNG)

(Goldfinger quotes the source of this data as *Colvin, Fortune Magazine*, but does not provide the full citation in his paper. We’ll have to take it at face value.)

It’s indeed interesting to see that the pinnacle of costliness is still gold (in this limited comparison) but not far behind was the Intel P3 processor (the most powerful personal computer microprocessor at the time). Note also the relatively low cost per pound of the Mercedes in comparison to Viagra, for example.  There’s plenty I could write about how gold, Mercedes and Viagra might work together (not to mention hot rolled steel) but I will spare you just this one time. 

What’s the point of this? It is to demonstrate the shift to what LSE professor Danny Quah (see the [Goldfinger article](http://www.idate.fr/fic/revue_telech/68/goldfing.pdf) for the reference) has called the *increasingly weightless economy* where more and more GDP is generated by “economic commodities that have little or no physical manifestations.” While this shift is not inherently due only to the so-called *digital revolution* it is also clear that much of the intangible output in this new economy is produced by or requires information and communication technology *to unlock its value*. 

[Interested?]( http://www.davies.com.au/ip-news/the-future-of-manufacturing-embracing-intangible-assets)

In case you were thinking that this is a strictly western, developed economy phenomenon, take a look at the infographic below from *The Globe and Mail* (Saturday, August 15, 2015 citing the World Bank as the data source) which accompanied an article entitled *China's New Economic Reality."  Ignoring the image of the Chinese currency between the two lines, we clearly see that the red *services* line, separated by miles from the grey *industry* line in 1981, has recently crossed above the heavy industry contribution to China's GDP. The times they are a changing, indeed. 

**Figure LJP. Contribution of industry vs. services to China's GDP, 1981 to 2015**

![Price per pound](https://raw.githubusercontent.com/robertriordan/2400/master/Images/ChinaWeb4.jpg)

[Interested?](http://www.theglobeandmail.com/report-on-business/international-business/article25974515.ece)

####Disassociation of content, time and support

We live in an increasingly digitised world. Consider a simple example: a hockey game.  Time was, the only one way to enjoy the event was in person. Consider the myriad of ways that event can be enjoyed today, both *synchronously* (in real time both in person and offsite) and *asynchronously* (not in real time – stored for later consumption).  See Figure TT below.

**Figure TT. Disassociation of content, time and support**

![Disassociation](https://raw.githubusercontent.com/robertriordan/2400/master/Images/disassociation.png)

Let’s unpack this busy figure. The event is a hockey game. In the early days of such games, they were played on local frozen ponds and then on makeshift rinks, and only when the temperature was below freezing. The teams were made up mostly of locals, and the fans, if there were such hardy souls, were locals rooting for the local boys (it was a man’s game back in the day). In order to experience the spectacle, it was necessary to attend in person. There wasn’t often any press to report on the match. The only artifacts produced by the games were in the memories of those in attendance, and any anecdotal scribblings of a local historian or scribe. Those were the days indeed.
*
As the cities and towns grew, so too did the rivalries. More and more attention was paid to sporting events and more archival material became available in the form of photographs and perhaps even some grainy film. Players began to receive small *rewards* for their efforts. Firms became interested in having their name associated with a winning side (as brand management). When real money enters the picture, interest picks up. More people attend the games. More widespread interest in the outcomes is aroused. This spiral of interest and investment leads to capacity problems at the arenas. Not enough seats for willing purchasers, and the widespread interest leads to the inability for *fans* to attend due to long travel times and cost.

Enter new support systems. The dawn of commercial radio, and then television, led to the ability for remote fans to enjoy at least some of the *in-person* experience of a sporting event in the comforts of their own home. This revolution required what Goldfinger calls *supports* and what we might think of as technology (which includes print media). Supports are the things that allow us to have a *near-real-time* experience of an event occurring in some other place. 

I remember way back to the 1965-66 Junior “A” hockey season, the last where legendary Bobby Orr played for my hometown *Oshawa Generals*, and I was all of 13 years old. There was no TV coverage at all but I owned a gorgeous little compact transistor radio, powered by the very first 9-volt batteries,  that I hid under my pillow so I could listen to the games as the Generals travelled to exotic (to me) locations around the country to beat first the *St. Catherine’s Black Hawks*, then the *Montréal Junior Canadiens* (at that time playing in the Ontario Hockey League) and finally the *Kitchener Rangers* to win the J. Ross Robertson trophy emblematic of OHA (not yet called the OHL) supremacy. The Oshawa side then went on to best the *North Bay Trappers* (by a combined score of 43-9 over four games) and finally the *Shawinigan Bruins* in three straight in a best-of-five to earn the right to represent the eastern provinces in the Memorial Cup.  

Oshawa lost to the *Edmonton Oil Kings* in that national final, with Orr playing hurt and finally reputedly being benched on orders from his new masters, the *Boston Bruins*, who had drafted and signed him to an NHL contract. Orr’s bad knees were legendary, even at 17 years of age. 

While the pain of losing that Memorial Cup series to the Oil Kings is still palpable, so is the sheer excitement of listening in near real-time as the games unfolded in faraway places. That little Japanese *Candle* transistor radio was my support. By the same token, I remember well watching the Toronto Maple Leafs (my dad’s team – I was a Bruin’s fan as Oshawa was a Boston feeder team) win their last ever (and maybe final ever) Stanley Cup, in 1967, when I was the tender age of 14… alas. This was the golden age of *broadcasting*, or sending a single signal to many receivers, in this case, most of the TV sets in Canada.  

This explains the first two spheres on Figure TT, numbered 1 and 2, extending the in-person experience to the home, through early radio and then TV.  Sphere number three is a bit more complex, as it introduces the confounding influence of *synchronicity* to the equation. Attending the game, or listening via radio or watching on TV are all *synchronous* events – experienced in real time (or very near real time taking *latency* into account).  Sphere number three, labelled *Anywhere*, shows that it’s not necessary to be *tethered* to a location in order to experience an event. The automobile radio and the portable transistor radio were the first to *cut the cord* as it were, allowing the very first *roaming* experience. And make no mistake-- it was big at the time. The ability to bring music to the beach or on a walk or to a ball game on a summer's eve was truly liberating. 

Today the ability to consume anything, anywhere, in real time is taken for granted. Thus we have a myriad of ways to experience the game, including streaming services available wherever the internet is available, on devices such as desktop and laptop computers, tablets, smart phones and smart TVs, most of which are quite portable. Commercial providers offer services varying from streamed audio, streamed audio/video, to instant, *bite-sized* updates in various forms depending on the device in use.  All these are on the synchronous side of the equation. Asynchronous media include newspaper reports, news media on radio and TV reporting (which are nothing new at all), as well as our own capacity, with a Personal Video Recorder (PVR), to *tape* an event and dissect it at any time later in all its stop and start, slow-motion glory. Time is no longer in the equation. 

And that covers the commercial providers. Increasingly important, however, is our own ability to report (share) the real-time experience for both synchronous and asynchronous consumption. Witness the rise of social media and the concept of *narrowcasting*.  Twitter and Facebook are narrowcasting technologies, narrow being defined relatively. Twitter supports narrow communities of *followers* and Facebook the same concept, referred to as *friends*. I don’t need to explain these platforms to you. 

Let’s think about what could happen at a public event, such as our hockey game. With social media, you could easily narrowcast through any number of platforms. Your followers and friends could consume, in near synchronicity, your experience of the game. But let’s go farther afield and consider that you could also narrowcast a live stream of the event through technologies such as *periscope, and *meerkat* which allow anyone with a camera and a connection to the internet to live stream any event. You are the new CBC. An emerging *Sportsnet*. A nascent *Hockey Night in Canada*. 

Ok so cool enough. But consider that you can be Tweeting, Facebooking and *periscoping* in real time *inside the venue* where the event is happening. Others in your sphere could well be consuming your narrowcasts, and by the same token you could be consuming theirs.  This adds a whole new dimension to the model. Finally, you could also be live narrowcasting the game to anyone on the other end of a cell phone call, or recording your play-by-play on a voicemail recorder for later consumption and analysis. 

Alas, just two days after my writing the above paragraph, The NHL itself officially banned* livestreaming of its property (games) by anyone. [Mashable.com reported](http://mashable.com/2015/04/22/periscope-meerkat-banned-nhl/) that NHL Deputy Commissioner Bill Daly announced on April 21, 2015 that: “… any streaming of footage in violation of the NHL’s Broadcast Guidelines (including, for example, live-streaming inside the arena less than 30 minutes before the start of the game) and Media Access Policy is expressly prohibited." The Mashable piece concludes with: “… the branding opportunities for organizations like the NHL seem pretty limitless: rinkside live streams of team warm-ups, exclusive interviews with players and coaches, the list goes on. So it figures: Why cede those opportunities (and future dollars) to fans?”

[Interested?](http://yhoo.it/1dkOJFp)

All of this represents a wrinkle in the space-time continuum of synchronicity and location. But what’s the bottom line here? Why does this matter other than being really cool and other than actually making it considerably less likely that we will enjoy an in-person, synchronous event? (Next time you either attend or watch a major event such as the opening or closing of the Olympic Games, note how many people are recording it, watching the pageantry through a tiny camera screen, to be played back… never?)

There is no denying that the majority of the *weightless* or the *digital* or the *intangible economy* is if not driven by, at least facilitated by, information and communication technology. So to fully participate, it’s necessary to be well versed in the technology – not necessarily to be able to *create* new technology but clearly to be able to *leverage* it. To understand it well enough to be able to see how it can solve problems or create opportunities for competitive advantage. 

And it’s also important because of ownership. And ownership is all about business because business sells access to events such as hockey games. This is why it’s a revolution. The ownership model is completely upside-down now. The ability to participate in (consume) an event is now available to any and all with a support mechanism (technology) and an internet connection (technology). So ownership of that event is now widely dispersed. Who, for example, owns the rights to a Tweet or a FB post or indeed to a live stream via *periscope* of a hockey game? And the pricing for consumption is now multi-tiered. In fact, one could argue that the price to consume many events (or movies or broadcasts of any kind – indeed at the time of writing there have been numerous reports of live-streaming feature films) has been reduced to the hit you take on your data plan. So how to generate revenue from events? How do you pay your hockey players or your screen actors? 

This at least partly explains the consolidation among entertainment providers (see the infographic below for US entertainment industry) and also why the former content *purveyors* (such as Rogers and Bell) are now content *producers*, owning major league sports teams for example, and buying up sports broadcasting properties (while Bell owns at least part of TSN, for example, and Rogers owns the Blue Jays as well as their ball park, and at the time of writing, they together hold a nearly 80% share in Maple Leaf Sports and Entertainment, which owns major league teams in three leagues as well as considerable prime real estate). So the folks who formerly *brought you* the product (via cable, satellite and the internet), now own the venues, the product and the distribution channels. And in a market such as Canada, where people live and die hockey, having the purveyors of content own the competition is particularly problematic.  

If they don’t follow this model, they risk being shoved aside by the content producers. They’ve become the creators rather than the purveyors. *Shomi* is a Rogers – Telus joint response to *Netflix* (isn’t it fun when competitors get in bed with each other – this is the definition of a strategic partnership). Bell’s *CraveTV* is the same. As of the writing of this paragraph, Rogers/Telus has announced that, just like Netflix, Shomi will be available to all as a streaming service to anyone (not just their customers). It’s the only way to compete in this world of intangible products. And it’s due in large part to the *digital revolution*. Netflix is a *disruptor*. We will return to disruptors later in this text. 

**Figure STA. The illusion of choice**

![Illusion of choice](http://www.dailyinfographic.com/wp-content/uploads/2015/04/IllusionofChoice.jpg)
 
**NEED SOME DISCUSSION OF THIS**

![The on-demand effect](https://raw.githubusercontent.com/robertriordan/2400/master/Images/netflix.jpg)

While the Policy Horizons article (*ibid.*) is excellent in its entirely, below is the most important material from our perspective: They write: 

“While a large part of the services sector will continue to include low-skilled work, some significant shifts for an economy that increasingly revolves around services are highlighted below.

“Economic growth increasingly linked to intangibles: The shift from a goods economy to a knowledge economy has increased the importance of intangibles. Intangible assets include brand, customer base, design, human capital, intellectual property and networks, among others, which are all essential aspects driving firm value. Investments in intangible assets are an increasingly important source of growth that could result in a new range of business services. Also reshaping services is the rise of intangible artifacts through the digitization of information and arts. Freed from the resource constraints of physical production, the economics of these markets are being transformed by a shift from relative scarcity to relative abundance, suggesting a need for different pricing mechanisms. New monetization models are emerging, no longer centred on ownership but access, such as online subscriptions or the use of targeted advertising powered by data analytics. The greater ease of replicating intangible artifacts has also increased piracy and intellectual property concerns.

“Employers cater to the 'creative class': As developed economies have found their value through knowledge creation and innovation activities, 'creative class' workers (e.g., scientists, engineers, media workers, designers) in the services sector have seen their market worth rise, while blue-collar workers and low-skilled service workers have seen little growth in their relative value, even though the latter still dominate the services industry. These creative workers tend to seek to live in diverse and creative cities and are comfortable working in flexible work arrangements such as online employment or self-employment. With the expectation of greater future competition for creative class workers, their preferences may be influential in determining workplace norms and firm location choice (Florida, 2002).

“Services increasingly crossing borders: Some service firms may find that the need for proximity to their workers becomes a moot point. Information communication technologies have facilitated the off-shoring and exporting of service tasks, which to date have largely been limited to the fields of architecture, engineering, finance, education and research and development. Services have traditionally been less tradable than goods, particularly in the most customer-facing industries such as retail and health care (Farrell et al., 2005). However, even in these fields, virtual and communications technologies are advancing the ability to bridge distance and offer personalized service, for instance through mobile health tools and applications, remotely controlled robots for high-precision surgeries, and the use of avatars and 'virtual fitting rooms' to aid online shopping for clothes. Within firms, developments in virtual technologies that can easily reproduce the human interface could expand the tradability of other service tasks, forging a pathway for the greater participation of 'virtual service workers.' Where employers are challenged to find specialized skills in their local labour market, online contracting is seen as an advantage to fill needs quickly. The transnational nature of service provision raises important questions for Canada's labour market and Canadian service providers.

“Services influenced by an aging population: Many service industries will likely see greater demand due to the needs and preferences of an aging population (e.g., personal care, health, leisure, tourism, transportation, supplementary careers). At the same time, service industries are concerned about their ability to source specific skills as needed, as they anticipate a smaller working age demographic to fill these needs. Firms are exploring incentives to retain older workers (e.g., flexible work schedules) and interventions to delay the onset of chronic ailments that might limit their ability to keep workers (e.g., offering workplace wellness programs).”

From this you should clearly see that a big part of your ability to both create new and consume existing value in society will be fuelled and facilitated by ICT. That’s why we’re here. That’s why we study ICT. You are at university to learn how to become knowledge creators. 

Indeed that is exactly what you are doing as you train and study – creating intangible and (though we do try via testing and require that you write papers for our assessment) almost immeasurable capacities for thought and synthesis and creation. The value you create in your career will not, in all likelihood, be measured on a weight scale (as in a bucket of made widgets), but rather on an ethereal scale that measures ideas, theories, mashups and disruptions. You will indeed be working in the weightless economy. Best to be prepared. 

#### Takeaways
To bring it home, we live in and are likely to remain living in an increasingly a service-based economy. Services are weightless. Weightless artifacts are intimately tied to ICT whether by services or by manufacturing. And the contribution of services to GDP is consistently outpacing all other sectors. It appears that services, and ICT-leveraging services among them, is a good place to be. Let's get trained up. 

## Pillar 3: Does this (technology/service/data/etc.) either shrink input or grow output or both?

If neither, is there any value to it? The short answer is no. That's the medium and the long answer as well. 

There are primarily four ways that a business can derive benefit by applying ICT to support the organisation and its processes:

- Supporting the value chain – As we have discussed in previous sections, systems can be applied directly to the value chain to make it more efficient and effective.
- Automating – Business processes and functions can be automated, enabling cost reductions, normally through the consumption of less labour and increased efficiency and reliability.
- Informing – ICT systems create, store, and make available data that can be used to improve the quality of business decisions, as discussed in Chapter 3.
- Gaining a competitive advantage – Implementing a new system that creates new capabilities may give an organisation a temporary competitive advantage over their competitors. 

A McKinsey piece on capitalism proposes that the purpose of business is to solve problems. Here's the quote: "... the crucial contribution business makes to society is transforming ideas into products and services that solve problems." They go on to specify that if problems are solved at an increasing rate, prosperity results. If this is true (and it makes good sense to me on the surface) then our task as we study technology becomes to determine if the product or service or technology under consideration (which is the tangible manifestation of a new idea) solves a *real* problem. 

[Interested?]( http://www.mckinsey.com/insights/corporate_social_responsibility/redefining_capitalism)

But it also seems to me that something is missing from the McKinsey postulation. And that something is *profit*. Under capitalism, for an enterprise to remain viable, it must either be supported externally or must generate a competitive return on investment. Nothing is free. Everything (and I mean everything) has a cost. So cancer research (cancer being the problem) can be supported by governments through their revenues, derived mainly from taxation. So you and I pay for that research. Some cancer research is also undertaken by private enterprise (pharmaceutical firms and university research labs as funded research, for example), but private enterprise won’t (or can’t for long) go down a road without the reasonable expectation of recouping a premium over what they have invested in their work. This is ROI. This is the basis of free enterprise and of capitalism itself. 

It’s clear that problems don’t get solved for free. If business solves a problem, it must make a profit in so doing. But there’s also the age-old challenge of widening the gap between costs and revenues. It’s not enough to make a profit. The profit must *increase* year over year. Grow the margins. Be more effective and/or more efficient. Expand the market, open new frontiers. Grow revenues and shrink costs, that’s what drives the system. Keep this in mind.

What is also interesting from the McKinsey piece is their contention that problems must be solved at an increasing rate. So this year we must solve more problems (either in absolute terms or as a rate) than last year and the year before that. Everything escalates under capitalism. Goods cost more, wages rise to meet the cost of goods and we live in this inflationary spiral which ebbs and flows but other than in rare periods of deflation (which are quickly corrected by monetary policy) is always with us. Problems must be solved at an increasing pace and profit must increase year over year. It’s enough to drive one mad. 

Thus the pace of everything is accelerating. This puts increasing pressure on the systems that businesses use to solve problems.  What then is a system?

<a name="system"></a>
### Systems and process
**Figure SG. A simple system**

![Process](https://raw.githubusercontent.com/robertriordan/2400/master/Images/simple_system.png)

A simple system or a complex system, any system, has only three pieces. First, input is required. Once input is present, work (a process) then transforms that input into output. That's it. Input --> Process --> Output. All three are necessary. No work process without input, no output without work, no input and work without something coming out the other side. Make sure to understand that a system also requires a trigger: something to set it in motion. Systems don't just start of their own volition (if they had any) or momentum. Something must spur them into action. Furthermore, and equally important, a system must have a *goal*; a reason for its existence; a purpose. We don't just do things because we can.

Our simple graphic above depicts a system, the goal of which is to make dairy products -- something my family did 100 years ago when their telephone number in Oshawa, Ontario was *7*. Yup, that’s it. Seven. Talk about getting in on the ground floor! 

![Riordan’s Dairy](https://raw.githubusercontent.com/robertriordan/2400/master/Images/riordans_dairy_cap.jpg)

To make dairy products, plenty of inputs are required, including hay, corn and grass to feed the cows, milk from those cows, scientific know-how, elbow grease and muscle, expertise, time, land and, of all things, data, information and accumulated knowledge. And lots more such as a farm to nurture the cows and a dairy with machinery to bring it all together and a supply chain to move stuff in and out. The message is that in order to produce dairy, plenty of input is required.

These inputs are fed to the dairy process, and this work process yields the output: cheese and milk and yogurt and ice cream. It’s pretty clear that our little graphic represents a very high-level view of the dairy system. There are literally hundreds of other systems, linked in chains, that actually account for the production here. But each is the same; a simple process, flanked by inputs and outputs. How then, and where, is value added and profit realised? 

Let’s look at our system again, this time in light of value add. 

**Figure Z. Value creation**

![Process](https://raw.githubusercontent.com/robertriordan/2400/master/Images/value_creation.png)

### How is value created?

It sounds simple, but the entire premise upon which the capitalist system rests is that the realised output from a system (a business) must be worth *somewhat more*, in the long run, than the investment in inputs required to produce the output. Thus the goal of a business is that the difference between what you get when you sell your wares and what it costs to fuel the system to produce those wares is greater than what profits you might realise from *doing something else with your money*. This is *opportunity cost*. You have *an opportunity* to do lots of stuff with your money. You can put it in the bank, invest it, lend it (a form of investment if charging interest), or use it to buy inputs for a process that you wager will give you a better return than any or all of the other alternatives. Looked at another way, the value generated must be greater that what it would cost to borrow the money to run the business system. If you’re paying 3% to borrow money to run your business and it’s only returning a 2% margin, well... you’ve got trouble with a capital F. 

And remember that the purpose of business is to solve problems. I have added the *caveat* that those problems won’t get solved by private sector firms unless they can be solved *at a profit*. 

### How does technology add value in solving problems? 

A business is a collection of systems, large and small, simple and complex. How do we measure the value creation potential of technology in a set of complex processes? 
 
The twin concepts used in measurement of all systems are: *effectiveness – doing the right thing*, and *efficiency – doing the thing right*. Effectiveness is all about the components of a system. No process should be part of a system if it doesn’t add value at output. Nothing should be done that doesn’t need to be done. On the other hand, efficiency is all about doing the right thing in the right way, with the least waste and the maximum output. It should be clear that the top priority is effectiveness. Once the process is doing the right thing, effort can be put into doing the thing right. It’s all too easy to be really efficiently doing the *wrong thing*. 

Paradoxically, technological progress is not measured in terms of impact on the actual *business process* of doing anything. Technological change impacts business process, but it’s impact is measured elsewhere. Though this sounds heretical, in the simplest terms, technological progress is measured not in terms of process improvements, but rather by the impact such progress has on the inputs and outputs of the system -- the resultant decrease in required inputs to produce the same output or by an increase in realisable output given the same input. In the simplest terms, the ratio of realised output (what you reap when you sell) to the cost of input (what you invest in procuring inputs for your system) must be greater than 1. And it must be greater than 1 **and** in excess of what you might earn by doing something else with your money. There are many terms in business to describe this phenomenon, including *Return on Investment (ROI), Rate of Return,* and *yield.* 

Of course this is a very simple measure and there are plenty of other contingencies and vagaries of which to take account when deciding what to do with your money. Long-term ROI, however, is a must. You must eventually get a decent and competitive return on your investment in order to justify investing in a business. *The output must eventually be greater than the input.*  

[Interested?](http://www.businessdictionary.com/definition/return-on-investment-ROI.html)

Let's do some very simple math here. If you dialed *7* in 1915 and asked my grandfather Joseph Riordan what his ROI was for last year and he said "None of your &@#$%&* business!" you would know that you reached my grandfather. If you were to get a chance to look at the books and found that a simple ROI was 1.03, for example, but that the interest charged by lenders on borrowing capital was 4%, you might conclude that, in the short term, Joe would be better off lending his money to someone and making 4% on it. The ratio of output/input (profit) was 1.03 and the ratio of input/output (cost) was 1.04. His money was better off somewhere else. And eventually it ended that way. Another local dairy gobbled up Riordan's Dairy and many others like it in an effort to increase efficiency through scale.   

**Figure JD. ICT Value creation**

![ICT value add](https://raw.githubusercontent.com/robertriordan/2400/master/Images/ict_value_creation.png)

### A working definition of progress
We know progress has occurred when inputs cost less for a given output or output is more plentiful or valuable from a given input. Simple. The value creation graphic (Figure ZX below) shows this clearly.

Assume a stable system where yesterday, 100 units of input produced 100 units of output. Progress has been achieved if today we get the same output from only 99 units of input. On the other end of the system, progress will also have been achieved if tomorrow those 100 units of input produce 101 units of output. Either of these might represent an increase in *effectiveness*, where we are making more *effective* use of our input resources, or an increase in *efficiency* wherein we are doing more with what we have. Message: technology effects are measured at the boundaries of the system, where we count inputs and outputs.  

Thus technology has an impact if it moves the needle on efficiency and/or effectiveness. That’s how we measure technological impact. Either it leads to cost savings on the input side or to increased realisable value on the output side. Technology has the capacity to widen the margin between costs and revenues. It can increase profit. It can also cost a lot of money and have no appreciable impact.

What if an entirely *new* solution to an existing problem emerges? What if there's no input to improve on or outputs to augment? We're still good. If it's entirely new and better than anything extant. then if must be cheaper to run or produce better outcomes, otherwise why use it? Bingo.

Finally, the part where I specify *realisable* output is important. If progress results in *so much output that it can be used or sold or examined or put to good use* then it's of no value, and in fact is potentially wasteful. Imagine a new variant on growing mushrooms that increased output by a factor of 100,000. *That's a lot o' 'shrooms.* What to do with them all? Is it progress if we can't realise value from it? That's a great topic for a debate someday.

Important takeaway: Information and Communication Technology (ICT) operates in the process piece of business problem solving, but its impacts are measured on the edges of the system or by calculating the ratio of input/outputs and comparing to the benefit of doing something else with your capital. See the *ICT Value creation graphic* above.

An intimately related topic is that of *Business Process Management* (BPM). Specifically, BPM has expanded its scope recently to encompass *Six Sigma* methods among others, and to specifically embrace the following functionality:

- Visualize - functions and processes
- Measure - determine the appropriate measure to determine success
- Analyze - compare the various simulations to determine an optimal improvement
- Improve - select and implement the improvement
- Control - deploy this implementation and by use of user-defined dashboards monitor the improvement in real time and feed the performance information back into the simulation model in preparation for the next improvement iteration
- Re-engineer - revamp the processes from scratch for better results

If you are interested in *Supply Chain Management* you might find BPM to be of interest.

[Interested?](https://en.wikipedia.org/wiki/Business_process_management)

### What *real problems* can technology solve? 
A real problem is one that either has input or output challenges – and we know by now that those are the *only* challenges. See the [McKinsey article](http://www.mckinsey.com/insights/corporate_social_responsibility/redefining_capitalism). Thus in this book and this course we will examine at each juncture in discussing a piece of technology (whether it is personal productivity software or social media or an enormous enterprise system) whether the technology can be demonstrated to either reduce the cost of inputs or increase the value of outputs. If not, it simply is a waste of time and effort.

## Pillar 4: Does this product/service/technology lead to a sustainable competitive advantage?

This is developed in the context of both PERSONAL Competitive Advantage (such as FB or Twitter, etc. - needs to be carefully defined but this definition will allow us to understand the corporate part) and CORPORATE Competitive Advantage (easier to define but must also encompass not-for-profit and government services). TK

### What is competitive advantage?
All of us, every minute of every day, are in competition for scarce resources. Though not particularly *culturally inclusive*, Wikipedia (accessed June 12, 2015) has this to say on the subject [emphasis added]:

"Competition in biology and sociology, is a contest between organisms, animals, individuals, groups, etc., for territory, a niche, or a location of resources, for resources and goods, mates, for prestige, recognition, awards, or group or social status, for leadership. Competition is the opposite of cooperation. It arises whenever at least two parties strive for a goal which cannot be shared or which is desired individually but not in sharing and cooperation. Competition occurs naturally between living organisms which co-exist in the same environment. For example, animals compete over water supplies, food, mates, and other biological resources. Humans compete usually for food and mates, though when these needs are met deep rivalries often arise over the pursuit of wealth, prestige, and fame. **Competition is also a major tenet of market economies and business is often associated with competition as most companies are in competition with at least one other firm over the same group of customers**, and also competition inside a company is usually stimulated for meeting and reaching higher quality of services or products that the company produce or develop."

[Interested?](https://en.wikipedia.org/wiki/Competition)

The [Visual Thesaurus](http://www.visualthesaurus.com/ "Visual Thesaurus") website offers a word picture on the term:

**Figure TTT. Definition of competition from Visual Thesaurus**

![Competition](https://raw.githubusercontent.com/robertriordan/2400/master/Images/competition.png)

In discussing competition in the business context, we usually consider the struggle for *competitive advantage*. The online Business Dictionary offers the following definition:

"A superiority gained by an organization when it can provide the same value as its competitors but at a lower price, or can charge higher prices by providing greater value through differentiation. Competitive advantage results from matching core competencies to the opportunities."

[Interested?](http://www.businessdictionary.com/definition/competitive-advantage.html)

Broadly scoped, competition can be understood as anything that could otherwise occupy customers' time or money, *including doing nothing.* In order to sustain long-term profitability, a firm must respond strategically to competition and that’s just as much about creating value as it is about being efficient and effective. But just looking at your direct competitors is completely inadequate. Buyers can play you against your rivals to drive prices down. Suppliers can constrain profits with high prices. New entrants can bring new technology and synergies to bear, ratcheting up the investment required to remain competitive. Substitute offerings (even from outside your own industry) can lure customers away. These are Porter’s Five Forces. [Source: BUSI 3800 course notes, Fall, 2013, Robert Riordan]

Once competitive advantage is understood in terms of one firm's ability to provide superior price, quality or service over another, the discussion normally turns to *sustainable* competitive advantage. The ability to persist in this position of superiority over your competitors. To stay ahead, different and desirable. 

The question then becomes, in our context, *How can ICT contribute to sustainable competitive advantage?*

The answer is complex and not at all pretty for proponents of ICT. The back story is that ICT has become, in the words of Nicholas Carr, a *commodity*, not unlike electricity (*DOES IT MATTER?: Information Technology and the Corrosion of Competitive Advantage*, Harvard Business Review Press, 2004). It's all the same for every firm in every market. No firm can *sustain* a competitive advantage from ICT since it's freely available to all and is not at all *differentiated*. We will see in Chapter 2 how more and more firms are installing ERP systems (mammoth enterprise systems that manage all important processes in a firm) and that, when deciding to implement an ERP, the most cost-efficient way to do so is to accept the defaults in the system rather than customise. So firms alter their business processes to conform with the way the ERP works *rather than the other way around*. So a firm doesn't customise (in any meaningful way) the ERP. Rather, the ERP standardises the firm. And with more and more forms implementing ERPs as a *competitive necessity*, there is little of no competitive advantage left in ICT *because every firm has ICT and it operates in the same way for everyone, thus there cannot be any advantage in it*.

In addition, what Carr refers to as the *technology replication cycle* is continually being shortened. If a firm were to create their own ICT system and gain some kind of advantage from it, the entire industry will know about it very soon through the free flow of information and the fact that large tech consultancies (such as Accenture and McKinsey and Bain and Deloitte and others) hire out their ICT consultants to a wide range of companies in a market and those consultants act to diffuse the insights of particular firms to the broader community. So everyone knows fairly quickly the mechanics of the advantage. Coupled with robust and efficient technology tools to build systems quickly, a competitive advantage conferred on a firm for creating their own value-add system will soon be replicated across the industry. So *sustainable* competitive advantage is difficult.

This is not to say that there have never been cases of ICT conferring sustained advantage. For example, American Airlines automated reservations (the beginning of the end for travel agencies), pricing, and seat assignments (using yield management technology) in the 1960s and continue to enjoy some advantage to this day. And Walmart created logistics applications in the 1980s and invested heavily in *Radio Frequency Identification* (RFID) applications (though this initiative eventually lost steam) and continue to dominate rivals such as Target, Sears and Kmart. But the examples are few and far between. The value gained by standardising - the cost advantage for a firm to be like their competitors - trumps the advantages of being different.

###The two contributions of ICT to value creation

I have argued above that there are but *two impacts* that ICT can have in our individual daily lives and in the functions of every for-profit or not-for-profit enterprise, public institution and social endeavour. The first is as *Input Reducer* and the second function is that of *Output Enhancer*.  

An input reducer functions as an agent which, *at a given level of output*, reduces the unit cost of inputs such that either more input is available at the same cost, or less input is required at the same cost. An output enhancer either increases the volume of output or the value of that output, holding volume constant. 

So *context* becomes the frame of reference within which we narrow the error bands around our decisions. Context is everything, and ICT provides, or can provide, rich context. We will be examining context repeatedly in this book, because context is what drives information creation, and information drives decision making and good decisions are something that we all, as actors in both public and private scenarios, need to make. 

For now, in examining the ICT task of input reducer, we need to examine this function in terms of its ability to provide the necessary rich array of data which allow decisions to be made with relative certainty. In order to make fewer errors in our decision-making endeavours (and thus to reduce the amount of input necessary to produce the same output), we need as much context (the rich backdrop of facts, figures and shared understandings) as possible. We need to know what data means. We need to be able to put data into context to create actionable information. All this will come together in Chapter 2. 

Information systems can not only distribute that scarcest of all commodities - knowledge - but can create knowledge and understanding in socially equitable ways. It is argued that the purpose of business is to solve problems, leading to a better social situation for as many humans as possible. It is only as a by-product of problem solving that profits are generated. One could assume, however, that problems for which no profit can be made won't be solved. Unless, of course, we consider the rapidly-expanding social economy and the not-for-profit sector.

And we shall. Onward! 
