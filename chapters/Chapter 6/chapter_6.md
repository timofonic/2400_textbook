# Chapter 6 - What's coming?

## What's in store in the world of ICT


http://dilbert.com/strip/2015-08-27
![Dilbert replacing workers w robots](https://raw.githubusercontent.com/robertriordan/2400/master/Images/dilbert_robots.png)

Before you decry the above cartoon as being in bad taste, please consider that such technology already exists, and is being put to good, albeit less humerous, use. 
<iframe src="http://riordan.ca/images/hype_cycle_2014.png" height=650 width=1050 scrolling=no frameborder=0 seamless></iframe>

###The Singularity

Way back in 20912, Dilbert predicted the day would come when machines would be intelligent enough (some call this *sentience*) to *create* themselves. Here's what that looked like:

**Figure LJST. Dilbert and the Singularity**
![Dilbert teaches robots to code](https://raw.githubusercontent.com/robertriordan/2400/master/Images/dilbert_singularity.png)

Here's the link to that strip (but there is no context provided by scrolling back and forth in the strip):
[Dilbert The Singularity](http://dilbert.com/strip/2012-01-31)

Now just short of four years later, Dilbert's creator, Scott Adams, returns Dilbert to his prescient prediction:

**Figure LJTP. Dilbert teaches robots to code** 
![Dilbert teaches robots to code](https://raw.githubusercontent.com/robertriordan/2400/master/Images/dilbert_teaches_robots_to_code.png)

Here's the strip so you can scroll back and forth to get the context: [Dilbert The Singularity](http://dilbert.com/strip/2015-12-01)

To begin, let's establish an excellent resource for looking up the gobbeldygook craziness of technology terminology. Need to know what a particular technology means in (almost) human terms? Check out Gartner's [IT Glossary](http://www.gartner.com/it-glossary/). Keep this resource close. You'll use it often.

We begin by looking at Gartner Group's Emerging Technology Hype Cycle. The 2014 version was the most recent available at time of writing.

**Figure EC. Gartner's Emerging Technology Hype Cycle**

![Gartner Group Emerging Tech Hype Cycle](https://raw.githubusercontent.com/robertriordan/2400/master/Images/hype_cycle_2014.png)

Let's unpack the graphic. First of all, the Gartner Group is a large IT consultancy with offices worldwide and a great reputation in the IT consultancy field. Each year, they release research on emerging (up-and-coming) technology, and in a novel way, fit it to a curve of their own design. The curve is both a visual and semantic indicator of the _life_ of a piece of technology from Innovation Trigger (red section on the left of the chart) to Plateau of Productivity (the green section on the far right). [Here](http://www.smartinsights.com/managing-digital-marketing/marketing-innovation/technology-for-innovation-in-marketing/) are some historical ones for reference. 

There are five _phases_ to the cycle. Here's how each is explained by [Gartner](http://www.gartner.com/technology/research/methodologies/hype-cycle.jsp) with the my comments in [square parens]:

"Each Hype Cycle drills down into the five key phases of a technology's life cycle.

"Technology [or Innovation] Trigger: A potential technology breakthrough kicks things off. Early proof-of-concept stories and media interest trigger significant publicity. Often no usable products exist and commercial viability is unproven. [Think of Twitter in the early days. It was cool for sure, but how to make money from it?]

"Peak of Inflated Expectations: Early publicity produces a number of success stories — often accompanied by scores of failures. Some companies take action; many do not. [This is the stage at which so-called *wearables* (such as the Apple iWatch) are currently located. They are all cool and people are tripping over themselves to get them, but is there a real future or is it all hype? To frame it in the context of our book here, will they either reduce input requirements or increase outputs? And if they *do* increase output, we need to very carefully examine whether or not the output is actually *valuable*. As an outlandish example, if the output were to include *average hair follicle growth rate*, would that really be *valuable* as opposed to *cool* or *interesting*... We need to metricise the output to determine if it's affording real value. ]

"Trough of Disillusionment: Interest wanes as experiments and implementations fail to deliver. Producers of the technology shake out or fail. Investments continue only if the surviving providers improve their products to the satisfaction of early adopters. [Technology located here is not a failure! It's simple the life cycle stage where all the *hype* has worn off. It's technology in the spot where it will either live or die; if it lives, it moves on to the Slope of Enlightenment" as productive applications are found and exploited.] 

"Slope of Enlightenment: More instances of how the technology can benefit the enterprise start to crystallize and become more widely understood. Second- and third-generation products appear from technology providers. More enterprises fund pilots; conservative companies remain cautious. [Note that 3D printing technologies are in this phase.]

"Plateau of Productivity: Mainstream adoption starts to take off. Criteria for assessing provider viability are more clearly defined. The technology's broad market applicability and relevance are clearly paying off." [Here we find stuff such as *speech recognition* and *natural language processing*; things that have made Apple's Siri and Microsoft's new Windows 10 Cortana popular.]  

[[Interested?](http://www.gartner.com/newsroom/id/2819918)]

Let's now take a look at some of the technology plotted on the curve, beginning with tech that Gartner thinks is at least 10 years out. Ten years from now, you will be early in your career and probably looking to move up the ladder. What kind of technology can you expect to be maturing (that is, on the *Plateau of Productivity* end of the curve), if it survives the treacherous middle of the curve mortality that fells so much tech. *Human Augmentation* might be entering the profitable stage. The Bionic Human might become a reality. Super-human athletes and soldiers and police and fire fighters could become a routine part of life. And bio-controlled prosthetics could become commonplace. Amputees will be able to function at a level approaching the performance of the wholly able-bodied. 

And now the Emerging Technology Roadmap.

**Figure KM. Emerging Technology Roadmap.**

![CEB Emerging Tech Roadmap](https://raw.githubusercontent.com/robertriordan/2400/master/Images/emerging_tech_roadmap_2014.png)

![CEB Emerging Tech Roadmap 2017](https://raw.githubusercontent.com/robertriordan/2400/master/Images/emerging-technology-roadmap-2014-2017.jpg)

![Value Zone](https://raw.githubusercontent.com/robertriordan/2400/master/Images/value_zone.png)

It's in the Telematics/Usage/Data exhaust box where we are finding a lot of value being generated by what were traditional manufacturers or service providers who now find that their data are as or more valuable than their product/service. Witness Uber, Google, GE and Ford (see the industries tab in OneNote.   

The bait must be tasty to the fish, not the fisher. 

Uber might disrupt parcel delivery (FedEx, UPS, Purolater), pizza and prescription delivery, newspaper distribution, mail delivery and anything else that's on the road. Uber can sell their on-road experience (real time) data to lots of ppl and could probably make a living just being on the road and f the taxi industry shit. What else can they disrupt? Real-time traffic, security patrol (random though), city maintenance surveillance, predictive policing, 

The sharing economy: http://sloanreview.mit.edu/article/adapting-to-the-sharing-economy/

There is a difference between "recall" (to bring back from memory; recollect; remember) and "memory" (the mental capacity or faculty of retaining and reviving facts, events, *impressions*, etc., or of recalling or recognizing previous *experiences*; this faculty as possessed by a particular individual; the act or fact of retaining and recalling *impressions*, facts, etc.; remembrance; recollection). ICT does recall. People do memory. Nothing new can possibly be created out of recall. Memory, especially *curated* memory (which is what we do: "to pull together, sift through, and select for presentation, as music or website content") creates new and novel and revolutionary content.  

### The robot piece

This will provide important balance:

Humans Are Underrated: What High Achievers Know That Brilliant Machines Never Will
Geoff Colvin

Review
“Beautifully written and deeply researched, Humans Are Underrated is one of the most creative and insightful leadership books I have ever read. It is a triumph!”
—DORIS KEARNS GOODWIN, Pulitzer Prize–winning historian

“A powerful exposition of the strengths and limitations of technology in shaping our lives and addressing today’s greatest challenges. More than ever, as Colvin demonstrates, we need people who embody the most human of qualities. An uplifting account of the enduring potential of humanity itself.”
—PAUL POLMAN, CEO, Unilever

“As machines inexorably become ever more competent at doing machinelike things, interpersonal skills, irreplaceable skills of human interaction, will come to be recognized as being even more valuable than they’ve always been. This is an extremely important, highly practical, and indeed exhilarating book.”
—SIR MARTIN SORRELL, CEO, WPP

“Through a series of practical case studies and insights, Colvin clearly demonstrates that—regardless of where the future takes us—emotional intelligence will remain one of the most valuable human skills and the Human Element will remain a differentiator.” 
—ANDREW N. LIVERIS, chairman and CEO, Dow Chemical Company

“Geoff Colvin’s fresh take on how to respond to the rise of brilliant machines and the changing nature of work is as wise as it is inspiring.”
—DOMINIC BARTON, global managing director, McKinsey & Company

“Corporate leaders often say, ‘People come first.’ True innovation is realized only when their actions match their words.” 
—ROBERT GREIFELD, CEO, Nasdaq

Product Description
As technology races ahead, what will people do better than computers?

What hope will there be for us when computers can drive cars better than humans, predict Supreme Court decisions better than legal experts, identify faces, scurry helpfully around offices and factories, even perform some surgeries, all faster, more reliably, and less expensively than people?

It’s easy to imagine a nightmare scenario in which computers simply take over most of the tasks that people now get paid to do. While we’ll still need high-level decision makers and computer developers, those tasks won’t keep most working-age people employed or allow their living standard to rise. The unavoidable question—will millions of people lose out, unable to best the machine?—is increasingly dominating business, education, economics, and policy.

The bestselling author of Talent Is Overrated explains how the skills the economy values are changing in historic ways. The abilities that will prove most essential to our success are no longer the technical, classroom-taught left-brain skills that economic advances have demanded from workers in the past. Instead, our greatest advantage lies in what we humans are most powerfully driven to do for and with one another, arising from our deepest, most essentially human abilities—empathy, creativity, social sensitivity, storytelling, humor, building relationships, and expressing ourselves with greater power than logic can ever achieve. This is how we create durable value that is not easily replicated by technology—because we’re hardwired to want it from humans.

These high-value skills create tremendous competitive advantage—more devoted customers, stronger cultures, breakthrough ideas, and more effective teams. And while many of us regard these abilities as innate traits—“he’s a real people person,” “she’s naturally creative”—it turns out they can all be developed. They’re already being developed in a range of far-sighted organizations, such as:

• the Cleveland Clinic, which emphasizes empathy training of doctors and all employees to improve patient outcomes and lower medical costs; 
• the U.S. Army, which has revolutionized its training to focus on human interaction, leading to stronger teams and greater success in real-world missions;
• Stanford Business School, which has overhauled its curriculum to teach interpersonal skills through human-to-human experiences.

As technology advances, we shouldn’t focus on beating computers at what they do—we’ll lose that contest. Instead, we must develop our most essential human abilities and teach our kids to value not just technology but also the richness of interpersonal experience. They will be the most valuable people in our world because of it. Colvin proves that to a far greater degree than most of us ever imagined, we already have what it takes to be great.

