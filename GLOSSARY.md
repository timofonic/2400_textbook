# Glossary

## software

Computer code designed to accept input, process the input and provide results as output. 

## requirements

What systems are expected to accomplish. What the party requesting the system needs the system to do. 

## ip 

1) Intellectual Property. The embodiment of value in an idea, process or creation of music, art, literature, inventions, and designs of all kinds. It's the value attached to creations of the mind.
2) Internet Protocol (Address). Part of either the IPv4 or IPv6 identification standard in which every device connected to the internet has a unique "address" in the form of xxx.xxx.xxx.xxx (a 32-bit IPv4 address) or xxxx.xxxx.xxxx.xxxx.xxxx.xxxx.xxxx.xxxx (a 128-bit IPv6 address).