# Chapter 4 - The corporate side #

### In which we discuss strategy, Porter, value chain, IT decision making models and corporate governance (and it'll be fun... sort of...)

http://www.gartner.com/technology/research/methodologies/research_mq.jsp

How does a Gartner Magic Quadrant Work?

A Magic Quadrant provides a graphical competitive positioning of four types of technology providers, in markets where growth is high and provider differentiation is distinct:

Leaders execute well against their current vision and are well positioned for tomorrow.

Visionaries understand where the market is going or have a vision for changing market rules, but do not yet execute well.

Niche Players focus successfully on a small segment, or are unfocused and do not out-innovate or outperform others.

Challengers execute well today or may dominate a large segment, but do not demonstrate an understanding of market direction. 



![The big picture of the organisation](https://raw.githubusercontent.com/robertriordan/2400/master/Images/porter_books.png)

Michael Porter is perhaps the best known of all the business strategy gurus
You will see and hear reference to him throughout your academic and professional career
He is perhaps most famous for his work on determining and interpreting what he calls the Five Forces which operate in every for-profit industry
The 5 Forces (P5F) are a strategy tool for use by a firm in an industry to assess and position itself in terms of the dynamics of the industry

Recall that sustainable competitive advantage occurs when an organisation uses organisation-specific resources to accomplish something its rivals cannot 
It’s having or doing something that your rivals do not have in the way that you have it, or doing something in a way that your rivals cannot match
But it’s also possible to hedge your competitive bets by buying into the value chain of your competition…
This is referred to as either forward (on the customer side) or backward (on the supplier side) integration

There are few substitutes for petGRO’s (or other pet industry) products…
Remember that a competing product or service in the same industry is not the same thing as a substitute
So Purina is not a substitute for petGRO – Purina is a competitor
A substitute for a car is a bicycle or public transit or teleworking or agoraphobia…  
Owning the rubber company that produces shoe soles and tires for cars, cycles, buses, airplanes and subways is an example; no matter what the substitute, the firm benefits – a strategic involvement in this way is a great hedge against substitutes
Don’t forget that buying or doing nothing is a substitute
For example you don’t have to go to a movie. You can decide to just sit around… but a pet, once owned, must be fed. There are laws.
But what of substitutes? A rope could sub for a leash. A bucket for a water bowl or a cereal bowl for a feed dish and you could feed your pet table scraps or make your own feed or breed cats to feed your dogs and mice to feed the cats and maybe grow peanuts to feed the mice… but really… pet products are fairly unsubstitutable… 


![The big picture of the organisation](https://raw.githubusercontent.com/robertriordan/2400/master/Images/p5f.png)

![The big picture of the organisation](https://raw.githubusercontent.com/robertriordan/2400/master/Images/frontier.png)

My notion of circularity. There's no other *service function* that also can generate revenue AND implement strategy. IT alignment infographic. 

Must discuss strategic alignment here - promised in Chapter 2.

And governance. week 8 chapter 5 pptx

Is ICT a cost centre or a revenue centre?

Week 3 Chaptr 3 Pareto optimality should go somewhere... ? - decision making

KM Explicit vs Tacit knowledge... critical - somewhere

Week 4 chapter 7 - history of retail innovation.
e-commerce and frictionless economy
Types of e-commerce



See business dictionary definitions such as:

http://www.businessdictionary.com/definition/governance.html
http://www.businessdictionary.com/definition/corporate-governance.html

Plus all the stuff from Module 1 (etc.)

From Module 2:
IS should not be used as a cure-all for organizational problems. Even today, with the knowledge of best practices, project management, and information systems, failures in the implementation of such critical systems like Enterprise Resource Planning (ERP) still occur. In many cases, we cannot blame these failures on technology. The key to developing a good strategy to achieve an organization’s goals is to buy, rent or build but at any rate, have well-designed and well-managed systems available.

Also Module 2:

IT is a crucial part of an organization’s assets. It must be managed like any other asset, through governance and planning. This means that the business impact of IT investments must be as measurable and compelling as any other business case. Strategically, CIOs must justify IT spending and show return the same as any other strategic investment. Competitive advantages because of information technology are no longer sustainable advantages [what does this mean and why is it important?, therefore IT is granted no special dispensation when it comes to budget, capital or other strategic investments.
Using our simple test of whether or not there is less input... think here. 

Ensuring alignment of IS strategy and business strategy
Because of the pervasiveness and importance of IS in the business environment, it is imperative for businesses to ensure that their IS strategies are aligned with and adequately reflect their overall business strategy. If these strategies are misaligned, an organization’s ability to meet its goals and objectives can be severely jeopardized. For example, given the significant percentage of capital investment represented by IT assets, it is critical that these assets, and the systems they support, fit with the objectives and strategies of the business as a whole. The business/IS strategic alignment in the example (increase sales by 10% aligned with a reduction in backup/archive/recovery costs resulting in increased system availability) works because the goal of increased sales as a key business strategy is supported by backup initiative of IS. 

IT Decision making structures (EDIT THIS)
Among the most common decision-making structures are:
· executive or senior management committees which provide ongoing business leadership and involvement in IT—often referred to as an IT steering committee
· IT leadership committees composed of senior IT leaders from different areas of IT, such as infrastructure and development
· process teams made up of IT members and business/IT relationship managers
· IT councils composed of business and IT executives
· architecture committees
· capital approval committees

Of these, CIOs rate IT leadership committees and business/IT relationship managers as the most effective means of ensuring adequate decision making. In both cases IT expertise is matched with business knowledge so that decisions are more comprehensive than departmental.


## The big picture of the organisation ##

We now come to the bigger picture of where data, information, decision, action and knowledge fit in the context of the organisation. The result is the rather busy and complex-looking Figure JL (here titled simply *The big picture* to save some space but with the working title of "The measurement to  knowledge continuum with associated indices, system types and spans, and organisational correlates"). This figure encapsulates the thinking of many who have gone before in the study of ICT and brings an order to the various insights about *how things fit* in the modern organisation. Bear in mind that the analysis and conclusions conveyed by this Figure are based on the author's experience and accumulated *tacit knowledge* of the field. There is no empirical evidence of which I am aware that would give an exact number of how many machine decisions versus human decisions are made. With that *caveat* in mind, let's take a look at it piece by piece.     

**Figure JL. The big picture**

![The big picture of the organisation](https://raw.githubusercontent.com/robertriordan/2400/master/Images/big_picture.png)

### Type of work and who’s doing it
Let’s begin at the left of this Figure, and consider the “Type of work and done by” column. We start at the bottom, left corner of the figure, with the colour-coded Machine / Human distinction. The things above it are all activities performed in all organisations on a regular basis (Measurement, data collection/storage, etc.). Most of these activities we have seen before from examining Figure LLJJ in depth. The leftmost column seeks to inform regarding the relative contribution of human versus machine in the execution of the task activities stacked up on this dimension. The more yellow the activity box, the more humans are the nexus of activity in this particular domain. Conversely, the more black, the more machines are the ones doing the work in this domain. 

We begin at the bottom, considering the activity of *Measurement*. The predominance of black in the yellow/black saturation indicates that most measurement is effected by machines. There is simply so much measurement to be done that it’s impossible for humans to do it. Sensors shoulder the majority of the load in this activity. There are just so many sensors measuring so much data that it’s impossible to even quantify the volume of data measurement going on. The volume is truly mind-boggling with, as we know, the data equivalent of 200,000 years of DVD-quality movies being generated every two days. 

Next up is the activity of *Data collection and storage*. Again, the vast majority of the activity in this area is effected by machines. Almost no human activity here at all. There are countless millions of corporate, government and private databases and data warehouses holding immeasurable archives of data backed up in multiple locations. Imagine how many redundant copies of *Dropbox* data there must be in order to provide seamless availability and rock-solid failover of those often mission-critical data. If Dropbox lost data even once, their reputation would take a massive hit.

We next find *Context*, where we see the dominance of machines being reversed, with human activity taking the lead. This won’t last long however, as machines are coming to the fore in terms of providing context through the mass deployment of sensors and artificial intelligence. More later.

With context created, however, we find machines being responsible for the majority of *Information creation*. This assumes that the contexts created by humans are being turned over to machines to create the information necessary to make decisions. There is simply just so fucking much information out there that it’s impossible for humans to keep up. In order to make decisions that return value to the decision makers, it is imperative that all available and pertinent data be applied to the context around the creation of information. And that's what ICT is doing.

The notion of *bounded rationality* is pertinent here. Wikipedia offers the following:

"Bounded rationality is the idea that in decision-making, rationality of individuals is limited by the information they have, the cognitive limitations of their minds, and the finite amount of time they have to make a decision. The concept of bounded rationality [...] account[s] for the fact that perfectly rational decisions are often not feasible in practice because of the finite computational resources available for making them."

[Interested?](http://en.wikipedia.org/wiki/Bounded_rationality)

The notion of bounded rationality is rooted in the fact that humans can process only so much information in a particular time frame thus, we, as humans, actually simplify the decisions we must make because we just don't have the horsepower, time or resources to make a fully-informed decision. {See, for example, *Bounded Rationality: The Adaptive Toolbox*, Gigerenzer, Gerd and Reinhard Selten (eds.), The MIT Press,July 26, 2002.) The same is increasingly *not* true of machines. Machines can construct elaborate contexts, filter and process massive amounts of data in near real-time and come to incredibly informed decisions for extremely complex problems. Humans need no longer be constrained by limited information, resources, computational power or time in tackling many heretofore impossibly complex problems. The machines can do so much for us. Witness *The Human Genome Project* [Interested?](http://www.genome.gov/12011238) or the *Large Hadron Collider* [Interested?](http://home.web.cern.ch/about/updates/2015/03/lhc-experiments-join-forces-zoom-higgs-boson) or, in the ultimate expression of context, how about IBM's Deep Blue computer beating the world chess champion [Interested?](http://www-03.ibm.com/ibm/history/ibm100/us/en/icons/deepblue/) or IBM's Watson defeating the reigning Jeopardy champs on national TV [Interested?](http://www.techrepublic.com/article/ibm-watson-the-inside-story-of-how-the-jeopardy-winning-supercomputer-was-born-and-what-it-wants-to-do-next/). 

When it comes to *Decision*, the activity is currently being done mostly by machines. But this dimension is unique. It's not a measurement of *volume* of decisions made. If that were the case, machines would win hands down. This dimension rather reflects the *importance* of the decisions made. Humans still make many of the critical and consequential decisions. We are the last resort. We always insist on *speaking to a human* when we need a critical decision made or to hear our arguments or listen to our entreaties. It's an open question whether this human/machine balance will persist. Especially with new *sharing economy* services such as Uber and Lyft ride-sharing (and others). Increasingly, a software layer is being injected between those creating the services and doing the marketing and finance (the secondary value chain activities) and the workers who are providing increasingly commodified work. So dispatch decisions for Uber cabs are done entirely by machine. All decisions are made with a maximising algorithm. While the drivers are overwhelmingly in favour of working for a machine (instead of *the man*), the bliss might be short-lived. When it comes time to cut costs, or when really cheap alternatives to human drivers come along (driverless cars, for example, which aren't far away at all) the *algorithm* might not be your friend. [Interested?](http://www.cbc.ca/radio/popup/audio/player.html?autoPlay=true&clipIds=2658630111).

Increasingly, however, machines are taking the *Action*. Machines carry out a myriad of instructions on our behalf from those we cannot do because we are simply not strong enough (think trucks and airplanes) to those we are not biologically equipped to do (think landing on Venus and sending back data for 10 years). Machines do much of our dirty, repetitive, mind-numbing and back-breaking work. And for that, we thank them.

When we get to *Knowledge*, however, it is humans who again take control of the process. But for how long? NEED MORE HERE. AI and Neural Networks, etc...

### Cost, complexity, variety and granularity
Moving to the next column to the right, we see an infographic indicating relative cost and complexity of the work at the various levels. In general, as we move up the measurement to knowledge ladder, the cost of working at each successive level increases at roughly the same rate as increases the complexity of the operations performed and the variety of data sources with which one must deal. The higher you go, the more expertise and computing power is required with a commensurate increase in cost. Nothing is free.

Moving to the next column to the right in Figure JL, we find the title Granularity. Granularity refers to the amount of data embedded or encapsulated in the information being presented at each level. At the bottom of the spectrum, we find all the Transaction data that each organisation must collect. The granular (small, tiny) detail represented on a bill of sale, for example, might include the exact product number, product model, version or revision, the exact quantity and the price charged for each. The date and time of the transaction and the ship to and bill to addresses and the salesperson responsible might also be included at this level of detail. This is a very fine level of data, much like the grains of sand on the beach to which we were alluding. This is the type of data that is measured in the bowels of the organisation. The data that the firm must collect in order to be able to account for itself. 

This is the level of data where measurement occurs, where raw data are collected and stored and marshalled to provide insight and context to the higher levels of the organisation. You can't give the CEO a total sales figure for last year without a summation of the totals on all the individual sales invoices from last year. So as we move from bottom to top in an organisation, we move from very granular (grains of sand) up through small stones, through boulders to the mountains required by senior management. 

There is a relationship between the Type of work done and the granularity of data required. But we mustn’t lose sight of the fact that often, once taking a look at the mountain, senior managers and executives will want to take a walk on the beach and feel the sand between their toes. Thus data must roll up to big-picture numbers, but must also persist and be available for *drill-down* exercises so that sense (read *context*) can be made of the big pictures. Data are rolled up and down with great regularity and must be available when required at the level of detail appropriate for the task at hand. And the task at hand involves problem solving at each level.

### Decisions, decisions, decisions (in the spirit of Jan Brady's *Marsha, Marsha, Marsha!*)

Problems, and the decisions that accompany them, come in three flavours: *structured, unstructured* and a hybrid, middle ground called *semi-structured*, with elements of both structured and unstructured decisions.

*Structured Decisions* are algorithmic (A x B / C) in nature where all quantities are measured and known. This leads to a verifiable result --  there is a correct answer and we can assess our answer against that standard. An example of a structured decision is the calculation of the sales tax on a purchase. There is little or no *context* involved here. Just the facts ma'am. 

An *Unstructured Decision*, on the other hand, has no standard against which to measure the decision. Decisions such as whether to open a new store or launch a new product or hire a new CIO. The quality of such decisions can only be accurately assessed after the fact. Time will tell if the new store does well. This is not to say that the decision is made blind. Context provides important parameters to guide the creation of information that will lead to the decision. The important point is that there is no correct answer going forward. We can reduce error by creating a rich context and valuable information, but we can never eliminate potential error it the way we can by testing algorithms for the calculation of sales tax, for example.

The final decision type is the hybrid, *Semi-structured Decision*. Such decisions have elements of both previous types, such as would be found in an investment decision. The rate of return on an investment can be calculated in a structured fashion once the investment amount and the rate are known, but the actual rate itself is not and cannot be known in advance. Thus the decision to invest in this versus that opportunity is made without a standard against which to measure, other than the history of investments *like this one*. Again, we can reduce the error around the decision by creating a rich context (examining detailed history and doing our homework about the marketplace and anticipated trends), and the actual monetary value algorithm can easily be verified. It’s just the actual rate of return that cannot be known with precision in advance.

Note the correspondence between type of work and type of decision. Knowledge work is inherently unstructured, involving the synthesis of various sources and the exercise of judgment involving *tacit* knowledge. Tacit knowledge is differentiated from *explicit* knowledge in that the former is knowledge that comes from experience and wisdom, whereas the latter is codified, rules-based and written down. A CEO often operates on *gut instinct*. This is tacit knowledge. A transaction processing system has no gut feelings. In fact, it has no gut at all. They follow ridged rules. 

#### Organisational levels - Taking the hill
In considering organisational levels, we need to talk about how stuff gets done; about what strategy is and how a strategic direction gets set and translated into actionable directives in an organisation. 

The online Business Dictionary defines [Strategy](http://www.businessdictionary.com/definition/strategy.html) as: 

"1.A method or plan chosen to bring about a desired future, such as achievement of a goal or solution to a problem.

2.The art and science of planning and marshalling resources for their most efficient and effective use. The term is derived from the Greek word for generalship or leading an army. See also tactics."  And we shall, *viz*.

[Tactics](http://www.businessdictionary.com/definition/tactics.html) are defined as: 

"Means by which a strategy is carried out; planned and *ad hoc* activities meant to deal with the demands of the moment, and to move from one milestone to other in pursuit of the overall goal(s). In an organization, strategy is decided by the board of directors, and tactics by the department heads for implementation by the junior officers and employees."

A simple analogy is found in war. 

At the highest level, strategy gets made in the smoky war rooms. General Highbottom declares: “In order to win this grand battle, we must take control of that hill!” Next comes the tactical level, which makes possible the strategic. Colonel Mittlestaff receives the strategic direction from Highbottom and declares that in order to take that hill, “We’ll need 100 soldiers and 10 tanks and 5 mortars and 3 APCs. Split the infantry soldiers into two groups. Send 50 to the back of the hill with the mortars. Bring 5 tanks in from each flank. Send 50 infantry up the hill in the APCs once the tanks and mortars have disbursed the enemy and weakened their resistance.” 

Mittlestaff then pushes the tactical plan down the line to Captain Gitterdunn, who works at the operational level, at the interface with the enemy. Gitterdunn receives the tactical directives, is given access to the human and physical resources and proceeds to organise and execute the plan, with the help of subordinates such as Sergeant Spitzumfire, according to the tactical directives. The soldiers, such as Private Gruntz, are equipped, taken into battle and expected to succeed.  

Gitterdunn reports at regular intervals on how the battle is progressing. In turn, Mittlestaff (who might be coordinating several different battles), informs Highbottom, who has the highest view in our scenario. Much of the *gut work* is done by Mittlestaff. Middle management has forever been under-appreciated. As we will see, this group faces a particularly dim employment future. 

Take a look at Figure PP below and note especially the directional arrows for Strategy and Information:

**Figure PP. Strategy, tactics and operations – the case of taking a hill** 

![Strategy](https://raw.githubusercontent.com/robertriordan/2400/master/Images/strategy.png)

You can see that information flows both ways in this scenario. From the top, the messages sent down are “Do this!” The messages pushed up from the field are “Here’s how it’s going.” Each player and level has a different view on the situation and each requires different information in order to fulfil their role. It’s the same in any organisation. Different roles require different information, and different information dictates different systems. 

Et *voila*. From strategy to operation. The similarity to the situation at your own workplace is striking... 

At what level of the organisation are we talking when we’re talking about knowledge and low-granularity data and unstructured decisions versus the TPS type of data and decision? Let’s start at the bottom of Figure JL in the *Decision type* column. 

Measurement and data collection/storage are TPS responsibilities, and TPS deal with highly granular data; the TPS is on the beach. Such data are suited to structured decisions. There are no nuances or shades of meaning or complicated contexts to navigate. It’s black and white. This is the *shop floor* area. The showroom, the warehouse, the factory. The staff here are concerned with the very nuts and bolts of the organisation and require systems that are unambiguous, straightforward and next to fool-proof. They must be fast and efficient and they must work as advertised. This is the *operational level* of the organisation. But make no mistake; the data must be accurate and fresh since they provide the foundation for all that is to come as we move up the line. The overall health of the organisation – how well it is performing according to the tactical direction from the middle – is measured here and is pushed up the chain of command in the form of increasingly rolled-up data. Thus the upward arrow under the word *Operational.* 

At the other end of the spectrum we find the exact opposite in terms of data, type of work and type of decision. Up there at the senior management and executive levels, unstructured decisions and nuanced contexts are the daily diet. This is where strategic decisions are made and those strategic goals are pushed down the management chain, as illustrated by the down arrow under the word *Strategic*. Strategic direction is pushed to the middle management level where it is translated into tactical directives for enforcement at the operational level. Thus middle management is the *sandwich meat* so to speak, accepting directives from the slice of bread above and translating and pushing to the slice below, from where it receives and transmits the status of the tactical directives up to the top slice. No baloney. 

### Business intelligence
Business Intelligence is created through the use of both internal and external data in order to gain insight into the (you guessed it) *context* of the organisation. BI provides metrics on the complete range of activities of importance to the organisation through organising, analysing and synthesising the organisations *information assets* and making them available through a variety of means, but increasing through web-based applications and *dashboards*. Note the almost one-to-one correspondence between the activities of BI and those in the Type of work column. BI's *Acquisition phase* (collecting and marshalling data) lines up nicely with the Measurement and Collection/storage functions of the organisation. *Organisation* meshes nicely with collection/storage, shading into Context creation. And so on up the hierarchy. This reinforces the notion that Data engineering is an important function - making certain that the right data are available at the right time in the right place for the right function. 

**Figure PPM. A wealth management dashboard (information-driven app) example dashboard**

Note the various metrics displayed in an easily-understandable format. The displays are continuously updated. Take a look at the video.

<iframe width="560" height="315" src="https://www.youtube.com/embed/1_lIXq4uW8M" frameborder="0" allowfullscreen></iframe>

[Interested?](http://www.informationbuilders.com/business-intelligence)

### System type and span 
This is where we need to start talking about the various ICT systems that support the work at each level. 

#### Transaction Processing Systems (TPS)
Enter TPS, or *Transaction Processing Systems*. As the name suggests, TPS are masters of the often massive number of tiny transactions and events that underpin almost every organisation (even your own personal organisation). Each sale a firm makes or service an agency provides is a transaction. Each hiring and firing and retirement of a staff person is an event. Accounts receivable and payable when received or paid are transactions. As are pay cheques and loan payments and stock purchases and corporate tax payments. Everything that goes into or out of the firm is a transaction, as are all the small events and trades that occur within an organisation. *Gazillions* of activities. And they must all be recorded in a way that allows them to be saved, rolled up, boiled down and retrieved in all their granular detail for many years to come. This is how the organisation accounts for itself. 

TPS are primarily involved in the measurement, collection and storage functions of the organisation. TPS systems do validation (ensuring data are correctly formatted and respect the rules of the process for which they are gathering data), sorting, filtering, listing and basic calculations. They handle marshalling and storage. And through reporting functions, they will tell you everything they did and everything they have in storage. Examples of TPS include: stock control systems to maintain inventory, order processing systems to replenish stock, payment systems (other than payroll – for accounts receivable and payable, for example), payroll systems, and reservation systems. 

#### Management Information Systems (MIS)
Transactions form much of the grist for the analysis types that follow up the ladder. *Management Information Systems* or MIS, are systems used by lower to middle management types who must make decisions in the context of operational realities. MIS include systems such as human resource management, sales management, supply chain management and budgeting. Sometimes used as a blanket term for a wide range of organisational systems, MIS are in fact a particular set of tools used by a particular set of managers. We will come back to this.

Context is important for these types of decisions, and MIS can provide context as they contain the rolled-up transactions from the various TPS systems in the organisation and can cross boundaries and pull data from all functional areas. The scope of MIS is internal. They don’t deal with data that is external to the organisation. They support predominantly structured decisions but can shade into semi-structured decisions with help from either humans or DSS (considered below).  

MIS help firms to manage the myriad processes that must be managed on a day-to-day basis. They aren’t about strategy but are rather about operations. Boots on the ground. They deal in structured data (known and predictable format) that is of low to medium granularity, cost and complexity. They sort, prioritise and merge data from different internal sources and then summarise it, providing rich context. They will also tell you anything you want to know about what they have done. They have excellent reporting facilities but can’t tell you about the future. They deal strictly in the past up to the present and are not at all strategic. 

Another way to think of MIS is to approach it in terms of supporting particular business activities in the *value chain* along two dimensions, namely *primary* and *secondary* activities. The value chain is a connected series of activities, each of which adds value or supports the addition of value to the firm’s goods or services. *Primary activities* create the most direct business value for the organisation and its customers. Primary activities in order along the value chain are *inbound logistics*, *operations*, *outbound logistics*, *marketing and sales* and finally *service* (encompassing the intricate relationship between product and customer). 

*Secondary activities* are conducted in support of or expansion of the business value that is created by the primary activities. These include *firm infrastructure* (which is often also called administration, including Accounting, Finance, Communication and Legal), *ICT*, *quality assurance* and *product development* (R&D), as well as *human resource management* (HRM) and *procurement* (sourcing and purchasing).

A final way to conceive of the various systems that are required to run and organisation is the take the *supply chain* perspective. Every business has a supply chain, which is a *system of organisations, people, technology, activities, information, and resources involved in moving a product or service from suppliers, through the firm, to the customer.* Supply chain activities transform natural resources, raw materials, and components into a finished product, delivered to the end customer. In sophisticated supply chain systems, used products may re-enter the supply chain at any point where residual value is recyclable. For example, reclaimed steel, paper and plastic are often reused in several industries.

There are multiple systems available from multiple vendors to support and automate each of these functional systems. And that could be part of the problem. Read on. 
 
#### Decision Support Systems (DSS)
Next up are *Decision Support Systems*. DSS, as they are most often to referred to, are knowledge-based systems used by senior management to assist in the complex process of making decisions. They are, in fact, *context systems*, creating the rich soup of data, information and previous knowledge that allows organisations to *learn*. They are best suited to assisting in the complex process of making *semi-structured* and *unstructured* decisions. Tapping databases of TPS and MIS data for their *memory*, they do *what if* analysis (discussed below), perform forecasting to simulate the long-term effects of various decisions and are adept at distributing their results through reporting and integration into various *dashboards* displaying the health of the organisation.

Several different *flavours* of DSS are in use, including *Group Decisions Making Systems* (GDMS), *Logistics systems (logistics is concerned with getting the right thing to the right place at the right time in the supply chain) and financial planning systems (where forecasting comes in quite handy). Systems as ubiquitous (found everywhere) as spreadsheets such as Microsoft Excel and Apple Numbers can and are also considered to be DSS. Thus every enterprise large or small has access to a DSS. It’s just knowing where to find then and how to use them. 

##### What-if analysis
*What-if analysis* answers the question “What is likely to happen to *x* if we do *y*?” So what is likely to happen to our sales if we raise prices by 5%? Or what will be the impact on our bottom line if we ramp up production to 125% next fiscal year? In a structured analysis, using historical data and *algorithms* (a step-by-step set of operations to be performed on data that yield a predictable and verifiable result - sort of like structured decision making eh?) designed to produce outcomes along a continuum from worst to best, what-if analysis can reduce the error around a decision by allowing the range of potential outcomes to be examined before the decision is made to embark on a tactic. For example, in the scenario where the impact of ramping up production is being examined, it might be concluded that, while the short-term implications are serious (spending on new machinery, new hires and new warehouse and fulfillment capacity will impact the bottom line in the near-term), the longer-term gain from increasing capacity will more than sooth the short-term pain, and overall ROI will be quite favourable. Taking the short-term outlook would lead to erroneous conclusions. A simple spreadsheet model can illuminate the way forward. What-if analysis uses historical data in an algorithmic model to produce a range *likely* results based on past performance and likely best guesses about future parameters. It's not a *fortune-telling machine.* 

#### Executive Information Systems (EIS)
We now move on to the penultimate (next to last) set of systems, those used by the very top executives in the organisation. Occupants of the *C-suites* need a specific set of tools with a specific set of characteristics. Such systems allow for analysis of the context (environment) in which the organisation finds itself (the wider set of circumstances illuminated by a PESTLED analysis, for example), illuminate long-term trends and possibilities, and suggest potential strategic initiatives to address the longer-term trends identified. The data in such systems is often poorly structured (which is an unfortunate word for it since poor structure does not automatically mean bad data -- rather the data might be quite rich as a *function of* it's lack of 'proper' structure) or completely unstructured and makes use not only of rolled-up TPS and MIS data, but also data from sources external to the organisation (as suggested by the PESTLED analysis). EISs are future-oriented, support unstructured decision-making, and are designed with simplicity and intuitiveness in mind so that executives can easily manipulate and customise the system for individual circumstances and preference. An EIS used by an oil exploration executive would look much different from one used by a senior federal bureaucrat, but both are the same *under the hood*.

When we say that a system characteristic is *simplicity* it is not to be confused with *simplistic* or *immature*. Quite the opposite. As a long-time systems designer and developer, I can attest to the fact that making something appear simple and, moreover, to be *intuitive*, is the product of very hard work. The simpler a system appears on the outside, the more work has gone into the back end of it. My mantra is "Simplicity is difficult." Words to live by.  

#### Enterprise Systems
The final type of system we will discuss here is the most all-encompassing of all, the so-called *Enterprise System* or ES for short. Enterprise systems are just as their name suggests – they target or encompass a wider target of functions that the MIS, for example. Their scope is the enterprise. Recall that MIS includes systems such as payroll and sales support and personnel (HR) functions. What happens when an organisation is structured around the various * functional silos* in terms of function? Take a look at Figure PB below.

**Figure ESA. The siloed organisation**

![A siloed organisation](https://raw.githubusercontent.com/robertriordan/2400/master/Images/silo.png)

In a so-called *siloed* organisation, functional areas of the firm (such as Sales and Marketing) operate in silos much like farm silos, one of which might  contain only corn and another only oats, etc. and they never mix. They operate independently of each other, even though they are on the same farm and farmed by the same farmer.  Silos in organisation create problems such as:

- Delays in communication where protracted lead time results in problems getting raw materials from suppliers 
- Expanded cycle time, or the elapsed time from the beginning to end of a process
- Excess inventory in order to compensate for lead time and cycle time delays, a firm must have finished product or raw materials on hand – this costs money
- Lack of visibility in terms of the overall workflow within the organisation – issues such as confusion over the status of process when it’s currently in other parts of organisation (lack of communication between functions means that only the current function knows the status of an order making its way through the firm, for example)
- This lack of visibility leads to Point of contact confusion -- who has the current order status? Sales? Accounting? Customer Service?
- Performance metrics are a challenge – it’s difficult to know how well a process is performing over time because of the lack of process visibility

Figure PB shows an organisation where each functional silo has its own data store (represented by the multicoloured cylinders at the bottom of the figure). Pity the poor Senior VP responsible for Sales, Marketing and Customer Service (represented by the horizontal green bar labelled *SVP* above those three functions) who needs data from each department in order to do some forecasting. The database of each department must be accessed (and each database might be from a different vendor with a different file structure and data characteristics and refresh cycle), and the data from each must be formatted in a compatible way with corresponding dates and reporting periods in order to make sense of the overall picture in the VP's division. Even though each department might be using a *state-of-the art MIS optimised for their function*, note the path of requests and returns, represented by the blue and yellow paths up and down the silo and out to and back from the data stores. Often systems just don’t play nice with each other. 

In siloed organisations, even the sharing of information *between* functional areas can be a challenge, as requests must be made up the chain to senior management, passed down to the target silo, back up to the management level and then back down to the requesting silo. Neither efficient nor effective. Now move up one to the Cxx level and imagine, since now some lower boundaries are crossed (note the CFO is responsible for the Customer Service function rather than the Chief Marketing Officer -- stranger things have and do happen in organisations). Imagine the reporting nightmare of trying to integrate data across so many functional and management boundaries.  

What to do? As a first step, some rationalisation of data stores is in order. Looking at the organisation from a *process viewpoint*, we realise that the Sales, Marketing and Customer Service functions all relate to the *customer interface* and share more in common with each other than they do with Manufacturing or Finance, for example. So we might thing to rationalise around an enterprise system that has a customer focus. And there's just such a beast: the *Customer Relationship Management* or CRM system. Take a look at Figure CSV. Note the only real change? Functions under the same VP now share databases, making reporting much easier at that level. The databases are bigger, more sophisticated in terms of reporting and relationships and allow for data to be stored and formatted in compatible ways for reporting purposes in this management structure. That’s a great first step. But really, we have only *reduced* the pain. It’s not a cure. Note the CFO would experience the same problem getting compatible and timely reports as before, as she or he must cross systems boundaries to get rolled-up data. Not a complete solution at all.

Examples of enterprise systems include: 

- *CRM* (Customer Relationship Management) -- manages customer-related processes
- *PLM* (Product Lifecycle Management) -- handles creation and implementation of new products
- *SCM* (Supply Chain Management) -- deals with procurement, inventory, and other supply chain processes
- *SRM* (Supplier Relationship Management) -- helps working with suppliers, vendors and service providers

Each of these types of ES concentrate on a particular aspect of the value chain. Note they are *managing* some process or aspect, such as *Customers* or *Suppliers* or *Products*. Using such systems, while productive (efficient and effective) in and of themselves, does not get us to the truly integrated enterprise stage that we seek. Read on.

**Figure CSV. An enterprise systems organisation**

![An ES organisation](https://raw.githubusercontent.com/robertriordan/2400/master/Images/es.png)

You are probably already guessing what the ultimate fix might be, and that is the enterprise system to beat all enterprise systems -- the mother of all ESs -- the *Enterprise Resource Planning*, or ERP, system. The ERP organisation is a medium to large (to enormous) operation with enough functional complexity and cash to warrant such a comprehensive and expensive solution. ERP (Enterprise Resource Planning systems) automate and rationalize key business functions such as financials, human capital, operations, and corporate services, and also provide complete *snap-in* solutions for a wide variety of industries (such as pharma, auto, defence, agriculture, retail, etc…) which integrate and automate all functions of an organisation. Take a look at Figure GST below and spot the change.  

<a name="erp_org"></a>
**Figure GST. An ERP organisation**

![An ERP organisation](https://raw.githubusercontent.com/robertriordan/2400/master/Images/erp.png)

While this is a *massive* oversimplification of all the benefits or ERP, it serves to illustrate its most basic principle: that a transaction should be *atomic*, that is, there should only ever be one instance of a thing such as a *sale*. The sale, as an *entity*, should make its way through the organisational value chain but should never exist anywhere other than in the value area in which it resides. So there should not be an instance of the sale in Accounting, and in Finance, and in Sales and in Shipping. There is only ever *one* sale. We can track its position but never copy it. Because a sale, like any other object in an organisation, has a lifetime. We can watch it and interact with it and query it to ask what it's up to through its lifetime but we must never clone it to fulfil the siloed needs of functional areas. And enterprise database *backends* help to fulfil this very basic requirement. We will discuss *backends* in Chapter [system development].

What are the benefits of Enterprise Systems (such as ERP)? According to Rick Mullin, *ERP Users Say Payback is Passé*, (Chemical Week, Feb. 24, 1999) as quoted in CGA stuff waiting for permission, they are plenty: 

- inventory reduction 
- personnel reduction 
- productivity improvement 
- order management improvement 
- financial close cycle reduction 
- procurement cost reductions 
- cash management improvements 
- revenue increases 
- transportation/logistics cost reduction 
- maintenance reduction 
- on-time delivery improvements 
- information visibility 
- improved processes 
- customer responsiveness 
- cost reduction 
- integration 
- standardization 
- flexibility 
- globalization 
- improved business performance 
- supply chain management

What's not to love? 

Note the major change – one massive database has replaced all functional and MIS systems. While this is the most *obvious* change, it is by no means the *only* change that organisations undergo when adopting an ERP.  Next in line in terms of significance following the integration of all company data stores into a single, transaction-oriented database, the largest impact that an ERP has on an organisation is the requirement that the firm examine and alter all its business processes to match the *best practice* standards enforced by the ERP system. Don’t want to do business our way? Then you won’t be running SAP or Oracle or Microsoft ERP systems. You play by the rules or you don’t play. This is the most common reason cited for failed ERP installations – the inability (or unwillingness) of the organisation to change the way it does business to match the ERP’s standards.

Let's take a quick look at role-based visibility and scope as it applies to organisations using ERP-type software. Figure KT shows the breadth of visibility of data and information corresponding to position in the organisational hierarchy. 

**Figure KT. Role-based scope of visibility**

![An ERP organisation](https://raw.githubusercontent.com/robertriordan/2400/master/Images/scope.png)

The *visibility* of data/information refers to the reporting structure. Starting at the very top, note the great, grey pyramid radiating downward from the CEO position. The CEO *sees* all data and their Executive Information System software ensures that they have complete visibility of all data because their *role* as Chief, demands it. So their role-based visibility is 100% wide in scope. Others in the hierarchy have different visibility of, and responsibility for, more restricted domains. Not all the roles are scoped out on this Figure. You can see what the others would be by simply tracing the functional responsibilities up to each successive level of reporting. Such *role-based* visibility is easily effected in enterprise software.    

##### ERP vendors

There are several large vendors of these enormously complex systems, and some of the names of the players in this market will be familiar to you. You may or may not know that the world’s largest provider of enterprise application software is not Microsoft or Apple of even IBM. It’s *SAP*, a large German software house, started by former employees of IBM. 

[Interested in SAP?]( http://bit.ly/1MDeI6Y)

Other big players in this market are the database giant *Oracle*, who have over the past few years gobbled up other equally large players such as PeopleSoft and Siebel, whose offerings included Financial Management Solutions (FMS), Human Resource Management Systems (HRMS), Supply Chain Management (SCM), Customer Relationship Management (CRM - from their acquisition of Siebel), and Enterprise Performance Management (EPM) software, as well as systems for manufacturing and student administration. Microsoft is also a player in this general ERP marketplace (with special focus on small to medium-sized firms), but the biggest by far is SAP. Walk through any airport and you can’t help but notice the big ads for SAP. 

#### When ERP fails

What happens when organisations can't make the necessary adjustments to their business processes to allow the ERP to run their business? There are several high profile cases. IN the Canadian context, the biggest splash was made in 2001 when grocery giants Sobey's has a very public spat with SAP over a database failure (Sobey's was using IBM's DB2 software as their enterprise database system). [Interested?](http://www.computerdealernews.com/news/sobeys-fires-sap-over-erp-debacle/22906) and [a slightly different perspective](http://www.itworldcanada.com/article/sobeys-says-goodbye-sap/29540). But they apparently kissed and made up, as Sobey's implemented an SAP system a few years later [Interested?](http://www.itbusiness.ca/news/sobeys-gives-sap-a-second-chance/10471).  

In 2008, Mountain States Entertainment made the news for a big ERP flop [Interested?](http://www.erpsoftwareblog.com/2011/01/erp-implementation-failure-you-be-the-jury-part-1/), and in 2004 Marin County California's failed implementation led to a lawsuit against Deloitte [Interested?](http://www.computerworlduk.com/news/it-business/20575/deloitte-sued-over-failed-erp-project/).  A more academic treatment is found in a Harvard business case written by A. McAfee, entitled *Rich-Con Steel*, (Harvard Business School Case 9-699-133, HBS Press, January 27, 1999).

But the knife cuts both ways. ERP business process is based on best practices (extensive research, testing and proof from the field that the ERP-recommended way to do business is the most efficient and effective way. The opportunity for an organisation to actually stop and look at its processes – which may have grown up over years in a patchwork quilt of expediency and become inefficient and redundant – is an amazing opportunity. 

Key features of ERP include dealing with key business functions such as:

- Financials, including:
    - Tools for core accounting and reporting capabilities
    - Financial links to supply chain processes (paying suppliers)
    - Functions for compliance with national and global financial regulations
    - Monitoring cash flow

- Human Capital Management
    - Empowering employees to manage personal information
    - Allowing department-level collaboration for corporate budget (sharing of information)

- ERP Operations
    - Automate and manage day-to-day organisational activities

- Enterprise Systems (ES):
    - Enable a company to link its business processes
    - Remove silo walls between business functions leading to an integrated business solution
    - Remove friction between functions, individuals and processes to promote optimal workflow
    - Operate in real time (not batch or offline so everything is instantly up to date)
    - Are scalable (can grow and shrink as needed) and flexible (somewhat…)

There are downsides to implementing such large-scale solutions, and one of them is that they are expensive – not at all for the faint of heart or light of wallet. A 2013 report form the City of Toronto, for example, cited the all-in cost of some $69M to implement an SAP system. They write, “The FPARS project is a transformative effort that aims to restructure the financial planning, budgeting, accounting, human resource and management information systems and processes by implementing SAP as an integrative solution. Improving the City's financial and organizational management functions has the potential to produce significant benefits both in terms of greater internal efficiencies and enhanced value to the public in improved transparency, accountability and the delivery of higher quality of services.” 

[Interested in what Toronto did? Take a look at the first 5 pages](http://bit.ly/1MEPuFm)

As a final note on the virtues of an integrated solution, it must be noted that for any business transaction (such as a sale) there exists only *one instance of that object*. This means that there are not multiple *Sales Spreadsheets* floating around the organisation, some with and some without that sale included. Fresh data is obtained as needed (or even automatically populated to reports as they happen) from a single source. There isn't a record of that sale in the sales database, and another in the warehouse database and another in accounts receivable, etc. One sale object with various *properties* of that object exists in one place. This is an enormous efficiency booster for organisations. (More on objects and properties in Chapter 3). Bottom line message: single object for all transactions - no copies of stuff everywhere. 

Let's now return to our discussion of Figure JL where we left off, which was as we finished off the final vertical pieces. We now move to considering the horizontal dimensions of the figure in the context of the organisation.

We have explicitly stated here that different levels of workers in an organisation require different data resources in order to perform their duties. People at different levels of the organisation (just like those in different functional areas of the firm) make different types of decisions and the impact of those decisions is also different. The flow of information up and down the organisation is also dictated by one's position in the hierarchy. All of this implies that different functions at different levels require different information and different systems in order to work effectively. Much of the complexity of matching systems with functions and levels is taken care of with the ERP solution. An integrated, role-based system (your view of the data in the system is based on your assigned role in the organisation -- data entry clerks have a limited view whereas Senior VPs have a much broader view). 

### In-memory computing (IMC)
Our final system type is also the newest trend in systems. IMC or *in-memory computing* is becoming increasingly necessary as the number of sources and the volume of data increases exponentially - we're talking about the so-called *big data*. Not only do organisations have mountains of their own data (transactions, enterprise applications such as CRM and SCM and whatever data are generated by various lines of business, affiliates, employees, suppliers, retailers and the entire supply and value chains), they also have access to unfathomable amounts of data from new sources, such as is generated by social networks and the internet of things. Massive streams of data are flying past the organisation every second, and taking advantage of it is a challenge well worth taking. 

Here's the problem. Traditionally, data were measured, collected and stored by specific systems (TPS) and then passed *over the wall* to analysis systems for *rolling-up and massage* by MIS, DSS and EIS (considered above). This *separation of church and state* caused two problems" First, there was a degradation of detail as data were massaged and combined and made to fit the *analytics regime* of the organisation. Data were altered, dumbed-down and forced to fit the organisation's analytical tools. Don't worry too much about this particular point. Just take it as given that there was a real loss of intrinsic value as data moved up the chain because the data source and the analytics tools were not developed in tandem to work together. They serve fundamentally different masters, and that's a problem.

The second problem is *latency*. Latency is best understood as the problem of *sitting around* waiting for something to happen. The difference in time between clicking a button that says *Give me the answer* and the actual answer being provided is latency. It's idle time. It's time that your competitors can use to get the jump on you. Latency must be reduced, and that's the history of computing in a nutshell. We need and indeed *expect* instant gratification. That's how we run these days. 

*Forecasting* is an awful lot like *fortune telling*. Or like trying to drive forward in a big truck while looking only in the rear view mirror. All we have at our disposal to reduce the error around predicting the future of something is what we have observed in the past, and some assumptions about how things will unfold. But roads have bumps and curves, and looking behind you at the road you've already travelled won't greatly help your forward navigation. This is an important challenge for any organisation (and indeed for you, yourself). 

There are three types of strategic forecasting:

1. *Diagnostic* - the is the *rear-view mirror* approach. Diagnostic in this context means the same as it does when you visit your doctor - your doctor often does a diagnosis to *figure out how we got to the current state*. To ascertain what's wrong (less often, what's *right*) with you. A path is drawn from previous events or realities to the current moment. This is how we got here.
2. *Predictive* - this type of forecasting attempts to determine what is *about to happen*. But it's based on past realities. It assumes that past trends will persist or at very least, the future will unfold more or less in the same general way as in past. This isn't always a bad approach, but it fails to take into account what's currently happening. The old adage that "Those who fail to understand the past are doomed to repeat it" is very strong in corporate culture. Looking forward is fraught with danger. At least we understand the past. 
3. *Proscriptive* - this strategic activity takes what we have diagnosed and predicted and builds a *response* to what we expect to happen. How will we deal with what we think is coming? This is *prophylaxis.* It's the scout's adage to "Be prepared." 

**Figure IMC. Forecasting**

![In-memory computing](https://raw.githubusercontent.com/robertriordan/2400/master/Images/imc.png)

All of these issues are mashed-up in Figure IMC. Let's unpack it.    

We'll start at the bottom, right, where the strategic planners are in a meeting, all green and ready to go. They need to more forward with a strategy, as expressed by the green arrow moving to the right. Other than *gut instinct* (which is what most CxO-level executives rely on most (reference in the McKinsey deck), all they have to go on is analysis, the blue circle hovering over their heads. The data that provide the fuel for analysis have traditionally come from enterprise database systems. All the organisation's systems we have been discussing generate, store and marshal their own proprietary data. And make no mistake: there's plenty of it. Even in the most advanced and sophisticated of *enterprise organisations*, the data are collected for different purposes, specific to the function they serve (such as Customer Relationship Management (CRM) or Supply Chain Management (SCM) or any *XYZ* generic system in play at a firm) and no amount of data architecture or careful planning can render data that are completely compatible between the various systems. There will be differences in measurement frequency and measurement scale, time frames, reporting mandates and so many more. Thus what happens is, *even when the ultimate destination is the enterprise database*, the data have lost considerable detail (to make them compatible with each other). So the rich, detailed and powerful data collected by the Supply Chain Management System, for example, when rolled into the enterprise server, are but a shadow of their former selves. This is illustrated in Figure ICM by the funnels and the width of the arrows emanating from the various systems and ultimately from the enterprise database server in the middle. 

And that's only the half of it. A large part of the *intrinsic value* of data is *currency* - how fresh it is. The rear-view mirror in Figure IMC is no accident. All this data is *retrospective*. ERP never really facilitated real-time analysis because of the separation between transaction *collection* and *analysis*. These two functions never happened in close enough proximity, time-wise.  Thus ERP systems, until very recently, never really did the *Resource Planning* part of their title. They couldn't effectively do planning because they were rear-view oriented. 

As such, by the time the data are ready to be analysed to perform the forecasting functions, they are already stale. Some by as much as a year of more. Note the giant backwards-rotating clock featured prominently in the Figure. Data warehouses, for example, are created from data that are rolled up and frozen *annually*. No analysis until the fiscal or calendar year has ended and the data people have created the warehouse. Talk about the rear-view mirror! Smaller data marts come next. This lag between the time that data are collected and when they become available for analysis is referred to as *latency.* And latency is the enemy of strategy. It's the rear-view mirror.  

[Interested in data warehousing and business intelligence?](http://www.dwhworld.com/datawarehouse-and-business-intelligence/)

Now witness the raging storm that is *big data*, raining down in real time on our intrepid strategists. By definition (see Chapter ?) big data is data that is so voluminous and is travelling at such velocity and coming in such variety that it cannot be collected, stored or analysed using the traditional relational database technologies upon which we have become so dependant (see Chapter 6). The sources of big data are many and varied, including *sensors* and *beacons* (discussed in Chapter 2) and all the *social media* sources with which we are so familiar. Let's dig a little deeper: 

Big data is often described using the *Three Vs*, but IBM has suggested adding three more:

1. *Volume* – the sheer amount of big data is almost unfathomable. (see the data thing wherever it is tk)
2. *Variety* – the sources of big data are broad and growing every day – and so is *data exhaust*, which we’ll tackle later.
3. *Velocity* – the speed with which it is generated, and thus must be consumed, is phenomenal. Big data can quickly go stale.
4. IBM suggests adding *Viability* - the inherent ability of the data to provide the information we seek – is it feasible to obtain and will it persist?
5. IBM also adds *Value* to the list - whether analysis of the data actually leads to *needle-moving* information.
6. Finally, IBM adds *Veracity* to the definition - and thus the finer notion of *precision* that we have discussed in Chapter 2. I interpret this as answering the question of whether the data accurately, persistently and validly give insight into some underlying dimension of interest.

[Interested in IBM's take on Big Data?](http://www.wired.com/insights/2013/05/the-missing-vs-in-big-data-viability-and-value/)

Why is big data so critical? Because big data can give nearly real-time insight into breaking trends and current realities. In any business that needs to be agile (those that rely on *just-in-time* (JIT) inventory, real-time consumption trends -- any industry where product has a short shelf-life and deals with perishable inventory, any industry in which trends change rapidly and swing widely - fashion, for example), this *near real-time* big data is a huge advantage. But the problem with such big data is that it is difficult to analyse in time for it to make a difference. Pity the poor folks at the strategy table, showered with real-time big data but with no way to leverage it. All they want is to move forward in a way that reduces error... because having to account for potential *future error* means more input is required to cover contingencies. And we know that progress is made, at least in part, by *reducing* the effort to provide required input at any given level of output. Alas...  

Enter *in-memory computing* (IMC). 

IMC is just as its name suggests. Rather than collecting, storing, packaging and marshalling data using traditional enterprise database systems and feeding data as required to custom-made analysis software packages, in-memory computing seeks to have *all* relevant data sitting in a massive Random Access Memory (RAM) cache on a massive machine and doing analysis in a non-traditional way on real-time data. Analysing Facebook likes and Twitter tweets and Instagram photos as they happen. This is a radical departure from the old way of doing things. There are no more large data files, formatted to be fed to statistical programmes such as SPSS or SAS. Instead, packages such as Hadoop and R are used to marshal data in real time and spit out near instant results. We will consider some of these techniques in Chapter 6. 

For now, let's consider the ways that an enterprise can effect IMC. There are three (and a pretender):

1. Native IMC - an infrastructure built from the ground-up to take advantage of real-time data and non-traditional analysis techniques. This is the most ambitious and expensive alternative, but the most effective. Big effort, big reward.
2. Retrofitted IMC - taking legacy systems and adding the in-memory capacity to them. For example, taking a legacy SAP install and adding IMC modules to it to reap at least some of the  advantage of IMC. Modeate effort, moderate to big reward.
3. Hybrid IMC - described as *federating off* some of the processing from legacy systems. For example, creating a Native IMC application that would handle only real-time sales data but not other enterprise data and not social media. Running two parallel systems in effect. Moderate to high effort, moderate reward. 
4. Accelerated Storage - the *pretender* in this taxonomy as it involves only the addition of *flash cache* super-fast in-memory data transfer but no analysis capacity. Just moving stuff faster into the traditional analysis apps. Low effort and a matching reward.

Taking advantage of IMC has tremendous benefit for players in industries susceptible to real-time change in their marketplace. But it comes with a price, not the least of which is a change in process. Analysis protocols, data collection regimes and tools must be acquired or tweaked to take advantage of the avalanche of big data. The most gains thus far have been seen in the *supply chain management* areas of large organisations. But more breakthroughs are hotly anticipated in such areas as HR, Finance and Marketing. Stay tuned.   

As a final note, IMC provides type of work all the way up to Knowledge, including some action. IMC can make decisions and act on its own in certain situations. For example, it can automatically ramp up production or shipping of particular *fast-moving* products to match real-time demand. A hot, dry spell in Toronto, for example, could see an IMC application at a soft drink company communicate with production to schedule extra bottling and with distribution to ramp up delivery to areas under the influence of the weather. And in near real time. And all from analysing Twitter hashtags like *#hotintdot*. It's impressive stuff.

[Interested in a slide show on IMC?](http://public.brighttalk.com/resource/core/73977/july_21_too_much_data_nrayner_109065.pdf)

### The sideways view
Let's take the horizontal view of the big picture that is Figure JL and focus on the three numbered ovals running horizontally across all the vertical dimensions. First, let's bring back Figure JL for a refresher.

**Figure JL. The big picture (redux)**

![The big picture of the organisation](https://raw.githubusercontent.com/robertriordan/2400/master/Images/big_picture.png)

Oval number 1 runs across the bottom and encompasses functions and roles and systems that deal predominantly with the measurement and collection/storage of highly granular data, using systems that are low cost and complexity. Decisions at this level of the organisation are highly structured (algorithmic) and are overwhelmingly concerned with matters operational in the firm. The dominant system at this level of the organisation is the TPS. Note finally that increasingly, these tasks are being performed by machines. This is the future of such work - increasingly automated. Stay in school. 

Oval number 2 represents mostly the functions of middle management, where context becomes important and where data become information as they are rolled up through the hierarchy. This level makes decisions that vary from structured to some unstructured at the tactical level with information created predominantly by humans though increasingly by machines. Data and information here are becoming less granular as they roll up. Systems are predominantly at the MIS level of functional specificity within silos, but are becoming more costly and complex to match the level of decisions made. Some DSS are used here.

You will note that the three activities associated with middle management are all marked with a little ![The big picture of the organisation](https://raw.githubusercontent.com/robertriordan/2400/master/Images/target.png) target. Let's spend a minute here and unpack the reason for *targeting* these three activities and those humans who perform them. I have singled them out (unlike the rockets ![The big picture of the organisation](https://raw.githubusercontent.com/robertriordan/2400/master/Images/rocket.png) which indicate that there is high potential here for research and employment - note that for future reference) because the incumbents of these positions are in imminent danger of being replaced by machines. The whole *middle layer* of the organisation -- those who don't do knowledge work and don't do technical work (those in the bottom and the top of the hierarchy) and those who do predominantly rules-based work (accountants, HR people, programmers of all types, lawyers, physicians to a certain degree, managers of all functions) will soon largely be replaced in the majority of their functions of creating context, creating information and making decisions. It's simply a reality of working in a profession where the context and information creation, and the decision making can and is being increasingly accomplished by machines are we become more able to digitise (liquefy) analogue data and simplify the making of previously difficult decisions. Once again, stay n school.

The middle management functions of mentoring and coaching and socialising new employees into the organisational culture and providing visibility into the upper ranks will cease to be performed unless machines can take on these tasks. And that might be a ways away. Meanwhile, rational and maximising algorithms will continue to supplant middle managers. In fact, data, rich, contextualised streams of big data is the new middle manager. Dashboards are changing the structure of the organisation right now. Real-time operations data used in *tactical* decision making - the type that used to be monitored by the great, broad middle swath of the organisation - is being digested and operationalised by machines. Make sure you train yourself and seek employment either as a technician or as an idea person; someone who can leverage technology. Stay away from rules-based occupations. Their days are numbered.   

[The BBC hopes you are: Interested?](http://www.bbc.com/capital/story/20150624-the-end-of-middle-management)

[And another from Wall Street Journal: Interested?](http://www.wsj.com/articles/data-is-the-new-middle-manager-1429478017)

As we reach the top level at oval number 3, we are interacting predominantly with decision, action and knowledge creation at the strategic levels of the organisation. Information is not at all granular and is rolled up to the highest levels for consumption among senior executives who might only be concerned with a single number representing all sales year-to-date as a metric for organisational health. Here, DSS and EIS predominate as unstructured, highly strategic and potentially costly decisions are made. While it's true at every level since much data at the top come from TPS at the root of the organisational processes, especially here, the numbers must be right.

While there are many different types and sizes of organisations, and each is uniquely structured and will have developed with its own particular culture and style, the general taxonomy implied here by the three ovals is a reasonable approximation of the differences across all these dimensions driven by level in the organisation. There will inevitably be overlap (the ovals in fact overlap by design) and differences in particular cases, but this pretty much sums it up. Eat it for dinner with a dash of hot sauce.  

[Interested in types of ICT?](http://tutor2u.net/business/ict/intro*information*system*types.htm)

And that's all I've got to say about that. 
